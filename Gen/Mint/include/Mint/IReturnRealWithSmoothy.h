/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IRETURN_REALVALUE_WITHSMOOTHY_HH
#define IRETURN_REALVALUE_WITHSMOOTHY_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:17:55 GMT

#include "Mint/IReturnReal.h"
#include <complex>

namespace MINT{

class IReturnRealWithSmoothy : virtual public IReturnReal{
  protected:
    IReturnRealWithSmoothy(){};
  public:
    //virtual double RealVal()=0;
    virtual double SmootherLargerRealVal()=0;
    virtual ~IReturnRealWithSmoothy(){};
  };
  
}//namespace MINT
#endif
