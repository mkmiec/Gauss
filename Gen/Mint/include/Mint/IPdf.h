/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IPDF_HH
#define IPDF_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:17:56 GMT

#include "Mint/IEventAccess.h"
#include "Mint/IReturnReal.h"
#include "Mint/IGetRealEvent.h"

namespace MINT{

template<typename EVENT>
class IPdf : virtual public IGetRealEvent<EVENT>{
 public:
  virtual double getVal()=0;
  double RealVal() override =0;

  virtual void beginFit()=0;
  virtual void parametersChanged()=0;
  virtual void endFit()=0;

  //  virtual IPdf<EVENT>* Clone() const=0;

};
}// namespace MINT
#endif
//
