/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TGENPHASESPACE_WITH_RND_HH
#define TGENPHASESPACE_WITH_RND_HH

#include "TGenPhaseSpace.h"
#include "TRandom.h"

class TGenPhaseSpaceWithRnd : public TGenPhaseSpace{
  TRandom* _rnd;
 public:
 TGenPhaseSpaceWithRnd(TRandom* rnd=gRandom) : _rnd(rnd){}
  void setRandom(TRandom* rnd=gRandom){_rnd = rnd;}
  Double_t Generate(){
    TRandom* oldGRandom = gRandom;
    gRandom = _rnd;
    Double_t returnVal = TGenPhaseSpace::Generate();
    gRandom = oldGRandom;
    return returnVal;
  }
};

#endif
//
