/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FIT_COMPLEX_CART_HH
#define FIT_COMPLEX_CART_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:17:55 GMT

#include <string>
#include <iostream>
#include <complex>
#include <string>

#include "Mint/NamedParameterBase.h"
#include "Mint/NamedParameter.h"
#include "Mint/FitParameter.h"
#include "Mint/Phase.h"

#include "Mint/FitComplex.h"

namespace MINT{
class FitComplexCart : public FitComplex{
  //  FitComplexCart(const FitComplexCart& ){};
  // no copying for now.
  // dangerous because for each
  // FitParameter, a pointer
  // is held in MinuitParametSet.
  // Anyway, what would the copied parameter
  // mean?
 protected:
  FitParameter _real, _imag;

  void defaultInit();

  static double _degFac;
  static double degFac();
 public:
  inline const FitParameter& real() const{return _real;}
  inline const FitParameter& imag() const{return _imag;}

  FitParameter& real(){return _real;}
  FitParameter& imag(){return _imag;}

  FitComplex::TYPE type()const override {return FitComplex::CARTESIAN;}

  FitParameter& p1() override {return _real;}
  FitParameter& p2() override {return _imag;}
  const FitParameter& p1() const override {return _real;}
  const FitParameter& p2() const override {return _imag;}

  static std::string makeRealName(const std::string& varName);
  static std::string makeImagName(const std::string& varName);

  void set(std::complex<double> z) override;

  FitComplexCart(const std::string& varName
		 , const char* fname=0
		 , MinuitParameterSet* pset=0
		 , FitParameter::FIX_OR_WHAT fow=FitParameter::FIX
		 , NamedParameterBase::VERBOSITY vb=NamedParameterBase::VERBOSE
		 );
  FitComplexCart(const std::string& varName
		 , MinuitParameterSet* pset
		 , FitParameter::FIX_OR_WHAT fow=FitParameter::FIX
		 , NamedParameterBase::VERBOSITY vb=NamedParameterBase::VERBOSE
		 );

  virtual ~FitComplexCart();

  std::complex<double> getVal() const override;
  std::complex<double> getValInit() const override;
  bool gotInitialised()const override;
  void print(std::ostream& os = std::cout) const override;

  operator std::complex<double>() const override {
    return getVal();
  }

  bool isZero()const override {
    return (0.0 == real() && 0.0 == imag());
  }
};
}//namespace MINT
#endif
//
