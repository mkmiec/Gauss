/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CRYSTAL_BARREL_FORTRAN
#define CRYSTAL_BARREL_FORTRAN
// this is the FOCUS implementation of the Crystal Barrel Lineshape, 
// literally translated from FORTRAN
// (with added features to make it function in the framework)

#include "BW_BW.h"

#include "ILineshape.h"
#include "ILineshape.h"
#include "AssociatedDecayTree.h"
#include "DalitzEventAccess.h"
#include "IDalitzEventAccess.h"

#include "IGenFct.h"
#include "TRandom.h"

#include "counted_ptr.h"
#include <complex>

class CrystalBarrelFOCUS : public BW_BW, virtual public ILineshape{
 protected:
  mutable double _mRho, _GRho, _mOmega, _GOmega;
  std::complex<double> AlbertosFunction(double sij);

  double mRho() const;
  double GammaRhoFixed() const;
  double mOmega() const;
  double GammaOmegaFixed() const;

  std::complex<double> BW (double s, double m_w, double gamma) const;

 public:
  CrystalBarrelFOCUS( const AssociatedDecayTree& decay
		  , IDalitzEventAccess* events);
  CrystalBarrelFOCUS(const CrystalBarrelFOCUS& other);
  virtual ~CrystalBarrelFOCUS();

  virtual std::complex<double> getVal();
  virtual std::complex<double> getValAtResonance();
  virtual void print(std::ostream& out = std::cout) const;
  virtual void print(std::ostream& out = std::cout);

  virtual std::string name() const{
    return "CrystalBarrel_ala_FOCUS("+_theDecay.oneLiner() +")";
  }

};

std::ostream& operator<<(std::ostream& out, const CrystalBarrelFOCUS& amp);

#endif
//
