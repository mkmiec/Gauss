/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:18:05 GMT
#ifndef DALITZABSCOMPLEXBRACKETS
#define DALITZABSCOMPLEXBRACKETS
#include "TObject.h"
#include "Rtypes.h"

#include <complex>

class AbsComplexBrackets{
 public:
  virtual std::complex<Double_t> getCVal() const=0;
  virtual std::complex<Double_t> operator()() const{return getCVal();}
  ClassDef(AbsComplexBrackets, 0)
};
//#ifdef __MAKECINT__
//#pragma link C++ class std::complex<Double_t>+;
//#endif

#endif
//
