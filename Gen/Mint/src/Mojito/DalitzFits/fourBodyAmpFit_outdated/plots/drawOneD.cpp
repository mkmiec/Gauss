/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:18:01 GMT
void drawOneD(std::string a="s12"){

  gROOT->Macro("style.C");
  
  TFile* f = new TFile("AmpPlots.root");
  
  TNtuple* ntp = f->Get("DalitzEventList_sij");

  TCanvas* cow = new TCanvas;
  ntp->Draw((a + ">> h" + a).c_str());

  TH1F* h = gDirectory->Get(("h" + a).c_str());
  //  h->Rebin(2);

  cow->Print( (a + ".eps").c_str());
}
