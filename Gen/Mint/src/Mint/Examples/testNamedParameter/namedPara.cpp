/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Mint/NamedParameter.h"
#include <iostream>
#include <cmath>

using namespace MINT;
using namespace std;

int main(){
  NamedParameter<double> E("E"), p("p");
  cout << E << ", " << p << ", m = " << sqrt(E*E - p*p) << endl;
  return 0;
}
