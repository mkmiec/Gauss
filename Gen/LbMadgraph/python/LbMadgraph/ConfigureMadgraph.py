###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import
from Configurables import MadgraphProduction
from Configurables import Generation, MinimumBias, Special, Pythia8Production
def ConfigureMadgraph(sampleGenToolOpt):
    # Create the default settings.
    opts = {"EvtType": 0,
            "EvtMax": 10000,
            "DecEff": 1.0,
            "GenGridpack": False,
            "Commands": None }

    opts.update(sampleGenToolOpt)

    # Add Pythia as minimum bias production tool.
    Generation().addTool(MinimumBias)
    Generation().MinimumBias.ProductionTool = "Pythia8Production"
    Generation().MinimumBias.addTool(Pythia8Production)

    # Add Madgraph as special production tool.
    Generation().addTool(Special)
    Generation().SampleGenerationTool = "Special"
    Generation().Special.ProductionTool = "MadgraphProduction"
    Generation().Special.addTool(MadgraphProduction)
    Generation().DecayTool            = ""
    Generation().Special.CutTool        = ""
    Generation().Special.DecayTool      = ""
    # General PileUptool
    Generation().PileUpTool           = "FixedLuminosityForRareProcess"
    
    # Set the flag for gridpack generation
    Generation().Special.MadgraphProduction.GenGridpack = opts["GenGridpack"]

    # Set number of events to generate per batch. Include 20% overestimate.
    Generation().Special.MadgraphProduction.EventType = opts["EvtType"]
    Generation().Special.MadgraphProduction.Events = (
        1.2*opts["EvtMax"]/opts["DecEff"])

    # Set MadGraph commands.
    madgraphCommands = opts["Commands"]
    if madgraphCommands:
        Generation().Special.MadgraphProduction.Commands += madgraphCommands
    else:
        raise RuntimeError("No Madgraph Commands defined.")

    # Set shower, pileup, and reinitialization.
    Generation().Special.MadgraphProduction.ShowerToolName = "Pythia8Production"
    Generation().Special.PileUpProductionTool = "Pythia8Production/Pythia8PileUp"
    Generation().Special.ReinitializePileUpGenerator = False
