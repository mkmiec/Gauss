###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import *

# Configure Gauss.
OutputStream('GaussTape').Output = (
    "DATAFILE='PFN:gauss.xgen' TYP='POOL_ROOTTREE' OPT='RECREATE'")
LHCbApp().EvtMax = 1e2
LHCbApp().DDDBtag = "dddb-20170721-2"
LHCbApp().CondDBtag = "sim-20160321-2-vc-md100"
Gauss().Phases = ["Generator", "GenToMCTree"]
GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1
GaussGen.RunNumber = 2