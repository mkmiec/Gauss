/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// This class
#include "BeamShape.h"

// From STL
#include <cmath>

// From Gaudi
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/IRndmGenSvc.h"

//===========================================================================
// Implementation file for class: BeamShape
//
// 2009-10-21: Magnus Lieng
//===========================================================================

DECLARE_COMPONENT( BeamShape )


//===========================================================================
// Initialize Particle Gun parameters
//===========================================================================
StatusCode BeamShape::initialize() {
  StatusCode sc = GaudiTool::initialize() ;
  if ( ! sc.isSuccess() ) return sc ;

  // Get beam direction (force 1 or -1)
  m_zDir.value() = (m_zDir.value()<0?-1:1);
  
  // Get position spread
  m_xSigma = sqrt(m_xEmm.value()*m_xBeta.value());
  m_ySigma = sqrt(m_yEmm.value()*m_yBeta.value());

  // Get centroid momentums
  const double pt = m_momentum.value()*sin(m_theta.value());
  m_pxCenter = m_zDir.value()*pt*cos(m_phi.value());
  m_pyCenter = m_zDir.value()*pt*sin(m_phi.value());

  // Get momentum spread
  m_pxSigma = sqrt(m_xEmm.value()/m_xBeta.value());
  m_pySigma = sqrt(m_yEmm.value()/m_yBeta.value());


  IRndmGenSvc * randSvc = svc< IRndmGenSvc >( "RndmGenSvc" , true ) ;

  sc = m_flatGenerator.initialize( randSvc , Rndm::Flat( 0. , 1. ) ) ;
  if ( ! sc.isSuccess() )
    return Error( "Cannot initialize flat generator" ) ;

  sc = m_gaussGenerator.initialize( randSvc , Rndm::Gauss( 0. , 1. ) ) ;
  if ( ! sc.isSuccess() )
    return Error( "Cannot initialize gaussian generator" ) ;

  // Get the mass of the particle to be generated
  LHCb::IParticlePropertySvc* ppSvc =
    svc< LHCb::IParticlePropertySvc >( "LHCb::ParticlePropertySvc" , true ) ;

  // setup particle information
  m_mass = ppSvc->find(LHCb::ParticleID( m_pdgCode.value() ) )->mass();

  info() << "Particle type: " << ppSvc->find(LHCb::ParticleID( m_pdgCode.value() ) )->particle()
         << endmsg ;

  // printout vertex information
  info() << "Beam Centroid at : ( "
         << "x = " << m_xCenter.value() / Gaudi::Units::mm << " mm "
         << ", y = " << m_yCenter.value() / Gaudi::Units::mm << " mm "
         << ", z = " << m_zCenter.value() / Gaudi::Units::mm << " mm "
         << ")" << endmsg ;

  info() << "Momentum: ( "
         << "px = " << m_pxCenter / Gaudi::Units::GeV << " GeV "
         << ", py = " << m_pyCenter / Gaudi::Units::GeV << " GeV "
         << ", p = " << m_momentum.value() / Gaudi::Units::GeV << " GeV "
         << ")" <<  endmsg;

  info() << "Direction: " << (m_zDir.value()<0?"Beam 2":"Beam 1") << endmsg;

  release( ppSvc ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);

  return sc ;
}

//===========================================================================
// Generate the particles
//===========================================================================
void BeamShape::generateParticle( Gaudi::LorentzVector & fourMomentum , 
                                Gaudi::LorentzVector & origin , 
                                int & pdgId ) {
  double xn(0.),yn(0.),zn(0.) ;
  double x(0.),y(0.),z(0.) ;
  double px(0.), py(0.), pz(0.) ;

  // Get particle position in distance from center
  xn = m_gaussGenerator()*m_xSigma;
  yn = m_gaussGenerator()*m_ySigma;
  zn = 0.0; // If one ever would want to implement z-smearing.

  // In real lhcb coord sys
  x = xn + m_xCenter.value();
  y = yn + m_yCenter.value();
  z = zn + m_zCenter.value();

  // Get momenta
  double dx = -(m_xAlpha.value()/m_xBeta.value())*xn + m_pxSigma*m_gaussGenerator();
  double dy = -(m_yAlpha.value()/m_yBeta.value())*yn + m_pySigma*m_gaussGenerator();
  px = m_momentum.value()*dx + m_pxCenter;
  py = m_momentum.value()*dy + m_pyCenter;
  pz = m_zDir.value()*sqrt(m_momentum.value()*m_momentum.value() - px*px - py*py);

  // Set up fourmomentum
  fourMomentum.SetPx( px );
  fourMomentum.SetPy( py );
  fourMomentum.SetPz( pz );
  fourMomentum.SetE( std::sqrt( m_mass * m_mass + fourMomentum.P2() ) );

  // Set up vertex
  origin.SetCoordinates( x , y , z , 0. ) ; // No timing

  // Pass the particle code
  pdgId = m_pdgCode.value() ;
}
