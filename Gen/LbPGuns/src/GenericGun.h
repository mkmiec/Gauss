/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GenericGun.h,v 1.1.1.1 2009-09-18 16:18:24 gcorti Exp $
#ifndef PARTICLEGUNS_GENERICGUN_H
#define PARTICLEGUNS_GENERICGUN_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/PhysicalConstants.h"

// from ParticleGuns
#include "LbPGuns/IParticleGunTool.h"

namespace SPGGenMode {
  enum Mode { FixedMode = 1, GaussMode , FlatMode } ;
}

/** @class GenericGun GenericGun.h "GenericGun.h"
 *
 *  This code is used to generate particles with pt, eta and phi selected as
 *  specified by job options.  Three modes of generation are possible:
 *          - FixedMode (1):  Generation with a fixed value
 *          - GaussianMode (2): Generation with a gaussian of specified mean and
 *                              sigma
 *          - FlatMode (3): Generation of a flat distribution between a specified
 *                          minimum and maximum value
 *    The mode can be separately selected for pt, eta and phi.
 *    The particle pdg code can also be specified
 *
 *  @author M. Shapiro
 *  @author W. Pokorski
 *  @date 2000-03-01
 */
class GenericGun : public extends<GaudiTool , IParticleGunTool> {
 public:

  using extends::extends;
  /// Initialize method
  StatusCode initialize() override;

  /// Generate the particle
  void generateParticle( Gaudi::LorentzVector & fourMomentum ,
                         Gaudi::LorentzVector & origin ,
                         int & pdgId ) override;

  /// Print counters
  void printCounters( ) override { ; } ;

 private:
	// Setable Properties:-
  Gaudi::Property<double> m_requestedPt{this,"Pt",5.0 * Gaudi::Units::GeV,"Pt"} ;
  Gaudi::Property<double> m_requestedEta{this,"Eta",0.0,"Eta"} ;
  Gaudi::Property<double> m_requestedPhi{this,"Phi",0.0 * Gaudi::Units::rad,"Phi"} ;
  Gaudi::Property<double> m_minPt{this,"MinPt",1. * Gaudi::Units::GeV,"MinPt"} ;
  Gaudi::Property<double> m_minEta{this,"MinEta",-4.0,"MinEta"} ;
  Gaudi::Property<double> m_minPhi{this,"MinPhi",0. * Gaudi::Units::rad,"MinPhi"} ;
  Gaudi::Property<double> m_maxPt{this,"MaxPt",100. * Gaudi::Units::GeV,"MaxPt"} ;
  Gaudi::Property<double> m_maxEta{this,"MaxEta",4.0,"MaxEta"} ;
  Gaudi::Property<double> m_maxPhi{this,"MaxPhi",Gaudi::Units::twopi * Gaudi::Units::rad,"MaxPhi"} ;
  Gaudi::Property<double> m_sigmaPt{this,"SigmaPt",0.1 * Gaudi::Units::GeV,"SigmaPt"} ;
  Gaudi::Property<double> m_sigmaEta{this,"SigmaEta",0.1,"SigmaEta"} ;
  Gaudi::Property<double> m_sigmaPhi{this,"SigmaPhi",0.1 * Gaudi::Units::rad,"SigmaPhi"} ;
  Gaudi::Property<int> m_PtGenMode{this,"ModePt",SPGGenMode::FixedMode,"ModePt"}  ; 
  Gaudi::Property<int> m_EtaGenMode{this,"ModeEta",SPGGenMode::GaussMode,"ModeEta"} ;
  Gaudi::Property<int> m_PhiGenMode{this,"ModePhi",SPGGenMode::FlatMode ,"ModePhi"} ;
  Gaudi::Property<int> m_pdgCode{this,"PdgCode",211,"PdgCode"} ;

	// Local Member Data:
  double m_mass ;

	// Private Methods:
  double generateValue( const int mode , const double val ,
                        const double sigma , const double min ,
                        const double max ) ;

  // Random generators:
  Rndm::Numbers m_flatGenerator ;
  Rndm::Numbers m_gaussGenerator ;
} ;

#endif // PARTICLEGUNS_GENERICGUN_H
