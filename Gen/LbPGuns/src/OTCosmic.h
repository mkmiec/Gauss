/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PARTICLEGUNS_OTCOSMIC_H
#define PARTICLEGUNS_OTCOSMIC_H

// Include files
// From Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/Transform3DTypes.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/PhysicalConstants.h" 
// From ParticleGuns
#include "LbPGuns/IParticleGunTool.h"

class SolidBox ;
namespace LHCb { class ParticleProperty ; }


/** @class OTCosmic OTCosmic.h "OTCosmic.h"
 * The output will be stored in the transient event store so it can be
 *    passed to the simulation.
 *
 *  @author W. Seligman
 *  @date   2002-11-08
 */
class OTCosmic : public extends<GaudiTool, IParticleGunTool> {
 public:

  /// Constructor
  using extends::extends;

  /// Initialize method
  StatusCode initialize() override;

  /// Generate particle
  void generateParticle( Gaudi::LorentzVector & fourMomentum ,
                         Gaudi::LorentzVector & origin , int & pdgId ) override;

  /// Print counters
  void printCounters( ) override;

private:

  // event counter, used for event ID
  int m_events{0};
  int m_generated{0};
  int m_rejectedbyenergycut{0};
  int m_rejectedbyscintacceptance{0};
  
  Gaudi::Property<float> m_emin{this,"emin",70.*Gaudi::Units::GeV ,"emin"};
  Gaudi::Property<float> m_emax{this,"emax",10000.*Gaudi::Units::GeV ,"emax"};
  Gaudi::Property<float> m_ctcut{this,"tmin",0. ,"tmin"};
  Gaudi::Property<float> m_tmin{this,"tmax",0. ,"tmax"};
  Gaudi::Property<float> m_tmax{this,"ctcut",0.35 ,"ctcut"};
  Gaudi::Property<int> m_printEvent{this,"PrintEvent",10,"PrintEvent"};
  Gaudi::Property<int> m_printMod{this,"PrintMod",100,"PrintMod"};
  Gaudi::Property<float> m_thetamin{this,"ThetaMin", 0.,"ThetaMin"};
  Gaudi::Property<float> m_thetamax{this,"ThetaMax", 1.,"ThetaMax"};
  Gaudi::Property<float> m_phimin{this,"PhiMin", -1*Gaudi::Units::pi,"PhiMin"};
  Gaudi::Property<float> m_phimax{this,"PhiMax", Gaudi::Units::pi,"PhiMax"};

  /// Flat random number generator
  Rndm::Numbers m_flatgenerator ;
  std::unique_ptr<SolidBox> m_scintsolid ;
  Gaudi::Transform3D m_toptransform ;
  Gaudi::Transform3D m_bottransform ;
  const LHCb::ParticleProperty* m_muplus  ;
  const LHCb::ParticleProperty* m_muminus ;
};

#endif
