/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LBBOUND_BOUNDPRODUCTION_H 
#define LBBOUND_BOUNDPRODUCTION_H 1

// Gaudi.
#include "GaudiAlg/GaudiTool.h"
#include "Generators/IProductionTool.h"

/** 
 * Production tool to generate bound states. This is a pure virtual
 * class intended for generators which use minimum bias events to
 * produce bound states, e.g. deuteron production. The bindStates
 * methods must be implemented in derived classes.
 *  
 * @class  BoundProduction
 * @file   BoundProduction.h 
 * @author Philip Ilten
 * @date   2016-03-18
 */
class BoundProduction : public extends<GaudiTool,IProductionTool> {
public:

  /// Default constructor.
  using extends::extends;
  /// Initialize the tool.
  StatusCode initialize() override;

  /// Initialize the generator.
  StatusCode initializeGenerator() override;

  /// Finalize the tool.
  StatusCode finalize() override;

  /// Generate an event.
  StatusCode generateEvent(HepMC::GenEvent* theEvent,
				   LHCb::GenCollision* theCollision) override;

  /// Set particle stable.
  void setStable(const LHCb::ParticleProperty* thePP) override;

  /// Update a particle.
  void updateParticleProperties(const LHCb::ParticleProperty* thePP) override;

  /// Turn on fragmentation.
  void turnOnFragmentation() override;

  /// Turn off fragmentation.
  void turnOffFragmentation() override;

  /// Hadronize an event.
  StatusCode hadronize(HepMC::GenEvent* theEvent,
		LHCb::GenCollision* theCollision) override;

  /// Save the event record.
  void savePartonEvent(HepMC::GenEvent* theEvent) override;

  /// Retrieve the event record.
  void retrievePartonEvent(HepMC::GenEvent* theEvent) override;

  /// Print the running conditions.
  void printRunningConditions() override;

  /// Returns whether a particle has special status.
  bool isSpecialParticle(const LHCb::ParticleProperty* thePP) const override;

  /// Setup forced fragmentation.
  StatusCode setupForcedFragmentation(const int thePdgId) override;

  /// Initialize the bound process tool.
  virtual StatusCode boundInitialize();
  
  /// Initialize the bound process generator.
  virtual StatusCode boundInitializeGenerator();
  
  /// Finalize the bound process tool.
  virtual StatusCode boundFinalize();
  
protected:

  // Methods.
  /// Create the bound states.
  virtual StatusCode bindStates(HepMC::GenEvent *theEvent) = 0;

  // Properties.
  Gaudi::Property<std::string> m_beamToolName{this,"BeamToolName","CollidingBeams","The beam tool to use."};   ///< The beam tool name.
  Gaudi::Property<std::string> m_prodToolName{this,"ProductionToolName","Pythia8Production",
  "Name of the production tool to use for the actual event "
  "generation (any production tool except this tool is valid."};   ///< The production tool name.

  // Members.
  int m_nEvents{0};                      ///< Number of events.
  IProductionTool *m_prod{nullptr};      ///< The production tool.
};

#endif // LBBOUND_BOUNDPRODUCTION_H
