/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
// ============================================================================
#ifndef LBONIAPAIRS_PRODUCTION_H
#define LBONIAPAIRS_PRODUCTION_H 1
// ============================================================================
// Include files
// PythiaProduction
#include "LbPythia/PythiaProduction.h"
// ============================================================================
/** @class OniaPairsProduction
 *
 *  The productiuon tool for Onia-pairs
 *
 *  @author Alexey NOVOSELOV Alexey.Novoselov@cern.ch
 *
 *                    $Revision$
 *  Last modification mid/10/2013
 *                 by alexn
 */
class OniaPairsProduction : public PythiaProduction
{
 public:
  // ==========================================================================
  /** standard constructor
   *  @param type   tool type
   *  @param name   tool name
   *  @param parent parent component
   */
  OniaPairsProduction
    ( const std::string& type   ,
      const std::string& name   ,
      const IInterface*  parent ) ;

  // ==========================================================================
  /// initilize the production tool
  StatusCode initialize( ) override;
  /// finalize
  StatusCode finalize( ) override;
  // ==========================================================================
  /// generate the event
  StatusCode generateEvent
    ( HepMC::GenEvent*    theEvent     ,
      LHCb::GenCollision* theCollision ) override;
  // ==========================================================================
 protected:
  // ==========================================================================
 private:
  // ==========================================================================
  Gaudi::Property<double> m_ecm{this,"Ecm",0.,"Ecm"} ;
  Gaudi::Property<double> m_psi1S1S {this,"Psi1S1S",0.,"Psi1S1S"}; 
  Gaudi::Property<double> m_psi1S2S{this,"Psi1S2S",0.,"Psi1S2S"} ;
  Gaudi::Property<double> m_psi2S2S{this,"Psi2S2S",0.,"Psi2S2S"} ;
  Gaudi::Property<double> m_ups1S1S{this,"Ups1S1S",0.,"Ups1S1S"} ;
  Gaudi::Property<double> m_ups1S2S{this,"Ups1S2S",0.,"Ups1S2S"} ;
  Gaudi::Property<double> m_ups1S3S{this,"Ups1S3S",0.,"Ups1S3S"} ;
  Gaudi::Property<double> m_ups2S2S{this,"Ups2S2S",0.,"Ups2S2S"} ;
  Gaudi::Property<double> m_ups2S3S{this,"Ups2S3S",0.,"Ups2S3S"} ;
  Gaudi::Property<double> m_ups3S3S{this,"Ups3S3S",0.,"Ups3S3S"} ;
  Gaudi::Property<double> m_ScfAlpS{this,"ScaleFactorInAlpS",1.,"ScaleFactorInAlpS"} ;
  Gaudi::Property<double> m_ScfPDF{this,"ScaleFactorInPDF",1.,"ScaleFactorInPDF"} ;
  Gaudi::Property<double> m_ScfShwr{this,"ScaleFactorInShowers",1.,"ScaleFactorInShowers"} ;
  Gaudi::Property<double> m_MaxWghtMult{this,"MaxWeightMultiplier",1.,"MaxWeightMultiplier"} ;
  Gaudi::Property<CommandVector> m_PyCommVec {this,"PyCommVec",{},"Command Vector"}; 
  CommandVector m_PyDefComm ;

  // ==========================================================================
};
// ============================================================================
// The END
// ============================================================================
#endif // LBONIAPAIRS_PRODUCTION_H
// ============================================================================
