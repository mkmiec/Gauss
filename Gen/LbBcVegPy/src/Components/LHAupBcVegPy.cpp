/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// LbPythia.
#include "LbPythia/Pythia.h"

// Local.
#include "LHAupBcVegPy.h"

//-----------------------------------------------------------------------------
//  Implementation file for class: LHAupBcVegPy
//
//  2015-05-01 : Philip Ilten
//-----------------------------------------------------------------------------

//=============================================================================
// Default constructor.
//=============================================================================
Pythia8::LHAupBcVegPy::LHAupBcVegPy(HardProduction *hard) {m_hard = hard;}

//=============================================================================
// Fill the HEPRUP common block.
//=============================================================================
bool Pythia8::LHAupBcVegPy::fillHepRup() {
  if (!m_hard || !m_hard->m_pythia) return false;
  return (m_hard->hardInitializeGenerator()).isSuccess() && 
    (m_hard->m_pythia->initializeGenerator()).isSuccess();
}

//=============================================================================
// Fill the HEPEUP common block.
//=============================================================================
extern "C" {void bcvegpy_upevnt__();}
bool Pythia8::LHAupBcVegPy::fillHepEup() {
  if (!m_hard || !m_hard->m_pythia) return false;
  hepeup_.idprup = 1001;
  bcvegpy_upevnt__();
  hepeup_.istup[0] = -1;
  hepeup_.istup[1] = -1;
  return true;
}

//=============================================================================
// The END.
//=============================================================================
