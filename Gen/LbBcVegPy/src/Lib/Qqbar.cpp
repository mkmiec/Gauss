/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: Qqbar.cpp,v 1.1.1.1 2006-04-24 21:45:50 robbep Exp $
// access BcGen common Qqbar
#include "LbBcVegPy/Qqbar.h"

// set pointer to zero at start
Qqbar::QQBAR* Qqbar::s_qqbar =0;

// Constructor
Qqbar::Qqbar() : m_dummy( 0 ) , m_realdummy( 0. ) { }

// Destructor
Qqbar::~Qqbar() { }

// access iqqbar in common
int& Qqbar::iqqbar() {
  init(); // check COMMON is initialized
  return s_qqbar->iqqbar;
}

// access iqcode in common
int& Qqbar::iqcode() {
  init(); // check COMMON is initialized
  return s_qqbar->iqcode;
}



