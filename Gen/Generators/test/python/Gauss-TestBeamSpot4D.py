###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# Options to just run the beam spot smearing, 
# dummying the HepMCEvent with a single 'PV' 
# and producing output histograms and counters
#

from Gauss.Configuration import *

LHCbApp().EvtMax = 100

importOptions("$GAUSSOPTS/GenStandAlone.py")

from Configurables import Gauss 

Gauss().OutputType = 'NONE'

def test_only():
  from Gauss.Configuration import GaudiSequencer
  from Configurables import IVertexSmearingTool_test
  GaudiSequencer("Generator").Members  = [GenInit("GaussGen"), IVertexSmearingTool_test(VertexSmearingTool="BeamSpot4D", nPVs = 2000 )]
  GaudiSequencer("SkipGeant4").Members = []
  GaudiSequencer("GenFSRSeq").Members  = []

from Gaudi.Configuration import appendPostConfigAction
appendPostConfigAction(test_only)
