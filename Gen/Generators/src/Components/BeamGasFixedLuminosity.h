/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: BeamGasFixedLuminosity.h,v 1.4 2009-04-07 16:11:21 gcorti Exp $
#ifndef GENERATORS_BEAMGASFIXEDLUMINOSITY_H
#define GENERATORS_BEAMGASFIXEDLUMINOSITY_H

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "Generators/IPileUpTool.h"
#include "Event/BeamParameters.h"
#include "Event/GenFSR.h"

// forward declaration
class IRndmGenSvc ;
class ICounterLogFile ;

/** @class BeamGasFixedLuminosity BeamGasFixedLuminosity.h "BeamGasFixedLuminosity.h"
 *
 *  Tool to compute variable number of pile up events
 *  depending on beam parameters for beam-gas collisions
 *
 */
class BeamGasFixedLuminosity : public GaudiTool, virtual public IPileUpTool {
public:
  /// Standard constructor
  BeamGasFixedLuminosity( const std::string& type, const std::string& name,
                   const IInterface* parent) ;

  virtual ~BeamGasFixedLuminosity( ); ///< Destructor

  /// Initialize method
  StatusCode initialize( ) override;

  /// Finalize method
  StatusCode finalize( ) override;

  /** Implements IPileUpTool::numberOfPileUp
   *  Returns the number of pile-up interactions in one event. It follows
   *  a Poisson distribution with
   *  mean = Luminosity * cross_section / crossing_rate.
   *  The fixed luminosity is returned in the GenHeader.
   */
  unsigned int numberOfPileUp( ) override;

  /// Implements IPileUpTool::printPileUpCounters
  void printPileUpCounters( ) override;

protected:

private:

  /// Properties
  Gaudi::Property<double> m_fixedTargetLuminosity{this, "FixedTargetLuminosity",
                                   1.E26/(Gaudi::Units::cm2*Gaudi::Units::s)};
  Gaudi::Property<double> m_fixedTargetXSection{this, "FixedTargetXSection", 
                                   30.*Gaudi::Units::millibarn};

  /// Location where to store FSR counters (set by options)
  Gaudi::Property<std::string>  m_FSRName{this,"GenFSRLocation",LHCb::GenFSRLocation::Default,"GenFSRLocation"};

  ICounterLogFile * m_xmlLogTool ; ///< XML File for generator counters

  Gaudi::Property<std::string> m_beamParameters{
       this, "BeamParameters" , LHCb::BeamParametersLocation::Default, "BeamParameters Location"};

  int    m_numberOfZeroInteraction ; ///< Counter of empty events

  int    m_nEvents ; ///< Counter of events (including empty events)

  Gaudi::Property<bool> m_zeroAllowed{this, "ZeroAllowed", false,
"Allow for 0 interactions"};

  Gaudi::Property<bool> m_rareProcess{this, "RareProcess", false, "Generate (nu+1) average collisions"};

  IRndmGenSvc * m_randSvc ; ///< Pointer to random number generator service
};
#endif // GENERATORS_BEAMGASFIXEDLUMINOSITY_H
