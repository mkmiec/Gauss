/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: FixedLuminosityForSpillOver.cpp,v 1.2 2009-04-07 16:11:21 gcorti Exp $
// Include files

// local
#include "FixedLuminosityForSpillOver.h"

// from Gaudi
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/SystemOfUnits.h"

// From Event
#include "Event/GenCountersFSR.h"

// From Generators
#include "Generators/GenCounters.h"
#include "Generators/ICounterLogFile.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FixedLuminosityForSpillOver
//
// 2005-08-17 : Patrick Robbe
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory

DECLARE_COMPONENT( FixedLuminosityForSpillOver )

//=============================================================================
// Initialize method
//=============================================================================
StatusCode FixedLuminosityForSpillOver::initialize( ) {
  StatusCode sc = GaudiTool::initialize( ) ;
  if ( sc.isFailure() ) return sc ;

  // Initialize the number generator
  m_randSvc = svc< IRndmGenSvc >( "RndmGenSvc" , true ) ;

  // Log file XML
  m_xmlLogTool = tool< ICounterLogFile >( "XmlCounterLogFile" ) ;

  info() << "Poisson distribution with fixed luminosity. " << endmsg ;

  return sc ;
}

//=============================================================================
// Compute the number of pile up to generate according to beam parameters
//=============================================================================
unsigned int FixedLuminosityForSpillOver::numberOfPileUp( ) {
  LHCb::BeamParameters * beam = get< LHCb::BeamParameters >( m_beamParameters.value() ) ;
  if ( 0 == beam ) Exception( "No beam parameters registered" ) ;

  LHCb::GenFSR* genFSR = nullptr;
  if(m_FSRName.value() != ""){
    IDataProviderSvc* fileRecordSvc = svc<IDataProviderSvc>("FileRecordDataSvc", true);
    genFSR = getIfExists<LHCb::GenFSR>(fileRecordSvc, m_FSRName.value(), false);
    if(!genFSR) warning() << "Could not find GenFSR at " << m_FSRName.value() << endmsg;
  }

  unsigned int result = 0 ;
  m_nEvents++ ;
  if(genFSR) genFSR->incrementGenCounter(LHCb::GenCountersFSR::AllEvt,1);

  Rndm::Numbers poissonGenerator( m_randSvc , Rndm::Poisson( beam -> nu() ) ) ;
  result = (unsigned int) poissonGenerator() ;
  if ( 0 == result ) {
    m_numberOfZeroInteraction++ ;
    if(genFSR) genFSR->incrementGenCounter(LHCb::GenCountersFSR::ZeroInt, 1);
  }

  return result ;
}

//=============================================================================
// Print the specific pile up counters
//=============================================================================
void FixedLuminosityForSpillOver::printPileUpCounters( ) {
  using namespace GenCounters ;
  printCounter( m_xmlLogTool , "all events (including empty events)", m_nEvents ) ;
  printCounter( m_xmlLogTool , "events with 0 interaction" ,
                m_numberOfZeroInteraction ) ;
}

//=============================================================================
// Finalize method
//=============================================================================
StatusCode FixedLuminosityForSpillOver::finalize( ) {
  release( m_randSvc ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  return GaudiTool::finalize( ) ;
}
