/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENERATORS_BeamSpot4D_H
#define GENERATORS_BeamSpot4D_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/RndmGenerators.h"

// from Gaudi
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/PhysicalConstants.h"
// from Event
#include "Event/HepMCEvent.h"
#include "Event/BeamParameters.h"

#include "Generators/IVertexSmearingTool.h"
// from ROOT 
#include <TMatrixD.h>

/** @class BeamSpot4D BeamSpot4D.h
 *
 *  VertexSmearingTool to sample the (x,y,z,t) parameters from a 4D PDF
 *  that describes the intersection of two bunches.
 *
 *  @author Tim Evans
 *  @date   2019-09-06
 */
class BeamSpot4D final : public GaudiTool,
  virtual public IVertexSmearingTool
{

  public:

    /// Standard constructor
    BeamSpot4D( const std::string& type,
        const std::string& name,
        const IInterface* parent );


    /// Initialize function
    StatusCode initialize( ) override;
    StatusCode smearVertex( LHCb::HepMCEvent * theEvent ) override;

    virtual ~BeamSpot4D(){};
  private:
    Gaudi::Property<std::string>   m_beamParameters{this, "BeamParameters", LHCb::BeamParametersLocation::Default};
    Rndm::Numbers m_rand;            ///< Gaussian random number generator
    
    struct GaussND 
    {
      GaussND() = default; 
      GaussND(const GaussND& other); 
      GaussND& operator=( const GaussND& other);
      GaussND(const TMatrixD& C);
      std::vector<double> operator()(Rndm::Numbers& rnd) const;
      TMatrixD U;
    };
    
    GaussND calculateInteractionDistribution(const LHCb::BeamParameters* beamp);

};

#endif // GENERATORS_BeamSpot4D_H
