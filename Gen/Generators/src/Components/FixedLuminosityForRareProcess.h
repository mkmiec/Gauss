/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: FixedLuminosityForRareProcess.h,v 1.2 2009-04-07 16:11:21 gcorti Exp $
#ifndef GENERATORS_FIXEDLUMINOSITYFORRAREPROCESS_H
#define GENERATORS_FIXEDLUMINOSITYFORRAREPROCESS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/RndmGenerators.h"

#include "Generators/IPileUpTool.h"

// From Event
#include "Event/BeamParameters.h"
#include "Event/GenFSR.h"

// forward declaration
class IRndmGenSvc ;
class ICounterLogFile ;

/** @class FixedLuminosityForRareProcess FixedLuminosityForRareProcess.h "FixedLuminosityForRareProcess.h"
 *
 *  Tool to compute variable number of pile up events
 *  depending on beam parameters
 *
 *  @author Patrick Robbe
 *  @date   2005-08-17
 */
class FixedLuminosityForRareProcess : public extends<GaudiTool,IPileUpTool> {
public:
  /// Standard constructor
  using extends::extends;
  
  /// Initialize method
  StatusCode initialize( ) override;

  /// Finalize method
  StatusCode finalize( ) override;

  /** Implements IPileUpTool::numberOfPileUp
   *  Returns the number of pile-up interactions in one event. It follows
   *  a Poisson distribution with
   *  mean = Luminosity * cross_section / crossing_rate.
   *  The fixed luminosity is returned as the currentLuminosity.
   */
  unsigned int numberOfPileUp( ) override;

  /// Implements IPileUpTool::printPileUpCounters
  void printPileUpCounters( ) override;

protected:

private:
  ICounterLogFile * m_xmlLogTool{nullptr} ; ///< XML File for generator statistics

  Gaudi::Property<std::string> m_beamParameters{this,"BeamParameters",LHCb::BeamParametersLocation::Default,"BeamParameters"} ; ///< Location of beam parameters (set by options)

  /// Location where to store FSR counters (set by options)
  Gaudi::Property<std::string>  m_FSRName{this,"GenFSRLocation",LHCb::GenFSRLocation::Default,"GenFSRLocation"};

  int    m_nEvents{0} ; ///< Counter of events (including empty events)

  IRndmGenSvc * m_randSvc{nullptr} ; ///< Pointer to random number generator service
};
#endif // GENERATORS_FIXEDLUMINOSITYFORRAREPROCESS_H
