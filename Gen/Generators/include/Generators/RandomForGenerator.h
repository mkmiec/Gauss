/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: RandomForGenerator.h,v 1.2 2007-10-11 13:23:26 robbep Exp $
#ifndef GENERATORS_RANDOMFORGENERATOR_H 
#define GENERATORS_RANDOMFORGENERATOR_H 1

// Include files
#include "GaudiKernel/RndmGenerators.h"

/** @class RandomForGenerator RandomForGenerator.h Generators/RandomForGenerator.h
 *  Simple class to interface with Gaudi Random generator
 *
 *  @author Patrick Robbe
 *  @date   2007-10-10
 */
class RandomForGenerator {
public:
  virtual ~RandomForGenerator() {};

  static Rndm::Numbers & getNumbers() ;

  static double flat( ) ;

protected:

private:
  static Rndm::Numbers s_randgaudi ;
};
#endif // GENERATORS_RANDOMFORGENERATOR_H
