/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: HepMCUtils.h,v 1.8 2008-07-23 17:21:55 cattanem Exp $
#ifndef GENERATORS_IFULLGENEVENTCUTTOOL_H 
#define GENERATORS_IFULLGENEVENTCUTTOOL_H 1

// This include has been moved to Event/GenEvent package.
// This file is provided for backward compatibility.
#warning "You should now include MCInterfaces/IFullGenEventCutTool.h instead"
#include "MCInterfaces/IFullGenEventCutTool.h"

#endif // GENERATORS_IFULLGENEVENTCUTTOOL_H
