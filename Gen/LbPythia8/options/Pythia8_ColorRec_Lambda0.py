###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import Generation
from Configurables import MinimumBias
from Configurables import Inclusive
from Configurables import ( SignalPlain, SignalRepeatedHadronization, Special )
from Configurables import Pythia8Production

commandsTuning_Lambda0 = [
    'MultipartonInteractions:pT0Ref = 2.4188',
    'BeamRemnants:remnantMode = 1',
    'ColourReconnection:mode  = 1',
    'ColourReconnection:lambdaForm = 0',
    'ColourReconnection:allowDoubleJunRem = off',
    'ColourReconnection:timeDilationPar = 1.9667',
    'ColourReconnection:m0          = 0.9021',
    'ColourReconnection:junctionCorrection = 0.8399'
    'StringFlav:mesonSvector        = 0.3819',
    'StringFlav:probQQtoQ           = 0.1086',
    'StringFlav:probStoUD           = 0.3590',
    'StringFlav:probSQtoQQ          = 0.6451',
    ]

Pythia8TurnOffFragmentation = [ "HadronLevel:all = off" ]

gen = Generation("Generation")

gen.addTool( MinimumBias , name = "MinimumBias" )
gen.MinimumBias.ProductionTool = "Pythia8Production"
gen.MinimumBias.addTool( Pythia8Production , name = "Pythia8Production" )
gen.MinimumBias.Pythia8Production.Tuning = "LHCbDefault.cmd"
gen.MinimumBias.Pythia8Production.Commands += commandsTuning_Lambda0

gen.addTool( Inclusive , name = "Inclusive" )
gen.Inclusive.ProductionTool = "Pythia8Production"
gen.Inclusive.addTool( Pythia8Production , name = "Pythia8Production" )
gen.Inclusive.Pythia8Production.Tuning = "LHCbDefault.cmd"
gen.Inclusive.Pythia8Production.Commands += commandsTuning_Lambda0

gen.addTool( SignalPlain , name = "SignalPlain" )
gen.SignalPlain.ProductionTool = "Pythia8Production"
gen.SignalPlain.addTool( Pythia8Production , name = "Pythia8Production" )
gen.SignalPlain.Pythia8Production.Tuning = "LHCbDefault.cmd"
gen.SignalPlain.Pythia8Production.Commands += commandsTuning_Lambda0

gen.addTool( SignalRepeatedHadronization , name = "SignalRepeatedHadronization" )
gen.SignalRepeatedHadronization.ProductionTool = "Pythia8Production"
gen.SignalRepeatedHadronization.addTool( Pythia8Production , name = "Pythia8Production" )
gen.SignalRepeatedHadronization.Pythia8Production.Tuning = "LHCbDefault.cmd"
gen.SignalRepeatedHadronization.Pythia8Production.Commands += Pythia8TurnOffFragmentation
gen.SignalRepeatedHadronization.Pythia8Production.Commands += commandsTuning_Lambda0

gen.addTool( Special , name = "Special" )
gen.Special.ProductionTool = "Pythia8Production"
gen.Special.addTool( Pythia8Production , name = "Pythia8Production" )
gen.Special.Pythia8Production.Tuning = "LHCbDefault.cmd"
gen.Special.Pythia8Production.Commands += commandsTuning_Lambda0
gen.Special.PileUpProductionTool = "Pythia8Production/Pythia8PileUp"
gen.Special.addTool(Pythia8Production, name = "Pythia8PileUp")
gen.Special.Pythia8PileUp.Tuning = "LHCbDefault.cmd"
gen.Special.Pythia8PileUp.Commands += commandsTuning_Lambda0
gen.Special.ReinitializePileUpGenerator  = False

# Use same generator and configuration for spillover
from Configurables import Gauss
spillOverList = Gauss().getProp("SpilloverPaths")
for slot in spillOverList:
    genSlot = Generation("Generation"+slot)
    genSlot.addTool(MinimumBias, name = "MinimumBias")
    genSlot.MinimumBias.ProductionTool = "Pythia8Production"
    genSlot.MinimumBias.addTool(Pythia8Production, name = "Pythia8Production")
    genSlot.MinimumBias.Pythia8Production.Commands += commandsTuning_Lambda0
    genSlot.MinimumBias.Pythia8Production.Tuning = "LHCbDefault.cmd"




