###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import Generation, Gauss, MinimumBias, Pythia8Production
from Configurables import Special, Inclusive, SignalPlain, SignalRepeatedHadronization
from GaudiKernel import SystemOfUnits
import math

# Table of known ions
__ion_pdg_id__ = {
    'Pb': 1000822080,
    'Ar': 1000180400,
    'p': 2212,
    'Ne': 1000100200,
    'He': 1000020040,
    'Kr': 1000360840,
    'Xe': 1000541320}

# Mass table
__ion_mass__ = {
    'Pb': 207.6,
    'Ar': 39.948,
    'p': 0.938272046,
    'Ne': 20.1797,
    'He': 4.002602,
    'Kr': 83.798,
    'Xe': 131.293}


# Get some object
gen = Generation("Generation")
gauss = Gauss()

# Default Pythia8 configuration for LHCb
gen.addTool( MinimumBias , name = "MinimumBias" )
gen.MinimumBias.ProductionTool = "Pythia8Production"
gen.MinimumBias.addTool( Pythia8Production , name = "Pythia8Production" )
gen.MinimumBias.Pythia8Production.Tuning = "LHCbDefault.cmd"

gen.addTool( Inclusive , name = "Inclusive" )
gen.Inclusive.ProductionTool = "Pythia8Production"
gen.Inclusive.addTool( Pythia8Production , name = "Pythia8Production" )
gen.Inclusive.Pythia8Production.Tuning = "LHCbDefault.cmd"

gen.addTool( SignalPlain , name = "SignalPlain" )
gen.SignalPlain.ProductionTool = "Pythia8Production"
gen.SignalPlain.addTool( Pythia8Production , name = "Pythia8Production" )
gen.SignalPlain.Pythia8Production.Tuning = "LHCbDefault.cmd"

gen.addTool( SignalRepeatedHadronization , name = "SignalRepeatedHadronization" )
gen.SignalRepeatedHadronization.ProductionTool = "Pythia8Production"
gen.SignalRepeatedHadronization.addTool( Pythia8Production , name = "Pythia8Production" )
gen.SignalRepeatedHadronization.Pythia8Production.Tuning = "LHCbDefault.cmd"

gen.addTool( Special , name = "Special" )
gen.Special.ProductionTool = "Pythia8Production"
gen.Special.addTool( Pythia8Production , name = "Pythia8Production" )
gen.Special.Pythia8Production.Tuning = "LHCbDefault.cmd"
gen.Special.PileUpProductionTool = "Pythia8Production/Pythia8PileUp"
gen.Special.addTool(Pythia8Production, name = "Pythia8PileUp")
gen.Special.Pythia8PileUp.Tuning = "LHCbDefault.cmd"
gen.Special.ReinitializePileUpGenerator  = False

# Use same generator and configuration for spillover
spillOverList = gauss.getProp("SpilloverPaths")
for slot in spillOverList:
    genSlot = Generation("Generation"+slot)
    genSlot.addTool(MinimumBias, name = "MinimumBias")
    genSlot.MinimumBias.ProductionTool = "Pythia8Production"
    genSlot.MinimumBias.addTool(Pythia8Production, name = "Pythia8Production")
    genSlot.MinimumBias.Pythia8Production.Tuning = "LHCbDefault.cmd"


# Get beam 4-momentum
def getBeamMomentum(beam):

    # Get the crossings angles.
    hCrossingAngle = gauss.getProp('BeamHCrossingAngle')
    hBeamlineAngle = gauss.getProp('BeamLineAngles')[0]
    vCrossingAngle = gauss.getProp('BeamVCrossingAngle')
    vBeamlineAngle = gauss.getProp('BeamLineAngles')[1]

    # Calculate the momentum.
    sn = -1.0 if beam == 2 else 1.0
    p  = math.fabs(gauss.getProp('%sMomentum' % ('B2' if beam == 2 else 'Beam'))) / SystemOfUnits.GeV
    px = p * math.sin(hCrossingAngle + sn*hBeamlineAngle)
    py = p * math.sin(vCrossingAngle + sn*vBeamlineAngle)
    pz = math.sqrt(p**2 - px**2 -py**2)
    if beam == 2:  pz = -pz
    # compute the beam energy
    m = 0.938 # m_proton \simeq m_A/A
    e = math.sqrt(px**2 + py**2 + pz**2 + m**2)
    return px, py, pz, e

# Set collision and boost parameters
def configureHICollisions(): # Get beam momentum and CMS sqrt(s_NN) in GeV for Pythia

    pxA, pyA, pzA, eProjectile = getBeamMomentum(1)
    pxB, pyB, pzB, eTarget     = getBeamMomentum(2)

    # Set the properties in Pythia
    for genType in ["Inclusive", "SignalPlain", "SignalRepeatedHadronization", "MinimumBias", "Special"]:
        getattr(gen, genType).Pythia8Production.Commands += [
            "Beams:idA = %s" % __ion_pdg_id__[gauss.getProp('B1Particle')],
            "Beams:idB = %s" % __ion_pdg_id__[gauss.getProp('B2Particle')],
            'Beams:eA = %s' % eProjectile,
            'Beams:eB = %s' % eTarget,
            "Beams:frameType = 2"]
        if genType == "Special":
            getattr(gen, genType).Pythia8PileUp.Commands += [
                "Beams:idA = %s" % __ion_pdg_id__[gauss.getProp('B1Particle')],
                "Beams:idB = %s" % __ion_pdg_id__[gauss.getProp('B2Particle')],
                'Beams:eA = %s' % eProjectile,
                'Beams:eB = %s' % eTarget,
                "Beams:frameType = 2"]
    for slot in spillOverList:
        genSlot = Generation("Generation"+slot)
        genSlot.MinimumBias.Pythia8Production.Commands += [
            "Beams:idA = %s" % __ion_pdg_id__[gauss.getProp('B1Particle')],
            "Beams:idB = %s" % __ion_pdg_id__[gauss.getProp('B2Particle')],
            'Beams:eA = %s' % eProjectile,
            'Beams:eB = %s' % eTarget,
            "Beams:frameType = 2"]

from Gaudi.Configuration import appendPostConfigAction
appendPostConfigAction(configureHICollisions)

# Turn on Heavy-Ion Pythia
for genType in ["Inclusive", "SignalPlain", "SignalRepeatedHadronization", "MinimumBias", "Special"]:
    getattr(gen, genType).Pythia8Production.Commands += [
        "HeavyIon:mode = 2",
        "SoftQCD:all = on",
        "Init:showProcesses = on"]
    if genType == "Special":
        getattr(gen, genType).Pythia8PileUp.Commands += [
            "HeavyIon:mode = 2",
            "SoftQCD:all = on",
            "Init:showProcesses = on"]

for slot in spillOverList:
    genSlot = Generation("Generation"+slot)
    genSlot.MinimumBias.Pythia8Production.Commands += [
        "HeavyIon:mode = 2",
        "SoftQCD:all = on",
        "Init:showProcesses = on"]
