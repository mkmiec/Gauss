###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Gen/LbPythia
------------
#]=======================================================================]

string(APPEND CMAKE_Fortran_FLAGS " -fsecond-underscore")

gaudi_add_library(LbPythiaLib
    SOURCES
        src/Lib/Hepeup.cpp
        src/Lib/Pydat1.cpp
        src/Lib/Pydat2.cpp
        src/Lib/Pydat3.cpp
        src/Lib/Pydatr.cpp
        src/Lib/Pyint2.cpp
        src/Lib/Pyint5.cpp
        src/Lib/Pyint6.cpp
        src/Lib/Pymssm.cpp
        src/Lib/Pypars.cpp
        src/Lib/Pyssmt.cpp
        src/Lib/Pysubs.cpp
        src/Lib/Pythia.cpp
        src/Lib/PythiaProduction.cpp
        src/Lib/ReadFile.cpp
        src/Lib/ReadLHE.cpp
        src/Lib/SetUserProcess.F
        src/Lib/ghepeup.F
        src/Lib/gpydat1.F
        src/Lib/gpydat2.F
        src/Lib/gpydat3.F
        src/Lib/gpydatr.F
        src/Lib/gpyint2.F
        src/Lib/gpyint5.F
        src/Lib/gpyint6.F
        src/Lib/gpymssm.F
        src/Lib/gpypars.F
        src/Lib/gpyssmt.F
        src/Lib/gpysubs.F
        src/Lib/initpyblock.F
        src/Lib/lherestore.F
        src/Lib/lhesave.F
        src/Lib/lheupevnt.F
        src/Lib/lheupinit.F
        src/Lib/lunhep.F
        src/Lib/lutran.F
        src/Lib/pyaddp.F
        src/Lib/setbeam.F
    LINK
        PUBLIC
            Gauss::GeneratorsLib
            Gauss::pythia6forgauss
            HepMC::fio
)


gaudi_add_module(LbPythia
    SOURCES
        src/Components/JetProduction.cpp
        src/Components/PythiaProductionFactory.cpp
        src/Components/ReadLHEfileProduction.cpp
    LINK
        Gauss::LbPythiaLib
)
