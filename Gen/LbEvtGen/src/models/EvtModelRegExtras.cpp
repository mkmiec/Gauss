/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "EvtGenModels/EvtModelRegExtras.hh"

#include "EvtGenModels/EvtBToKStarllDurham07.hh"
#include "EvtGenModels/EvtDTohhhh.hh"
#include "EvtGenModels/EvtXLL.hh"
#include "EvtGenModels/EvtLbAmpGen.hh"
#include "EvtGenModels/EvtBnoCB0to4piCP.hh"
#include "EvtGenModels/EvtBnoCBptoKshhh.hh"
#include "EvtGenModels/EvtBnoCBpto3hpi0.hh"

//we use unique_ptr here to show explicit transfer of ownership
std::unique_ptr<const EvtModelList> EvtModelRegExtras::getModels() {

    EvtModelList* models { new EvtModelList{} };
    if (models == nullptr) {
        return std::unique_ptr<const EvtModelList>{};
    }

    models->push_back(new EvtBToKStarllDurham07());
    models->push_back(new EvtDTohhhh());
    models->push_back(new EvtXLL());
    models->push_back(new EvtLbAmpGen());
    models->push_back(new EvtBnoCB0to4piCP());
    models->push_back(new EvtBnoCBptoKshhh());
    models->push_back(new EvtBnoCBpto3hpi0());

    return std::unique_ptr<const EvtModelList>(models);
}
