/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#if 0
*       for IBM / RT
* This pilot patch was created from kernirt.car patch _kirt
* This directory was created from kernirt.car patch qmirt
* This directory was created from kernfor.car patch qmirt
*                 Normal Unix system machine
*               Posix call for setjmp/longjmp
*               MIL standard routines, IBITS, MVBITS, etc
*               ISA standard routines, ISHFT, IOR, etc
*                 External names with underscore
*                 IEEE floating point
*                 Hollerith constants exist
*              EQUIVALENCE Hollerith/Character ok
*              Orthodox Hollerith storage left to right
*               running Unix
*              Posix call for signal
#endif
#ifndef CERNLIB_QMIRT
#define CERNLIB_QMIRT
#endif
#ifndef CERNLIB_QSIGJMP
#define CERNLIB_QSIGJMP
#endif
#ifndef CERNLIB_QMILSTD
#define CERNLIB_QMILSTD
#endif
#ifndef CERNLIB_QISASTD
#define CERNLIB_QISASTD
#endif
#ifndef CERNLIB_QX_SC
#define CERNLIB_QX_SC
#endif
#ifndef CERNLIB_QIEEE
#define CERNLIB_QIEEE
#endif
#ifndef CERNLIB_QORTHOLL
#define CERNLIB_QORTHOLL
#endif
#ifndef CERNLIB_QS_UNIX
#define CERNLIB_QS_UNIX
#endif
#ifndef CERNLIB_QINTZERO
#define CERNLIB_QINTZERO
#endif
#ifndef CERNLIB_QSIGPOSIX
#define CERNLIB_QSIGPOSIX
#endif
