/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENCUTS_SEMILEPCUTFORADS_H
#define GENCUTS_SEMILEPCUTFORADS_H 1

// Include files
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "MCInterfaces/IGenCutTool.h"
#include "GaudiKernel/Transform4DTypes.h"

/** @class SemilepCutForADS SemilepCutForADS.h
 *
 *  Tool to keep Bu -> D0 mu nu events with Bu_M > a value
 *  and particles in LHCb acceptance.
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Paolo Gandini
 *  @date   2011-02-18
 */

class SemilepCutForADS : public extends<GaudiTool,IGenCutTool> {
 public:
  /// Standard constructor
  using extends::extends;

  bool applyCut( ParticleVector & theParticleVector ,
                 const HepMC::GenEvent * theEvent ,
                 const LHCb::GenCollision * theCollision ) const override;

 private:
  /** Study a particle a returns true when all stable daughters
   *  are in LHCb acceptance
   */
  bool passCuts( const HepMC::GenParticle * theSignal ) const ;

  Gaudi::Property<double> m_chargedThetaMin {this,"ChargedThetaMin",10 * Gaudi::Units::mrad,"ChargedThetaMin"} ;
  Gaudi::Property<double> m_chargedThetaMax {this,"ChargedThetaMax",400 * Gaudi::Units::mrad,"ChargedThetaMax"} ;
  Gaudi::Property<double> m_neutralThetaMin {this,"NeutralThetaMin",5 * Gaudi::Units::mrad,"NeutralThetaMin"} ;
  Gaudi::Property<double> m_neutralThetaMax {this,"NeutralThetaMax",400 * Gaudi::Units::mrad,"NeutralThetaMax"} ;
  Gaudi::Property<double> m_massMIN {this,"MassMin",4600*Gaudi::Units::MeV,"MassMin"}         ;
};
#endif // GENCUTS_SEMILEPCUTFORADS_H
