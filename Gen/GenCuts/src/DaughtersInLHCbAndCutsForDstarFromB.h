/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENCUTS_DAUGHTERSINLHCBANDCUTSFORDSTARFROMB_H
#define GENCUTS_DAUGHTERSINLHCBANDCUTSFORDSTARFROMB_H 1

// Include files
#include "DaughtersInLHCbAndCutsForDstar.h"

/** @class DaughtersInLHCbAndCutsForDstarFromB DaughtersInLHCbAndCutsForDstarFromB.h
 *
 *  Tool to select D* particles from a b-hadron with pT cuts
 *
 *  @author Adam Morris
 *  @date   2018-03-30
 */
class DaughtersInLHCbAndCutsForDstarFromB : public DaughtersInLHCbAndCutsForDstar, virtual public IGenCutTool {
public:
  /// Standard constructor
  DaughtersInLHCbAndCutsForDstarFromB( const std::string & type,
                                     const std::string & name,
                                     const IInterface * parent ) ;

  /// Initialization
  StatusCode initialize() override;

  /** Check that the signal D* satisfies the cuts in
   *  SignalIsFromBDecay and DaughtersInLHCbAndCutsForDstar.
   *  Implements IGenCutTool::applyCut.
   */
  bool applyCut( ParticleVector & theParticleVector,
                 const HepMC::GenEvent * theEvent,
                 const LHCb::GenCollision * theCollision ) const override;

private:
  /// From a b cut tool
  const IGenCutTool * m_fromBcuts = nullptr;
} ;
#endif // GENCUTS_DAUGHTERSINLHCBANDCUTSFORDSTARFROMB_H
