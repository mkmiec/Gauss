/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENCUTS_DMUCASCADEINACC_H
#define GENCUTS_DMUCASCADEINACC_H 1

// Include files
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "MCInterfaces/IFullGenEventCutTool.h"
#include "MCInterfaces/IGenCutTool.h"
#include "GaudiKernel/Transform4DTypes.h"

/** @class DMuCascadeInAcc DMuCascadeInAcc.h
 *
 *  Tool to filter generic Xb->Xc(->KKPi)Xc(->mu X) events and keep the particles in LHCb acceptance.
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Adam Webber
 *  @date   2011-07-26
 */

class DMuCascadeInAcc: public extends<GaudiTool,IGenCutTool> {
 public:
  /// Standard constructor
  using extends::extends;

  bool applyCut( ParticleVector & theParticleVector ,
                 const HepMC::GenEvent * theEvent ,
                 const LHCb::GenCollision * theCollision ) const override;

 private:
  /** Check the correct particles are in LHCb acceptance.
   */
  bool passCuts( const HepMC::GenParticle * theSignal
              , bool &hasMuon, bool &hasHadrons
              , bool &hasMuonAndHadrons  ) const ;

  Gaudi::Property<double> m_chargedThetaMin{this,"ChargedThetaMin",10   * Gaudi::Units::mrad,"Minimum value of angle around z-axis for charged children"} ;
  Gaudi::Property<double> m_chargedThetaMax{this,"ChargedThetaMax",400  * Gaudi::Units::mrad,"Maximum value of angle around z-axis for charged children"} ;
  Gaudi::Property<double> m_muonptmin{this,"MuonPtMin",0.5  * Gaudi::Units::GeV,"Muon Pt Min"};
  Gaudi::Property<double> m_muonpmin{this,"MuonPMin",1.0  * Gaudi::Units::GeV,"Muon P Min"};
  Gaudi::Property<double> m_hadronptmin{this,"HadronPtMin",0.25 * Gaudi::Units::GeV,"Hadron Pt Min"};
  Gaudi::Property<double> m_hadronpmin{this,"HadronPMin",2.0  * Gaudi::Units::GeV,"Hadron P Min"};
};
#endif // GENCUTS_DMUCASCADEINACC_H
