/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "DaughtersInLHCbAndCutsForDstarFromB.h"

// from Kernel
#include "Kernel/ParticleID.h"

// from Generators
#include "GenEvent/HepMCUtils.h"
#include "MCInterfaces/IDecayTool.h"

// from STL
#include <algorithm>

//-----------------------------------------------------------------------------
// Implementation file for class : DaughtersInLHCbAndCutsForDstarFromB
//
// 2018-03-30 Adam Morris
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory

DECLARE_COMPONENT( DaughtersInLHCbAndCutsForDstarFromB )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
DaughtersInLHCbAndCutsForDstarFromB::DaughtersInLHCbAndCutsForDstarFromB( const std::string& type,
                                                                          const std::string& name,
                                                                          const IInterface* parent )
  : DaughtersInLHCbAndCutsForDstar( type, name, parent ) {
  declareInterface< IGenCutTool >( this ) ;
}


//=============================================================================
// initialize
//=============================================================================
StatusCode DaughtersInLHCbAndCutsForDstarFromB::initialize()
{
  const StatusCode sc = DaughtersInLHCb::initialize();
  if ( sc.isFailure() ) return sc;

  // load the tool to check if the signal is ultimately from a b
  m_fromBcuts = tool<IGenCutTool>( "SignalIsFromBDecay", this );

  return sc;
}

//=============================================================================
// Acceptance function
//=============================================================================
bool DaughtersInLHCbAndCutsForDstarFromB::applyCut( ParticleVector & theParticleVector ,
                                                    const HepMC::GenEvent * theEvent ,
                                                    const LHCb::GenCollision * theHardInfo )
  const
{
  return ( m_fromBcuts->applyCut(theParticleVector,theEvent,theHardInfo) &&
           DaughtersInLHCbAndCutsForDstar::applyCut(theParticleVector,theEvent,theHardInfo) );
}

