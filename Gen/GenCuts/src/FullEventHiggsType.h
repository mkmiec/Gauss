/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENCUTS_FullEventHiggsType_H
#define GENCUTS_FullEventHiggsType_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

// from LHCb
#include "Kernel/IParticlePropertySvc.h"

// from Generators
#include "MCInterfaces/IFullGenEventCutTool.h"

/** @class FullEventHiggsType FullEventHiggsType.h component/FullEventHiggsType.h
 *
 *  Tool to select events with, 1 or 2 b quarks in the acceptance coming
 *  from a given mother, and/or 1 or 2 leptons in the acceptance with given
 *  minimal pt and comming (or not) from
 *  a W or Z boson
 *
 *  @author Neal Gauvin
 *  @date   2009-10-12
 */
class FullEventHiggsType : public extends<GaudiTool, IFullGenEventCutTool> {
public:
  /// Standard constructor
  using extends::extends;

  /// initialize function
  StatusCode initialize( ) override;

  bool studyFullEvent( LHCb::HepMCEvents * ,
                       LHCb::GenCollisions * ) const override;

private:
  /// Return true if the particle is a b quark
  bool Isb( const HepMC::GenParticle * p ) const ;

  /// Return true if the particle is a lepton
  bool IsLepton( const HepMC::GenParticle * p ) const ;

  /// Max theta angle to define the LHCb acceptance
  Gaudi::Property<double> m_thetaMax {this,"ThetaMax",400 * Gaudi::Units::mrad ,"Max theta angle to define the LHCb acceptance"};

  /// Number of leptons required in the acceptance
  Gaudi::Property<int> m_nbLepton{this,"NumberOfLepton",1,"Number of leptons required in the acceptance"};

  /// Type of lepton requiered in the acceptance
  Gaudi::Property< std::vector< std::string > > m_TypeLepton{this,"TypeOfLepton",{"e+","mu+"} ,"Type of lepton requiered in the acceptance"};

  /// Minimum pT of the lepton
  Gaudi::Property<double> m_ptMin {this,"LeptonPtMin",10 * Gaudi::Units::GeV,"Minimum pT of the lepton"};

  /// If true, the lepton is required to come from a given mother
  Gaudi::Property<bool> m_leptonFromMother{this,"LeptonIsFromMother",true,"If true, the lepton is required to come from a given mother"};

  /// List of mother of the lepton
  Gaudi::Property< std::vector< std::string > > m_motheroflepton{this,"MotherOfLepton",{"W+","Z0"},"List of mother of the lepton"};

  /// Number of quarks required to be in the acceptance
  Gaudi::Property<int> m_nbbquarks{this,"NumberOfbquarks",1,"Number of quarks required to be in the acceptance"};

  /// PDG id of the mother of the b quarks
  Gaudi::Property< std::string> m_motherofb_id{this,"MotherOfThebquarks", "H_10" ,"PDG id of the mother of the b quarks"};

  /// Substitute of the b quark, if any reqested
  Gaudi::Property<std::string> m_subb{this,"SubstitutebBy", "No","Substitute of the b quark, if any reqested"};

  int m_motherofb_pid{0};
  int m_b_pid{0};

  const LHCb::IParticlePropertySvc* m_ppSvc;

};
#endif // GENCUTS_FullEventHiggsType_H
