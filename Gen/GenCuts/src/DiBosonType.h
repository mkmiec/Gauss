/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENCUTS_DiBosonType_H
#define GENCUTS_DiBosonType_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IParticlePropertySvc.h"
#include "GaudiKernel/SystemOfUnits.h"

// from Generators
#include "MCInterfaces/IGenCutTool.h"

/** @class DiBosonType DiBosonType.h 
 *
 *  Tool to select events with, three or four leptons from di-boson decay 
 *  in the acceptance, with given minimal pt and comming (or not) from
 *  a W or Z boson
 *
 *  @author Hang Yin
 *  @date   2020-11-18
 */
class DiBosonType : public extends<GaudiTool,IGenCutTool> {
public:
  /// Standard constructor
  using extends::extends;

  /// initialize function
  StatusCode initialize( ) override;

  /* Accept events following criteria given in options.
   * Implements IGenCutTool::applyCut
   */
  bool applyCut( ParticleVector & theParticleVector ,
                 const HepMC::GenEvent * theEvent ,
                 const LHCb::GenCollision * theCollision ) const override;

private:
  /// Return true if the particle is a lepton
  bool IsLepton( const HepMC::GenParticle * p ) const ;

  /// Return true if the particle is a EW boson
  bool IsBoson( const HepMC::GenParticle * p ) const ;

  /// Max and Min theta angle to define the LHCb acceptance
  Gaudi::Property<double> m_thetaMax{this,"ThetaMax",400 * Gaudi::Units::mrad,"Max theta angle to define the LHCb acceptance"} ;
  Gaudi::Property<double> m_thetaMin{this,"ThetaMin",10 * Gaudi::Units::mrad,"Min theta angle to define the LHCb acceptance"} ;

  /// Number of leptons required in the acceptance
  Gaudi::Property<int> m_nLepton{this,"NumberOfLepton",4,"Number of leptons required in the acceptance"};

  /// Number of bosons 
  Gaudi::Property<int> m_nBoson{this,"NumberOfBoson",2,"Number of bosons "};

  /// Type of lepton requiered in the acceptance
  Gaudi::Property<std::vector< std::string > > m_TypeLepton{this,"TypeOfLepton",{"e+","mu+"},"Type of lepton requiered in the acceptance"};

  /// Minimum pT of the lepton
  Gaudi::Property<double> m_ptMin{this,"LeptonPtMin",10 * Gaudi::Units::GeV,"Minimum pT of the lepton"};

  /// If true, the lepton is required to come from a given mother
  Gaudi::Property<bool> m_leptonFromMother{this,"LeptonIsFromMother",true,"If true, the lepton is required to come from a given mother"};

  /// If true, the boson is required to come from a given mother
  Gaudi::Property<bool> m_bosonFromMother{this,"BosonIsFromMother",false,"If true, the boson is required to come from a given mother"};

  /// List of mother of the lepton
  std::vector< std::string > m_motheroflepton{"W+","Z0"};

  /// Minimum mass of the Mother of the lepton
  Gaudi::Property<double> m_MinMass{this,"MotherOfLeptonMinMass",-1.,"Minimum mass of the Mother of the lepton"} ;

  /// PDG id of the mother of the W/Z boson 
  Gaudi::Property<std::string> m_motherofboson_id{this,"MotherOfBoson","H_10","PDG id of the mother of the W/Z boson "};
  int m_motherofboson_pid{0};

  LHCb::IParticlePropertySvc* m_ppSvc{nullptr};
};
#endif // GENCUTS_DiBosonType_H
