/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENCUTS_DAUGHTERSINLHCBANDCUTSFORDINSLBDECAYS_H
#define GENCUTS_DAUGHTERSINLHCBANDCUTSFORDINSLBDECAYS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "MCInterfaces/IGenCutTool.h"

/** @class DaughtersInLHCbAndCutsForDinSLBdecays DaughtersInLHCbAndCutsForDinSLBdecays.h
 *
 *  Tool to keep events with daughters from signal particles
 *  in LHCb and with p and pt cuts on D0 daughters and muon.
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Maurizio Martinelli
 *  @date   2013-08-19
 */
class DaughtersInLHCbAndCutsForDinSLBdecays : public extends<GaudiTool,IGenCutTool> {
 public:
  /// Standard constructor
  using extends::extends;

  /** Accept events with daughters in LHCb and p/pt cuts on D0 daughters
   *  (defined by min and max angles, different values for charged and neutrals)
   *  Implements IGenCutTool::applyCut.
   */
  bool applyCut( ParticleVector & theParticleVector ,
                 const HepMC::GenEvent * theEvent ,
                 const LHCb::GenCollision * theCollision ) const override;

 private:
  /** Study a particle a returns true when all stable daughters
   *  are in LHCb AndWithMinP
   */
  bool passCuts( const HepMC::GenParticle * theSignal ) const ;

  /** Check particle is in LHCb acceptance
   */
  bool InLHCbAcceptance( const HepMC::GenParticle * stable, const double firstpz ) const ;

  /** Momentum Cut function
   */
  bool momentumCut( const HepMC::GenParticle *, double ) const ;

  /** Calculate proper time
   */
  double ProperTime( const HepMC::GenParticle * resonance ) const;

  /**  Minimum value of angle around z-axis for charged daughters
   */
  Gaudi::Property<double> m_chargedThetaMin{this,"ChargedThetaMin",10 * Gaudi::Units::mrad,"Minimum value of angle around z-axis for charged daughters"} ;

  /** Maximum value of angle around z-axis for charged daughters
   */
  Gaudi::Property<double> m_chargedThetaMax{this,"ChargedThetaMax",400 * Gaudi::Units::mrad,"Maximum value of angle around z-axis for charged daughters"} ;

  /** Minimum value of angle around z-axis for neutral daughters
   */
  Gaudi::Property<double> m_neutralThetaMin{this,"NeutralThetaMin",5 * Gaudi::Units::mrad,"Minimum value of angle around z-axis for neutral daughters"} ;

  /** Maximum value of angle around z-axis for neutral daughters
   */
  Gaudi::Property<double> m_neutralThetaMax{this,"NeutralThetaMax",400 * Gaudi::Units::mrad,"Maximum value of angle around z-axis for neutral daughters"} ;

  /** cut value of minimum (D0mu) invariant mass
   */
  Gaudi::Property<double> m_md0muMin{this,"MassMin",0*Gaudi::Units::MeV,"cut value of minimum (D0mu) invariant mass"};

  /** cut value of D0 pt
   */
  Gaudi::Property<double> m_d0PtMin{this,"D0PtMin",0 * Gaudi::Units::MeV,"cut value of D0 pt"} ;

  /** cut on D0 ctau
   */
  Gaudi::Property<double> m_d0ctauMin{this,"D0PropTimeMin",0 * Gaudi::Units::mm,"cut on D0 ctau"} ;

  /** cut value on daughters min pt
   */
  Gaudi::Property<double> m_daughtersPtMin{this,"D0DaughtersPtMin",0 * Gaudi::Units::MeV,"cut value on daughters min pt"} ;

  /** cut value on daughters max pt
   */
  //  double m_daughtersPtMaxCut ;

  /** cut value on daughters min p
   */
  Gaudi::Property<double> m_daughtersPMin{this,"D0DaughtersPMin",0 * Gaudi::Units::MeV,"cut value on daughters min p"} ;

  /** cut value on minimum muon p
   */
  Gaudi::Property<double> m_muonPMin{this,"MuonPMin",0 * Gaudi::Units::MeV,"cut value on minimum muon p"} ;

  /** cut value on minimum muon pt
   */
  Gaudi::Property<double> m_muonPtMin{this,"MuonPtMin",0 * Gaudi::Units::MeV,"cut value on minimum muon pt"} ;
};
#endif // GENCUTS_DAUGHTERSINLHCDANDCUTSFORDINSLBDECAYS_H
