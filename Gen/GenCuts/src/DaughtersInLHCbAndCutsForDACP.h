/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENCUTS_DAUGHTERSINLHCBANDCUTSFORDACP_H
#define GENCUTS_DAUGHTERSINLHCBANDCUTSFORDACP_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "MCInterfaces/IGenCutTool.h"

/** @class DaughtersInLHCbAndCutsForDACP DaughtersInLHCbAndCutsForDACP.h
 *
 *  Tool to keep events with daughters from signal particles in LHCb
 *  and with cuts on:
 *     (Dst p, D0 pT) plane
 *     (Dst p - D0 p, Dst pT - D0 pT) plane
 *     D0 tau
 *     D0 flight distance from origin vertex
 *     D0 daughter p and pT
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Chris Thomas, based on DaughtersInLHCbAndCutsForDstar
 *  @date   2012-05-14
 */
class DaughtersInLHCbAndCutsForDACP : public extends<GaudiTool, IGenCutTool> {
 public:
  /// Standard constructor
  using extends::extends;
  
  /** Accept events with daughters in LHCb and cuts outlined above
   *  Implements IGenCutTool::applyCut.
   */
  bool applyCut( ParticleVector & theParticleVector ,
                 const HepMC::GenEvent * theEvent ,
                 const LHCb::GenCollision * theCollision ) const override;

 private:
  /** Study a particle a returns true when all stable daughters
   *  are in LHCb AndCutsForDACP
   */
  bool passCuts( const HepMC::GenParticle * theSignal ) const ;

  /** Momentum Cut function
   *
   */
  bool momentumCut( const HepMC::GenParticle *, double ) const ;

  // Minimum value of angle around z-axis for charged daughters
  Gaudi::Property<double> m_chargedThetaMin{this,"ChargedThetaMin",10 * Gaudi::Units::mrad,"Minimum value of angle around z-axis for charged daughters"} ;

  // Maximum value of angle around z-axis for charged daughters
  Gaudi::Property<double> m_chargedThetaMax{this,"ChargedThetaMax",400 * Gaudi::Units::mrad,"Maximum value of angle around z-axis for charged daughters"} ;

  // Minimum value of angle around z-axis for neutral daughters
  Gaudi::Property<double> m_neutralThetaMin{this,"NeutralThetaMin",5 * Gaudi::Units::mrad,"Minimum value of angle around z-axis for neutral daughters"} ;

  // Maximum value of angle around z-axis for neutral daughters
  Gaudi::Property<double> m_neutralThetaMax{this,"NeutralThetaMax",400 * Gaudi::Units::mrad,"Maximum value of angle around z-axis for neutral daughters"} ;

  //
  //Cuts on (D* p, D* pT) plane
  //

  Gaudi::Property<double> m_scaleDst{this,"ScaleDst",0.1,"scaling between p and pT ranges"} ;
  Gaudi::Property<double> m_uDst{this,"uDst",6000.0 * Gaudi::Units::MeV,"x-coordinate of parabola base"} ;
  Gaudi::Property<double> m_vDst{this,"vDst",60000.0 * Gaudi::Units::MeV,"y-coordinate of parabola base"} ;
  Gaudi::Property<double> m_sinthetaDst{this,"sinthetaDst", 0.601815,"angle of rotation of parabola from vertical = thetaDst = 37.0 degrees"} ;
  Gaudi::Property<double> m_costhetaDst{this,"costhetaDst",0.798636,"angle of rotation of parabola from vertical = thetaDst = 37.0 degrees"} ;
  Gaudi::Property<double> m_kDst{this,"kDst",0.00009,"roughly proportional to inverse width of parabbola"} ;

  //
  //Cuts on (D* p - D0 p, D* pT - D0 pT) plane
  //

  Gaudi::Property<double> m_scaleDiff{this,"ScaleDiff",0.1,"scaling between x and y axes"} ;
  Gaudi::Property<double> m_uDiff{this,"uDiff",400.0 * Gaudi::Units::MeV,"x-coordinate of parabola base"} ;
  Gaudi::Property<double> m_vDiff{this,"vDiff",4500.0 * Gaudi::Units::MeV,"y-coordinate of parabola base"} ;
  Gaudi::Property<double> m_sinthetaDiff{this,"sinthetaDiff",0.615661,"angle of rotation of parabola from vertical = thetaDiff = 38.0 degrees"} ;
  Gaudi::Property<double> m_costhetaDiff{this,"costhetaDiff",0.788011,"angle of rotation of parabola from vertical = thetaDiff = 38.0 degrees"} ;
  Gaudi::Property<double> m_kDiff{this,"kDiff",0.0014,"roughly proportional to inverse width of parabola"} ;

  //
  //Cut on D0 tau
  //

  Gaudi::Property<double> m_D0_TAU{this,"D0_TAU",0.0005 * Gaudi::Units::ns,"Cut on D0 tau"} ;

  //
  //Cut on D0 flight distance from origin vertex
  //

  Gaudi::Property<double> m_D0_FD_ORIVX{this,"D0_FD_ORIVX",5.0 * Gaudi::Units::mm,"Cut on D0 flight distance from origin vertex"} ;

  //
  //Cuts on D0 daughter minimum pT, maximum pT and minimum p
  //

  Gaudi::Property<double> m_D0_daugHiPT{this,"D0_daug_HiPT",3000.0 * Gaudi::Units::MeV,"Cuts on D0 daughter maximum pT"} ;
  Gaudi::Property<double> m_D0_daugLoPT{this,"D0_daug_LoPT",1000.0 * Gaudi::Units::MeV,"Cuts on D0 daughter minimum pT"} ;
  Gaudi::Property<double> m_D0_daugLoP{this,"D0_daug_LoP",5000.0 * Gaudi::Units::MeV,"Cuts on D0 daughter minimum p"} ;

};
#endif // GENCUTS_DAUGHTERSINLHCDANDCUTSFORDACP_H
