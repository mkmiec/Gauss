/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
// local
#include "FullEventHiggsType.h"

// from Gaudi

// from HepMC
#include "HepMC/GenParticle.h"
#include "HepMC/GenEvent.h"

// from Kernel
#include "Kernel/ParticleID.h"
#include "Kernel/ParticleProperty.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FullEventHiggsType
//
// 2009-Oct-12 : Neal Gauvin
//
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory

DECLARE_COMPONENT( FullEventHiggsType )


//=============================================================================
// Initialise
//============================================================================
StatusCode FullEventHiggsType::initialize() {
  StatusCode sc = GaudiTool::initialize() ;
  if ( ! sc.isSuccess() ) return sc ;
  
	if ( ( "t" == m_motherofb_id.value() ) && ( 2 == m_nbbquarks.value() ) ) { // remplacer ici par les fonction de PID
		info() << "You want to have both b in the acceptance, " 
           << "check in pythia commands that you ask both top to decay in b" 
           << endmsg;
	}

	if ( ( 1 != m_nbLepton.value() ) && ( 2 != m_nbLepton.value() ) && ( 0 != m_nbLepton.value() ) ) {
		fatal() << "The only choices for NumberOfLepton property are 0, 1 or 2" 
            << endmsg;
		return StatusCode::FAILURE;
	}

	if ( ( 1 != m_nbbquarks.value() ) && ( 2 != m_nbbquarks.value() ) && ( 0 != m_nbbquarks.value() ) && 
       ( -1 != m_nbbquarks.value() ) ) {
		fatal() << "The only choices for NumberOfbquarks property are -1,0,1 or 2"
            << endmsg;
		return StatusCode::FAILURE;
	}
  m_ppSvc = svc<LHCb::IParticlePropertySvc>("LHCb::ParticlePropertySvc", true);
  m_motherofb_pid = abs( ((m_ppSvc->find( m_motherofb_id.value() ))->pdgID()).pid() );
  if( m_subb.value() == "No" ){
    m_b_pid = abs( ((m_ppSvc->find("b" ))->pdgID()).pid() );
  } else {
    m_b_pid = abs( ((m_ppSvc->find( m_subb.value() ))->pdgID()).pid() );
  }

	return sc ;
}

//=============================================================================
// b quark tagging
//=============================================================================
bool FullEventHiggsType::Isb( const HepMC::GenParticle * p) const {
  if ( abs(p->pdg_id())== m_b_pid ) {
		HepMC::GenVertex * thePV =  p->production_vertex() ;
		HepMC::GenVertex::particle_iterator iter ;
		for(iter = thePV->particles_begin( HepMC::parents);
        iter != thePV->particles_end(HepMC::parents); ++iter){
			if( abs( m_motherofb_pid ) == abs( (*iter)->pdg_id() )  ) return true;
		}
	}
	return false;
}

//=============================================================================
// look for lepton
//=============================================================================
bool FullEventHiggsType::IsLepton( const HepMC::GenParticle * p ) const {
  bool isalepton = false ;
  for ( std::vector< std::string >::const_iterator iPart = m_TypeLepton.value().begin();
              iPart != m_TypeLepton.value().end(); ++iPart ){
    std::string thepid = *iPart;
    if ( abs(p->pdg_id()) == abs( ((m_ppSvc->find(thepid))->pdgID()).pid() ) ) isalepton = true;
  }
  if( isalepton == true ){
		if ( ! m_leptonFromMother.value() ) return true ;
		else {
			HepMC::GenVertex * thePV =  p -> production_vertex() ;
			HepMC::GenVertex::particle_iterator iter ;
			for(iter = thePV -> particles_begin( HepMC::parents);
          iter != thePV -> particles_end(HepMC::parents); ++iter){
        for ( std::vector< std::string >::const_iterator iPart = m_motheroflepton.value().begin();
              iPart != m_motheroflepton.value().end(); ++iPart )
        {
          std::string thepid = *iPart;
          if(  abs( ((m_ppSvc->find(thepid))->pdgID()).pid() ) == abs( (*iter)->pdg_id() ) ) return true;
        }
			}
		}
	}
	return false;
}

//=============================================================================
// Accept function
//=============================================================================
bool FullEventHiggsType::studyFullEvent( LHCb::HepMCEvents * theEvents  ,
                                         LHCb::GenCollisions * /* theCollision */ )
  const {
	// Selection of the b quarks
	std::list< const HepMC::GenParticle * > bList ;
  const HepMC::GenEvent * theEvent = (*(theEvents->begin()))->pGenEvt() ;
  for(HepMC::GenEvent::particle_const_iterator iterall = 
        theEvent -> particles_begin() ;
      iterall!= theEvent -> particles_end();iterall++) {
    if ( Isb( *(iterall) ) ) bList.push_back( *(iterall) ) ;
  }
  
	const HepMC::GenParticle * theb1(0), *theb2(0);
	std::list<const HepMC::GenParticle * >::iterator iterb = bList.begin() ;
	if( 2<= bList.size() ) {
    theb1 = *(iterb); 
		theb2 = *(++iterb);
	}

	if ( ( 1 == bList.size() ) && ( m_motherofb_id.value() != "t" ) ) {
    Warning( "No b pairs in this event!" ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  //can occure in ttbar event when one top decay on another mode than bW
    return false ;
  }
	
  if( ( 1 == bList.size() ) && ( m_motherofb_id.value()=="t" ) ) {  // remplacer ici par les fonction de PID
		theb1 = *(iterb);
		theb2 = *(iterb);
	}
  
	if( ( 0== bList.size() ) && ( m_nbbquarks.value() != -1 ) ) {
    Warning( "No b in this event!" ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
    return false ;
  }

	double pzb1,thetab1;
	double pzb2,thetab2;
	if( ( m_nbbquarks.value() != -1 ) && ( bList.size() > 0 ) ) {
 		pzb1 = theb1->momentum().pz() * Gaudi::Units::GeV ;
    thetab1 = theb1->momentum().theta();
 		pzb2 = theb2->momentum().pz() * Gaudi::Units::GeV ;
    thetab2 = theb2->momentum().theta();
	}

	if( ( 1== bList.size() ) && ( m_motherofb_id.value()== "t" ) ) { 
    // necessary in order to avoid case where we ask 2b in ttbar event 
    // and that only one top decay in b in the acceptance
		pzb2=-1000;
		thetab2=1000000;
	}

	if( 0 == bList.size() ) {
		pzb2=-1000;
		thetab2=1000000;
		pzb1=-1000;
		thetab1=1000000;
	}

	// Selection of the lepton
	std::vector< const HepMC::GenParticle * > LeptonList ;
	for ( HepMC::GenEvent::particle_const_iterator iterall = 
          theEvent -> particles_begin() ;
        iterall!= theEvent -> particles_end() ; iterall++ ) {
    if ( IsLepton( *(iterall) ) ) LeptonList.push_back( *(iterall) ) ;
	}

  if( ( 0 == LeptonList.size() ) ) {
    if( msgLevel( MSG::DEBUG ) ){
      debug()<<"No lepton in this event of requiered type "
             <<"with requiered mother (if mother was asked)!"<< endmsg;   
      debug()<<"You either produced events with no leptons, "
             <<"or put the wrong type of lepton, or of lepton mother"<< endmsg;
    }
    return false ;
  }

	double pzl1,ptl1,thetal1;
	double pzl2,ptl2,thetal2;
	if ( m_nbLepton.value() <= ( int ) LeptonList.size() ) {
		const HepMC::GenParticle * theLepton1(0), *theLepton2(0);
		std::vector<const HepMC::GenParticle * >::iterator iterLepton = 
      LeptonList.begin() ;
		if ( 2 <= LeptonList.size() ) {
			theLepton1 = *(iterLepton);
			theLepton2 = *(++iterLepton);
		}
		if( 1== LeptonList.size() ) {
			theLepton1 = *(iterLepton);
			theLepton2 = *(iterLepton);
		}



 		pzl1 = theLepton1->momentum().pz() ;
    thetal1 = theLepton1->momentum().theta();
    ptl1 = theLepton1->momentum().perp();
 		pzl2 = theLepton2->momentum().pz() ;
    thetal2 = theLepton2->momentum().theta();
    ptl2 = theLepton2->momentum().perp();

		if ( 1 == LeptonList.size() ) {
			thetal2= 100000;
			ptl2=-1000;
			pzl2=-1000;
		}
	} else {
		thetal1= 100000;
		ptl1=-1000;
		pzl1=-1000;
		thetal2= 100000;
		ptl2=-1000;
		pzl2=-1000;
	}

	if((m_nbbquarks.value()==-1 &&  m_nbLepton.value() ==0)// no cut
		||(m_nbbquarks.value()==-1 &&  m_nbLepton.value() ==1 && 
       ( ( ( thetal1 <= m_thetaMax.value() )&& ( pzl1 >= 0. ) && ( ptl1 >= m_ptMin.value() ) )
         || ( ( thetal2 <= m_thetaMax.value() ) && ( pzl2 >= 0. )  && 
              ( ptl2 >= m_ptMin.value() ) ) ) )// no b cut on 1 lepton
     ||(m_nbbquarks.value()==-1 &&  m_nbLepton.value() ==2 && 
        ( ( ( thetal1 <= m_thetaMax.value() )&& ( pzl1 >= 0. ) && 
            ( ptl1 >= m_ptMin.value() ) )&& ( ( thetal2 <= m_thetaMax.value() ) && 
                                      ( pzl2 >= 0. )  
                                      && ( ptl2 >= m_ptMin.value() ) ) ) )
     // no b cut on 2 lepton
     ||(m_nbbquarks.value()==0 &&  m_nbLepton.value() ==0 
        &&  (  ( pzb1 >= 0. )
               || (  pzb2 >= 0. ) ) ) // only cut on pzb no lepton cut
     || (m_nbbquarks.value()==0 &&  m_nbLepton.value() ==1 
         &&  ( ( pzb1 >= 0. )
               || ( pzb2 >= 0. ) ) &&  
         ( ( ( thetal1 <= m_thetaMax.value() )&& ( pzl1 >= 0. ) 
             && ( ptl1 >= m_ptMin.value() ) )|| ( ( thetal2 <= m_thetaMax.value() ) 
                                          && ( pzl2 >= 0. )  && 
                                          ( ptl2 >= m_ptMin.value() ) ) ) )
     // only cut on pzb and 1 lepton
     ||(m_nbbquarks.value()==0 &&  m_nbLepton.value() ==2 
        &&  ( ( pzb1 >= 0. )
              || ( pzb2 >= 0. ) ) &&  ( ( ( thetal1 <= m_thetaMax.value() )
                                          && ( pzl1 >= 0. ) && 
                                          ( ptl1 >= m_ptMin.value() ) ) && 
                                        ( ( thetal2 <= m_thetaMax.value() ) && 
                                          ( pzl2 >= 0. )  && 
                                          ( ptl2 >= m_ptMin.value() ) ) ) )
     // only cut on pzb and 2 lepton
     ||(m_nbbquarks.value()==1 &&  m_nbLepton.value() ==0 &&  ( ( ( thetab1 <= m_thetaMax.value() ) 
                                                  && ( pzb1 >= 0. ) )
                                                || ( ( thetab2 <= m_thetaMax.value() )
                                                     && ( pzb2 >= 0. ) ) ) )
     // cut on 1b no lepton cut
     || (m_nbbquarks.value()==1 &&  m_nbLepton.value() ==1 &&  ( ( ( thetab1 <= m_thetaMax.value() ) 
                                                   && ( pzb1 >= 0. ) )
                                                 || ( ( thetab2 <= m_thetaMax.value() )
                                                      && ( pzb2 >= 0. ) ) )
         &&  ( ( ( thetal1 <= m_thetaMax.value() )&& ( pzl1 >= 0. ) && 
                 ( ptl1 >= m_ptMin.value() ) )|| ( ( thetal2 <= m_thetaMax.value() ) && 
                                           ( pzl2 >= 0. )  && 
                                           ( ptl2 >= m_ptMin.value() ) ) ) )
     // cut on 1b and 1 lepton
     ||(m_nbbquarks.value()==1 &&  m_nbLepton.value() ==2 &&  
        ( ( ( thetab1 <= m_thetaMax.value() ) && ( pzb1 >= 0. ) )
          || ( ( thetab2 <= m_thetaMax.value() ) && ( pzb2 >= 0. ) ) ) &&  
        ( ( ( thetal1 <= m_thetaMax.value() )&& ( pzl1 >= 0. ) && 
            ( ptl1 >= m_ptMin.value() ) ) && ( ( thetal2 <= m_thetaMax.value() ) && 
                                       ( pzl2 >= 0. )  && ( ptl2 >= m_ptMin.value() ) )
          ) )// cut on 1b and 2 lepton
     ||(m_nbbquarks.value()==2 &&  m_nbLepton.value() ==0 &&  ( ( ( thetab1 <= m_thetaMax.value() ) 
                                                  && ( pzb1 >= 0. ) )
                                                && ( ( thetab2 <= m_thetaMax.value() )
                                                     && ( pzb2 >= 0. ) ) ) )
     // cut on 2b no lepton cut
     ||(m_nbbquarks.value()==2 &&  m_nbLepton.value() ==1 &&  ( ( ( thetab1 <= m_thetaMax.value() ) 
                                                  && ( pzb1 >= 0. ) )
                                                && ( ( thetab2 <= m_thetaMax.value() )
                                                     && ( pzb2 >= 0. ) ) )
        &&  ( ( ( thetal1 <= m_thetaMax.value() )&& ( pzl1 >= 0. ) && 
                ( ptl1 >= m_ptMin.value() ) ) || ( ( thetal2 <= m_thetaMax.value() ) && 
                                           ( pzl2 >= 0. )  && 
                                           ( ptl2 >= m_ptMin.value() ) ) ) )
     // cut on 2b and 1 lepton
     ||(m_nbbquarks.value()==2 &&  m_nbLepton.value() ==1 &&  ( ( ( thetab1 <= m_thetaMax.value() ) 
                                                  && ( pzb1 >= 0. ) )
                                                && ( ( thetab2 <= m_thetaMax.value() )
                                                     && ( pzb2 >= 0. ) ) ) && 
        ( ( ( thetal1 <= m_thetaMax.value() )&& ( pzl1 >= 0. ) && ( ptl1 >= m_ptMin.value() ) )
          && ( ( thetal2 <= m_thetaMax.value() ) && ( pzl2 >= 0. )  && 
               ( ptl2 >= m_ptMin.value() ) ) ) )// cut on 2b and 2 lepton
     ) {
		if( msgLevel( MSG::DEBUG ) ){
      debug()  << "Event passed with requierement of "
               << m_nbLepton.value() << " lepton and " 
               << m_nbbquarks.value() <<" bquarks" << endmsg ;
      debug()  << " [thetab1 = " << thetab1<< "] [thetab2 = " << thetab2 
               << "] " << "[pzb1 = " << pzb1 << "] [pzb2 = " << pzb2
               << " [thetal1 = " << thetal1 << "] [pzl1 = " << pzl1 
               << "] [ptl1 = " << ptl1 << "] [thetal2 = "
               << thetal2 << "] [pzl2 = " << pzl2 << "] [ptl2 = " << ptl2 
               << "]" << endmsg ;
    }
		return true;

	} else {
    if( msgLevel( MSG::DEBUG ) ){
      debug() << "Event rejected with requierement of "<< m_nbLepton.value()
              << " lepton and " << m_nbbquarks.value() << " bquarks" << endmsg ;
      debug() << " [thetab1 = " << thetab1 << "] [thetab2 = " 
              << thetab2 << "] " << "[pzb1 = " << pzb1 
              << "] [pzb2 = " << pzb2 << " [thetal1 = "<< thetal1 
              << "] [pzl1 = "<< pzl1 << "] [ptl1 = "<< ptl1 << "] [thetal2 = "
              << thetal2 << "] [pzl2 = "<< pzl2 << "] [ptl2 = "<< ptl2 << "]" 
              << endmsg ;
    }
		return false;
	}
	return false ;
}



