/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENCUTS_DAUGHTERSINLHCBANDCUTSFORDSTAR_H
#define GENCUTS_DAUGHTERSINLHCBANDCUTSFORDSTAR_H 1

// Include files
#include "DaughtersInLHCb.h"
#include "GaudiKernel/SystemOfUnits.h"

/** @class DaughtersInLHCbAndCutsForDstar DaughtersInLHCbAndCutsForDstar.h
 *
 *  Tool to keep events with daughters from signal particles
 *  in LHCb and with p and pt cuts on Dstar daughters.
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Patrick Robbe
 *  @date   2012-02-07
 */
class DaughtersInLHCbAndCutsForDstar : public extends<DaughtersInLHCb,IGenCutTool> {
 public:
  using extends::extends;

  /** Accept events with daughters in LHCb and p/pt cuts on Dstar daughters
   *  (defined by min and max angles, different values for charged and neutrals)
   *  Implements IGenCutTool::applyCut.
   */
  bool applyCut( ParticleVector & theParticleVector ,
                 const HepMC::GenEvent * theEvent ,
                 const LHCb::GenCollision * theCollision ) const override;

 private:
  /** Study a particle a returns true when all stable daughters
   *  are in LHCb AndWithMinP
   */
  bool passCuts( const HepMC::GenParticle * theSignal ) const ;

  // cut value of D0 pt
  Gaudi::Property<double> m_d0ptCut{this,"D0PtCuts",1700 * Gaudi::Units::MeV,"cut value of D0 pt"} ;

  // cut value on daughters min pt
  Gaudi::Property<double> m_daughtersptminCut{this,"DaughtersPtMinCut",700 * Gaudi::Units::MeV,"cut value on daughters min pt"} ;

  // cut value on daughters max pt
  Gaudi::Property<double> m_daughtersptmaxCut{this,"DaughtersPtMaxCut",1050 * Gaudi::Units::MeV,"cut value on daughters max pt"} ;

  // cut value on daughters min p
  Gaudi::Property<double> m_daughterspminCut{this,"DaughtersPMinCut",4500 * Gaudi::Units::MeV,"cut value on daughters min p"} ;

  // soft pion pt cut
  Gaudi::Property<double> m_softpiptCut{this,"SoftPiPtCut",100 * Gaudi::Units::MeV,"soft pion pt cut"} ;

  // cut on D0 ctau
  Gaudi::Property<double> m_d0ctauCut{this,"D0CTauCut",-1.,"cut on D0 ctau"} ;
};
#endif // GENCUTS_DAUGHTERSINLHCDANDCUTSFORDSTAR_H
