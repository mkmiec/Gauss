/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: UpsilonDaughtersInLHCb.h,v 1.2 2010-09-19 09:03:47 robbep Exp $
#ifndef GENCUTS_UPSILONDAUGHTERSINLHCB_H
#define GENCUTS_UPSILONDAUGHTERSINLHCB_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Transform4DTypes.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "MCInterfaces/IGenCutTool.h"

// Forward declaration
class IDecayTool ;

/** @class UpsilonDaughtersInLHCb UpsilonDaughtersInLHCb.h
 *
 *  Tool to keep events with daughters from Upsilon
 *  in LHCb acceptance.
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Patrick Robbe
 *  @date   2009-02-26
 */

class UpsilonDaughtersInLHCb : public extends<GaudiTool,IGenCutTool> {
 public:
  /// Standard constructor
  using extends::extends;

  StatusCode initialize( ) override;   ///< Initialize method

  /** Accept events with daughters in LHCb acceptance (defined by min and
   *  max angles, different values for charged and neutrals)
   *  Implements IGenCutTool::applyCut.
   */
  bool applyCut( ParticleVector & theParticleVector ,
                 const HepMC::GenEvent * theEvent ,
                 const LHCb::GenCollision * theCollision ) const override;

  StatusCode finalize( ) override;   ///< Finalize method

protected:

private:
  /// Decay tool
  IDecayTool*  m_decayTool{nullptr};

  /** Study a particle a returns true when all stable daughters
   *  are in LHCb acceptance
   */
  bool passCuts( const HepMC::GenParticle * theSignal ) const ;

  /** Minimum value of angle around z-axis for charged daughters
   * (set by options)
   */
  Gaudi::Property<double> m_chargedThetaMin{this,"ChargedThetaMin",10 * Gaudi::Units::mrad ,"ChargedThetaMin"} ;

  /** Maximum value of angle around z-axis for charged daughters
   * (set by options)
   */
  Gaudi::Property<double> m_chargedThetaMax{this,"ChargedThetaMax",400 * Gaudi::Units::mrad ,"ChargedThetaMax"} ;

  /** Minimum value of angle around z-axis for neutral daughters
   * (set by options)
   */
  Gaudi::Property<double> m_neutralThetaMin{this,"NeutralThetaMin",5 * Gaudi::Units::mrad ,"NeutralThetaMin"} ;

  /** Maximum value of angle around z-axis for neutral daughters
   * (set by options)
   */
  Gaudi::Property<double> m_neutralThetaMax{this,"NeutralThetaMax",400 * Gaudi::Units::mrad ,"NeutralThetaMax"} ;

  /// Name of the decay tool to use
  Gaudi::Property<std::string > m_decayToolName{this,"DecayTool","EvtGenDecay","DecayTool"} ;

  Gaudi::Property<int>     m_sigUpsilonPID{this,"SignalPID",553 ,"SignalPID"};  ///< PDG Id of the Upsilon to consider (set by options)
  mutable int  m_nUpsilonBeforeCut{0}    ;  ///< Counter of generated upsilon
};
#endif // GENERATORS_BCDAUGHTERSINLHCB_H
