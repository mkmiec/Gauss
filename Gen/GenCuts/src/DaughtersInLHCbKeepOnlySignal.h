/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENCUTS_DAUGHTERSINLHCBKEEPONLYSIGNAL_H
#define GENCUTS_DAUGHTERSINLHCBKEEPONLYSIGNAL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "MCInterfaces/IGenCutTool.h"

// from LHCb
#include "Kernel/ParticleID.h"

namespace LHCb { class IParticlePropertySvc ; }

/** @class DaughtersInLHCbKeepOnlySignal DaughtersInLHCbKeepOnlySignal.h
 *
 *  Tool to keep events with daughters from signal particles
 *  in LHCb acceptance.
 *  Remove also all particles form the event except the signal particle
 *  This allows to embed J/psi, D, etc... in minimum bias events
 *
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Patrick Robbe
 *  @date   2015-08-01
 */
class DaughtersInLHCbKeepOnlySignal : public extends<GaudiTool,IGenCutTool> {
 public:
  /// Standard constructor
  using extends::extends;

  StatusCode initialize( ) override;   ///< Initialize method

  StatusCode finalize( ) override;   ///< Finalize method

  /** Accept events with daughters in LHCb acceptance (defined by min and
   *  max angles, different values for charged and neutrals)
   *  Remove all particles except the signal
   *  Implements IGenCutTool::applyCut.
   */
  bool applyCut( ParticleVector & theParticleVector ,
                 const HepMC::GenEvent * theEvent ,
                 const LHCb::GenCollision * theCollision ) const override;

 private:
  /// Decay tool
  IDecayTool*  m_decayTool{nullptr};

  /** Study a particle a returns true when all stable daughters
   *  are in LHCb acceptance
   */
  bool passCuts( const HepMC::GenParticle * theSignal ) const ;

  /** Minimum value of angle around z-axis for charged daughters
   * (set by options)
   */
  Gaudi::Property<double> m_chargedThetaMin{this,"ChargedThetaMin",10 * Gaudi::Units::mrad ,"Minimum value of angle around z-axis for charged daughters"} ;

  /** Maximum value of angle around z-axis for charged daughters
   * (set by options)
   */
  Gaudi::Property<double> m_chargedThetaMax{this,"ChargedThetaMax",400 * Gaudi::Units::mrad ,"Maximum value of angle around z-axis for charged daughters"} ;

  /** Minimum value of angle around z-axis for neutral daughters
   * (set by options)
   */
  Gaudi::Property<double> m_neutralThetaMin{this,"NeutralThetaMin",5 * Gaudi::Units::mrad ,"Minimum value of angle around z-axis for neutral daughters"} ;

  /** Maximum value of angle around z-axis for neutral daughters
   * (set by options)
   */
  Gaudi::Property<double> m_neutralThetaMax{this,"NeutralThetaMax",400 * Gaudi::Units::mrad ,"Maximum value of angle around z-axis for neutral daughters"} ;

  /// Name of the decay tool to use
  Gaudi::Property<std::string> m_decayToolName{this,"DecayTool","EvtGenDecay" ,"Name of the decay tool to use"} ;

  Gaudi::Property<int> m_signalPID{this,"SignalPID",443 ,"PDG Id of the signal to consider"}        ;  ///< PDG Id of the signal to consider (set by options)
  mutable int  m_nSignalBeforeCut{0}    ;  ///< Counter of generated signal

  LHCb::ParticleID::Quark m_signalQuark{LHCb::ParticleID::down} ;

  LHCb::IParticlePropertySvc * m_ppSvc{nullptr} ;

  /// Create a new event with a given particle
  HepMC::GenVertex * copyHepMCParticle( const HepMC::GenParticle * theSignal ,
                                        std::vector< HepMC::GenVertex * > &vV ) const ;

  /// Make an HepMC tree from a particle
  void fillTree(  HepMC::GenParticle * theNewParticle ,
                  const HepMC::GenParticle * theOldParticle ,
                  std::vector< HepMC::GenVertex * > &vV ) const ;
};
#endif // GENCUTS_DAUGHTERSINLHCBKEEPONLYSIGNAL_H
