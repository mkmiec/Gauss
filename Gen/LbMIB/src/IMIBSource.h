/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: IMIBSource.h,v 1.1 2007-08-17 12:54:14 gcorti Exp $
#ifndef IMIBSOURCE_H 
#define IMIBSOURCE_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

// From Event
#include "Event/HepMCEvent.h"
 
static const InterfaceID IID_IMIBSource ( "IMIBSource", 1, 0 );

/** @class IMIBSource IMIBSource.h
 *  
 *
 *  @author Gloria Corti
 *  @date   2007-08-10
 */
class IMIBSource : virtual public IAlgTool {
public: 

  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_IMIBSource; }


  // Generate one event, including empty ones
  virtual StatusCode generateEvent( LHCb::GenHeader* theHeader,
                                    LHCb::GenCollisions* theCollisions,
                                    LHCb::HepMCEvents* theEvents,
                                    int& numParts ) = 0;

protected:

private:

};
#endif // IMIBSOURCE_H
