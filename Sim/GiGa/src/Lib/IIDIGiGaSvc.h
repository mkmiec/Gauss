/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: IIDIGiGaSvc.h,v 1.1 2004-02-20 18:58:24 ibelyaev Exp $
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.5  2002/12/07 14:27:51  ibelyaev
//  see $GIGAROOT/cmt/requirements file
//
// ============================================================================
#ifndef GIGA_IIDIGIGASVC_H 
#define GIGA_IIDIGIGASVC_H 1
// ============================================================================

/** @file   
 *  Declaration of the unique interface ID 
 *  ( interface id, major and  minor versions) for class IGiGaSvc 
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 */
/** @var  IID_IGiGaSvc
 *  Declaration of the unique interface ID 
 *  ( interface id, major and  minor versions) for class IGiGaSvc 
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 */
static const InterfaceID IID_IGiGaSvc( "IGiGaSvc" , 5 , 0); 

// ============================================================================
// The End 
// ============================================================================
#endif ///< GIGA_IIDIGIGASVC_H
// ============================================================================
