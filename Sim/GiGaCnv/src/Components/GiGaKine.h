/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef      GIGACNV_GiGaKine_H
#define      GIGACNV_GiGaKine_H  1
// ============================================================================
/// from STL
#include <string>
/// from GiGaCnv
#include "GiGaCnv/IGiGaKineCnvSvc.h"
#include "GiGaCnv/GiGaCnvSvcBase.h"
#include "GiGaCnv/GiGaKineRefTable.h"
///
template <class SERVICE>
class SvcFactory;
///
class    IGiGaSvc;
namespace LHCb {
  class    IParticlePropertySvc;
}

/** @class GiGaKine GiGaKine.h
 *
 *  conversion service  for converting of Gaudi
 *  MCParticle/MCVertex structure into
 *  Geant4 Primary Event conversion of
 *  Geant4 Trajectories into Gaudi MCVertex/MCParticle structure
 *
 *  @author  Vanya Belyaev
 *  @date    07/08/2000
 */

class GiGaKine:
  public          GiGaCnvSvcBase ,
  virtual public IGiGaKineCnvSvc
{
public:

  /** standard constructor
   *  @param name  name of the service
   *  @param loc   pointer to service locator
   */
  GiGaKine( const std::string& name ,
                  ISvcLocator* loc );

  /// virtual destructor
  virtual ~GiGaKine();

  /** initialization
   *  @return status code
   */
  StatusCode            initialize() override;

  /** finalization
   *  @return status code
   */
  StatusCode            finalize  () override;

  /** retrieve the relation table between Geant4 track/trajectory
   *  identifiers and the converted MCParticle objects
   *  @return the reference to relation table
   */
  GiGaKineRefTable&     table()        override { return m_table;} ;

  /** access to particle properties service
   *  @return pointer to particle properties service
   */
  LHCb::IParticlePropertySvc* ppSvc () const override { return m_ppSvc; }

  /** query the interface
   *  @param ID unique interface identifier
   *  @param II placeholder for interface
   *  @return status code
   */
  StatusCode queryInterface( const InterfaceID& ID ,
                             void**             II ) override;

private:

  /// name of particle property service
  std::string            m_ppSvcName ;
  /// pointer to particle property service
  LHCb::IParticlePropertySvc*  m_ppSvc     ;
  /// reference table
  GiGaKineRefTable       m_table     ;

};

// ============================================================================
#endif  ///<  GIGACNV_GiGaKine_H
// ============================================================================
