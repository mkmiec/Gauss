/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef        GIGACNV_GIGAMixtureCnv_H
#define        GIGACNV_GIGAMixtureCnv_H  1
// ============================================================================
/// from STL
#include <set>
/// base class from GiGa
#include "GiGaCnv/GiGaCnvBase.h"
#include "GiGaCnv/GiGaLeaf.h"
///
class Material;
class Mixture;
class Element;
class Isotope;

/** @class GiGaMixtureCnv GiGaMixtureCnv.h
 *
 *  Converter of Mixture class to Geant4
 *
 *  @author  Vanya Belyaev  Ivan.Belyaev@itep.ru
 */

class GiGaMixtureCnv: public GiGaCnvBase
{
public: 
  /// Standard Constructor
  GiGaMixtureCnv( ISvcLocator* );
  /// Standard (virtual) destructor
  virtual ~GiGaMixtureCnv();
  ///
public:

  /// Create representation
  StatusCode createRep
  ( DataObject*      Object  ,
    IOpaqueAddress*& Address ) override;

  /// Update representation
  StatusCode updateRep
  ( IOpaqueAddress* Address, DataObject* Object ) override;

  /// Class ID for created object == class ID for this specific converter
  static const CLID&          classID();
  /// storage Type
  static unsigned char storageType() ;
  ///

private:

  GiGaLeaf m_leaf ;

};

// ============================================================================
// End
// ============================================================================
#endif   //     __GIGA_GEOMCONVERSION_GIGAMixtureCnv_H__
// ============================================================================



