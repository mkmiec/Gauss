/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: CaloSim.h,v 1.5 2006-06-24 16:23:44 ibelyaev Exp $
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $ 
// ============================================================================
// $Log: not supported by cvs2svn $
// ============================================================================
#ifndef CALOSIM_CALOSIM_H 
#define CALOSIM_CALOSIM_H 1
// ============================================================================
// Include files
// ============================================================================
// STD & STL 
// ============================================================================
#include <vector>
// ============================================================================
// forward declaration
// ============================================================================
class G4VPhysicalVolume;
// ============================================================================

/** @namespace CaloSim 
 *  
 *  Definiton of useful types, functions 
 *  and constants for CaloSim package 
 *
 *  @author Alexei Berdiouguine
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   2002-07-09
 */

namespace CaloSim 
{
  // cell id for CaloSim package 
  typedef  std::vector<const G4VPhysicalVolume*> Path ;
}


// ============================================================================
// The END 
// ============================================================================
#endif // CALOSIM_H
// ============================================================================
