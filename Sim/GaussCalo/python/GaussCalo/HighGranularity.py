###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import LHCbConfigurableUser
from Gaudi.Configuration import log
import Configurables


class HighGranularCalo(LHCbConfigurableUser):
    """HighGranularCalo sets up the Gaudi properties of the ``CALOXYZSensDet``
    and ``GetCaloSensDet`` that activate the high granularity feature.
    The feature is responsoible for dividing standard calo cells into
    ``NRows`` (along x-axis) x ``NCols`` (along y-axis) additional sub-cells.
    That information in the event model is carried by LHCb::MCSmallCaloHit.

    The High Granularity feature will be switched on per calorimeter if any
    of the ``NRows`` or ``NCols`` is > 1.

    It is possible to set the options for Ecal, Hcal, Spd & Prs. Please note
    that the options won't be set if the calos are not set in the main
    configuration dedicated to the geometry.

    :var Ecal: Slot for ECAL Run 1, 2 & 3
    :vartype Ecal: dict, optional

    :var Hcal: Slot for HCAL Run 1, 2 & 3
    :vartype Hcal: dict, optional

    :var Prs: Slot for PRS Run 1 & 2
    :vartype Prs: dict, optional

    :var Spd: Slot for SPD Run 1 & 2
    :vartype Spd: dict, optional

    :Example:

        .. highlight:: python
        .. code-block:: python

            # this is the only line necessary to activate HG in Ecal
            # each cell that is in the region cell will be split in
            # 4 x 4 sub-cells
            from GaussCalo.HighGranularity import HighGranularCalo
            hcg = HighGranularCalo()
            hcg.Ecal["Inner"]["NRows"] = 4
            hcg.Ecal["Inner"]["NCols"] = 4

    """

    __slots__ = {
        # calorimeters
        'Ecal': {
            'Outer': {
                'NRows': 1,
                'NCols': 1,
            },
            'Middle': {
                'NRows': 1,
                'NCols': 1,
            },
            'Inner': {
                'NRows': 1,
                'NCols': 1,
            },
        },
        'Hcal': {
            'Outer': {
                'NRows': 1,
                'NCols': 1,
            },
            'Inner': {
                'NRows': 1,
                'NCols': 1,
            },
        },
        'Prs': {
            'Outer': {
                'NRows': 1,
                'NCols': 1,
            },
            'Middle': {
                'NRows': 1,
                'NCols': 1,
            },
            'Inner': {
                'NRows': 1,
                'NCols': 1,
            },
        },
        'Spd': {
            'Outer': {
                'NRows': 1,
                'NCols': 1,
            },
            'Middle': {
                'NRows': 1,
                'NCols': 1,
            },
            'Inner': {
                'NRows': 1,
                'NCols': 1,
            },
        },
        'Force': False,  # only for tests
    }

    _possible_regions = {
        # order is important!!!
        'Ecal': ['Outer', 'Middle', 'Inner'],
        'Hcal': ['Outer', 'Inner'],
        'Prs': ['Outer', 'Middle', 'Inner'],
        'Spd': ['Outer', 'Middle', 'Inner'],
    }

    _supported_options = [
        'NRows',
        'NCols',
    ]

    added_calos = []

    def prepare(self, det, moni):
        """ Preapres the configuration of sensitive detector classes
        and GetCaloHitsAlgs such that they get all the right properties
        to activate the high granularity feature for a specific calo/region

        :param det: name of the calo (Ecal, Hcal, Spd, Prs)
        :param moni: GetCaloHitsAlg configurable
        """
        if self._inspect_properties(det) or self.getProp("Force"):
            props = self.getProp(det)
            sensdetname = det + 'SensDet'
            if det in ['Spd', 'Prs']:
                sensdetname = "SpdPrsSensDet"
            # TODO: for now supporting only DetDesc / GaussGeo!:
            geoname = "GaussGeo.{}".format(det)
            sensdetconf = getattr(Configurables, sensdetname)(geoname)
            log.info("Activating High Granularity feature for " + geoname)
            sensdetconf.HighGranularityOn = True
            moni.HighGranularityOn = True
            rows = [props[reg]['NRows'] for reg in self._possible_regions[det]]
            sensdetconf.HighGranularityNRows = rows
            cols = [props[reg]['NCols'] for reg in self._possible_regions[det]]
            sensdetconf.HighGranularityNCols = cols
            self.added_calos.append(det)

    def _inspect_properties(self, det):
        """ Checks the logic of the HG properties taking into account the
        varying geometry of calorimeters

        :param det: name of the calo (Ecal, Hcal, Spd, Prs)
        """
        if det not in self._possible_regions.keys():
            raise RuntimeError(
                "High Granularity: Not supported detector: {}. Supported detectors are: [{}]"
                .format(det, ','.join(self._possible_regions.keys())))
        props = self.getProp(det)
        if not props or type(props) is not dict:
            raise RuntimeError(
                "High Granularity: \"{}\" options must be a dictionary".format(
                    det))
        active = False
        for reg, reg_props in props.items():
            if reg not in self._possible_regions[det]:
                raise RuntimeError(
                    "High Granularity: Region \"{}\" not supported for {}".
                    format(reg, det))
            if type(reg_props) is not dict:
                raise RuntimeError(
                    "High Granularity: Region \"{}/{}\" options must be a dictionary"
                    .format(det, reg))
            for opt, val in reg_props.items():
                if opt not in self._supported_options:
                    raise RuntimeError(
                        "High Granularity: Not supported option: {}.".format(
                            opt))
                if opt in ['NRows', 'NCols'] and val > 1:
                    active = True
        return active
