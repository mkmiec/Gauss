<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="prerequisites"><set>
  <tuple><text>calo_collectors</text><enumeral>PASS</enumeral></tuple>
</set></argument>
<argument name="program"><text>gaudirun.py</text></argument>
<argument name="timeout"><integer>600</integer></argument>
<argument name="args"><set>
  <text>../options/calo_collectors.py</text>
  <text>../options/high_granularity.py</text>
</set></argument>
<argument name="validator"><text>
import re
import json

det_calo_hits = {}
calo_reg = r'GaussGeo\.{}\W*INFO Number of counters[\S\s]*?(?="#tslots).*'
for calo in ['Spd', 'Prs', 'Ecal', 'Hcal']:
    matches = re.findall(calo_reg.format(calo), stdout)
    if not matches:
        causes.append('No hit counter info about ' + calo)
    subhits_str = re.findall(r'#tslots.*', matches[0])
    det_calo_hits[calo] = int(subhits_str[0].split('|')[2].strip())

calo_hits = {}
coll_hits = {}
coll_reg = r'^{}CaloColl.+Number\W+of\W+counters[\s\S]+?(?="CollectorHitsCounter").*'
for coll in ["", "Double", "Triple", "Quadrupole"]:
    coll_matches = re.findall(
        coll_reg.format(coll), stdout, flags=re.MULTILINE)
    if not coll_matches:
        causes.append('No hit counter info about ' + coll)
    calo_hits_str = re.findall(r'CaloHitsCounter.*', coll_matches[0])
    calo_hits[coll] = int(calo_hits_str[0].split('|')[1].strip())
    coll_hits_str = re.findall(r'CollectorHitsCounter.*', coll_matches[0])
    coll_hits[coll] = int(coll_hits_str[0].split('|')[1].strip())

coll_hits_uq = set(coll_hits.values())
if len(coll_hits_uq) != 1 or sum(coll_hits_uq) != 16:
    causes.append('HG/MCCollector hit info was not written correctly')

if calo_hits[""] != det_calo_hits["Spd"]:
    causes.append('HG/Single collector hit info was not written correctly')

if calo_hits["Double"] != det_calo_hits["Spd"] + det_calo_hits["Prs"]:
    causes.append('HG/Double collector hit info was not written correctly')

if calo_hits["Triple"] != det_calo_hits["Spd"] + det_calo_hits[
        "Prs"] + det_calo_hits["Ecal"]:
    causes.append('HG/Triple collector hit info was not written correctly')

if calo_hits["Quadrupole"] != sum(det_calo_hits.values()):
    causes.append('HG/Quadrupole collector hit info was not written correctly')
    
std_gran_hits = {}
with open('standard_granularity_hits_no.json', 'r') as infile:
    std_gran_hits = json.loads(json.load(infile))

nrows = 1
ncols = 1
for calo in ['Spd', 'Prs', 'Ecal', 'Hcal']:
    if std_gran_hits[calo] * nrows * ncols != det_calo_hits[calo]:
        causes.append(
            "Incorrect number of high-granular hits in {}.".format(calo)
        )
</text></argument>
</extension>

