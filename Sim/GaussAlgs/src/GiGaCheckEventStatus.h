/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GiGaCheckEventStatus.h,v 1.1 2005-04-20 15:00:06 gcorti Exp $
#ifndef GIGACHECKEVENTSTATUS_H
#define GIGACHECKEVENTSTATUS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

// Forward declarations
// from GiGa
class IGiGaSvc;

/** @class GiGaCheckEventStatus GiGaCheckEventStatus.h
 *
 *  Algorithm to check processing status of G4Event as set by G4RunManager.
 *  If event was aborted by Geant4 set filtering status of algorithm as
 *  false.
 *
 *  @author Gloria Corti
 *  @date   2005-04-19
 */
class GiGaCheckEventStatus : public GaudiAlgorithm {
public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  /** Accessor to GiGa Service
   *  @return pointer to GiGa Service
   */
  inline IGiGaSvc* gigaSvc() const {
    return m_gigaSvc;
  }

private:

  Gaudi::Property<std::string> m_gigaSvcName{this,"GiGa","GiGa","Name of GiGaSvc to be used"} ;   ///< Name of GiGaSvc to be used
  IGiGaSvc*       m_gigaSvc{nullptr}     ;   ///< Pointer of GiGaSvc

};
#endif // GIGACHECKEVENTSTATUS_H
