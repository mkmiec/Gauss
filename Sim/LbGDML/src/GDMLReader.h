/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GDML_READER_H
#define GDML_READER_H 1

// Gaudi
#include "GaudiAlg/GaudiTool.h"

// Interface
#include "GiGa/IGDMLReader.h"

/** @class GDMLReader GDMLReader.h
 *
 *  Tool to import geometry from GDML file
 *
 */
class IGiGaSensDet;

class GDMLReader : public GaudiTool,
                   virtual public IGDMLReader {

public:
  /// Constructor
  GDMLReader(const std::string& type,
             const std::string& name,
             const IInterface* parent);
  /// Destructor
  virtual ~GDMLReader() {}

  StatusCode import(G4VPhysicalVolume* world) override;

private:

  /// Name of the GDML file to be loaded
  std::string m_gdmlfile;
  /// Optional name for a sensitive detector that is attached to all top level logical volumes
  std::map<std::string, std::string> m_sensDetName{};
  /// Translation
  double m_tx, m_ty, m_tz;
  /// Rotation (in degrees)
  double m_rx, m_ry, m_rz;

};
#endif
