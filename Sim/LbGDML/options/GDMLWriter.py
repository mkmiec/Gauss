###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##  File with options to set up specific run action to write
##  geometry in GDML ( Geant4 geometry description language)
##
##  @author G.Corti
##  @date 2012-07-12 

from Configurables import GiGa
giga = GiGa()
giga.addTool( GiGaRunActionSequence("RunSeq") , name="RunSeq" )
giga.RunSeq.Members.append( "GDMLRunAction" )

# The following lines have the default settings
#giga.RunSeq.addTool( GDMLRunAction("GDMLRunAction") , name = "GDMLRunAction" )
#giga.RunSeq.GDMLRunAction.Schema = "$G4GDMLROOT/schema/gdml.xsd";
#giga.RunSeq.GDMLRunAction.Output = "LHCb.gdml";
