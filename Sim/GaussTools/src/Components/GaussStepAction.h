/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef       GaussTools_GaussStepAction_H
#define       GaussTools_GaussStepAction_H 1
// ============================================================================
// include files
// GiGa
#include "GiGa/GiGaStepActionBase.h"
// forward declarations
//template <class TYPE> class GiGaFactory;

/** @class GaussStepAction GaussStepAction.h
 *
 *  @author  Witek Pokorski
 *  @date    1/11/2002
 */

class GaussStepAction: virtual public GiGaStepActionBase
{
  /// friend factory for instantiation
  //  friend class GiGaFactory<GaussStepAction>;

  //protected:
public:

  /** standard constructor
   *  @see GiGaStepActionBase
   *  @see GiGaBase
   *  @see AlgTool
   *  @param type type of the object (?)
   *  @param name name of the object
   *  @param parent  pointer to parent object
   */
  GaussStepAction
  ( const std::string& type   ,
    const std::string& name   ,
    const IInterface*  parent ) ;

  /// destructor (virtual and protected)
  virtual ~GaussStepAction();

public:

  /** stepping action
   *  @see G4UserSteppingAction
   *  @param step Geant4 step
   */
  void UserSteppingAction ( const G4Step* ) override;

private:

  GaussStepAction(); ///< no default constructor
  GaussStepAction( const GaussStepAction& ); ///< no copy
  GaussStepAction& operator=( const GaussStepAction& ) ; ///< no =

  bool m_storeHitPoints;
  int m_maxoptsteps;
};
// ============================================================================

// ============================================================================
// The END
// ============================================================================
#endif  ///<    GaussTools_GaussStepAction_H
// ============================================================================
