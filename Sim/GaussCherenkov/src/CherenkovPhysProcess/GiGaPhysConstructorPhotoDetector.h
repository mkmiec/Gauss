/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef    GIGA_GiGaPhysConstructorPhotoDetector_H
#define    GIGA_GiGaPhysConstructorPhotoDetector_H 1
// ============================================================================
// include files

//GaudiKernel

// GiGa
#include "GiGa/GiGaPhysConstructorBase.h"

// forward declarations
template <class TYPE> class GiGaFactory;

class GiGaPhysConstructorPhotoDetector : public GiGaPhysConstructorBase
{
  /// friend factory for instantiation
  friend class GiGaFactory<GiGaPhysConstructorPhotoDetector>;

public:

  GiGaPhysConstructorPhotoDetector
  ( const std::string& type   ,
    const std::string& name   ,
    const IInterface*  parent ) ;


public:

  void ConstructParticle () override;
  void ConstructProcess  () override;
  bool activateRICHPmtPhysProcStatus()
  {  return m_ActivateRICHPmtPhysProc.value();}

  void setRICHPmtPhysProcActivation(bool bAct)
  { m_ActivateRICHPmtPhysProc.value()=bAct;  }

  bool   ActivateTorchTBMcpEnergyLossProc()
  {
    return m_ActivateTorchTBMcpEnergyLossProc.value();
  }

  void setActivateTorchTBMcpEnergyLossProc(bool aAct)
  { m_ActivateTorchTBMcpEnergyLossProc.value()=aAct;  }

  ///

private:

  void ConstructPeGenericProcess();
  void ConstructPmtSiEnLoss  ();
  ///

private:
  ///
  GiGaPhysConstructorPhotoDetector           ( const GiGaPhysConstructorPhotoDetector& );
  GiGaPhysConstructorPhotoDetector& operator=( const GiGaPhysConstructorPhotoDetector& );
  ///

  Gaudi::Property<double>  m_RichPmtSiDetEfficiency{this,"RichPmtSiDetEfficiency",1.0,"RichPmtSiDetEfficiency"};
  Gaudi::Property<double> m_RichPmtPixelChipEfficiency{this,"RichPmtPixelChipEfficiency",0.90,"RichPmtPixelChipEfficiency"};
  Gaudi::Property<double> m_RichPmtPeBackScatterProb{this,"RichPmtBackScatterProb",0.0,"RichPmtBackScatterProb"};
  Gaudi::Property<bool> m_ActivateRICHPmtPhysProc{this,"RichPmtPhysicsProcessActivate",true,"RichPmtPhysicsProcessActivate"};

  Gaudi::Property<bool> m_ActivateTorchTBMcpEnergyLossProc{this,"TorchTBMcpEnergyLossActivate",false,"TorchTBMcpEnergyLossActivate"};
  Gaudi::Property<double> m_TorchTBMcpAnodeEfficiency{this,"TorchTBMcpAnodeEfficiency",1.0,"TorchTBMcpAnodeEfficiency"};
  Gaudi::Property<double> m_TorchMcpAnodeReadoutChipEfficiency{this,"TorchTBMcpAnodeReadoutChipEfficiency",1.0,"TorchTBMcpAnodeReadoutChipEfficiency"};



};
// ============================================================================


// ============================================================================
#endif   ///< GIGA_GiGaPhysConstructorPhotoDetector_H
// ============================================================================











