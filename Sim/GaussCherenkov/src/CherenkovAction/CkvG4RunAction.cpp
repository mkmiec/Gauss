/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: CkvG4RunAction.cpp,v 1.10 2009-07-03 11:59:49 seaso Exp $
// Include files 

// from Gaudi

// G4
#include "G4UImanager.hh"

// Local
#include "CkvG4RunAction.h"
#include "GaussRICH/RichG4HistoDefineSet1.h"
#include "CherenkovG4HistoDefineSet2.h"
#include "GaussRICH/RichG4HistoDefineSet3.h"
#include "CherenkovG4HistoDefineSet4.h"
#include "CherenkovG4HistoDefineSet5.h"
#include "GaussRICH/RichG4HistoDefineTimer.h"
#include "GaussRICH/RichG4Counters.h"
#include "GaussRICH/RichG4GaussPathNames.h"
#include "GaussRICH/RichG4MatRadIdentifier.h"


//-----------------------------------------------------------------------------
// Implementation file for class : CkvG4RunAction
//
// 2002-08-21 : Sajan Easo
// 2007-01-11 : Gloria Corti, modified for Gaudi v19
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( CkvG4RunAction )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CkvG4RunAction::CkvG4RunAction
( const std::string& type   ,
  const std::string& name   ,
  const IInterface*  parent )
  : GiGaRunActionBase( type , name , parent )
  {}



//=============================================================================
// performe the action at the begin of each run
//=============================================================================
void CkvG4RunAction::BeginOfRunAction( const G4Run* run )
{
  if( 0 == run )
  { Warning("BeginOfRunAction:: G4Run* points to NULL!").ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */); }
  // The part for interactive runnign is commented out.
  /// get Geant4 UI manager
  //  G4UImanager* ui = G4UImanager::GetUIpointer() ;
  // if( 0 == ui    )
  //  { Error("BeginOfRunAction:: G4UImanager* points to NULL!") ; return ; }
  // else
  //  {
  //    for( COMMANDS::const_iterator iCmd = m_beginCmds.value().begin() ;
  //         m_beginCmds.value().end() != iCmd ; ++iCmd )
  //      {
  //        Print("BeginOfRunAction(): execute '" + (*iCmd) + "'" ,
  //              StatusCode::SUCCESS                             , MSG::DEBUG );
  //        ui->ApplyCommand( *iCmd );
  //      }
  //  }
  //  The rich specific histo booking and counters are done only
  // at the first time this BeginRunAction is called.

  if(  m_FirstTimeOfBeginRichRun ) {

    // Also set the radiatormaterial numbers
    
    //  RichG4MatRadIdentifier* aRichG4MatRadIdentifier = RichG4MatRadIdentifier::RichG4MatRadIdentifierInstance();

  if(m_defineRichG4HistoSet1.value()) {

    m_aRichG4HistoSet1 = new RichG4HistoDefineSet1();
  }
  if(m_defineRichG4HistoSet2.value()) {

    m_aRichG4HistoSet2 = new CherenkovG4HistoDefineSet2();
  }

  if(m_defineRichG4HistoSet3.value()) {

    m_aRichG4HistoSet3 = new RichG4HistoDefineSet3();
  }
  if(m_defineRichG4HistoSet4.value()) {

    m_aRichG4HistoSet4 = new CherenkovG4HistoDefineSet4();
  }

  if(m_defineRichG4HistoSet5.value()) {

    m_aRichG4HistoSet5 = new CherenkovG4HistoDefineSet5();
  }

  if(m_defineRichG4HistoTimer.value()) {

    m_aRichG4HistoTimer = new  RichG4HistoDefineTimer();

  }

  // Now to create the RichCounters.
  // done in Event action.
  // RichG4Counters* aRichCounter=  RichG4Counters::getInstance();

  m_FirstTimeOfBeginRichRun= false;
  }
  
}


//=============================================================================
// perform the action at the end of each run
//=============================================================================
void CkvG4RunAction::EndOfRunAction( const G4Run* run )
{
  if( 0 == run )
  { Warning("EndOfRunAction:: G4Run* points to NULL!").ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */); }
  //  info()<<" Now in CkvG4RunAction End of run action "<<endmsg;
  
  // the part for the interactive running of G4 commented out.
  /// get Geant4 UI manager
  //  G4UImanager* ui = G4UImanager::GetUIpointer() ;
  // if( 0 == ui    )
  //  { Error("EndOfRunAction:: G4UImanager* points to NULL!") ; }
  // else
  //  {
  //    for( COMMANDS::const_iterator iCmd = m_endCmds.value().begin() ;
  //          m_endCmds.value().end() != iCmd ; ++iCmd )
  //      {
  //        Print("EndOfRunAction(): execute '" + (*iCmd) + "'" ,
  //              StatusCode::SUCCESS                           , MSG::DEBUG );
  //        ui->ApplyCommand( *iCmd );
  //      }
  //  }
  // if(m_defineRichG4HistoSet5.value()) {
  //  m_aRichG4HistoSet5->FillPmtOccpHisto();
  //  
  //  
  // }
  
}


//=============================================================================

