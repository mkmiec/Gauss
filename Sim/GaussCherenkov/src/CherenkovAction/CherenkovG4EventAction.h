/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: CherenkovG4EventAction.h,v 1.11 2007-01-12 15:32:03 ranjard Exp $
#ifndef CherenkovG4EventAction_h
#define CherenkovG4EventAction_h 1

// Include files
// from Gaudi
#include "GaussCherenkov/CkvG4HitCollName.h"

// GiGa
#include <vector>
#include "GiGa/GiGaEventActionBase.h"
#include "CkvG4HistoFillSet1.h"
#include "CherenkovG4HistoFillSet2.h"
#include "GaussRICH/RichG4HistoFillSet3.h"
#include "CherenkovG4HistoFillSet4.h"
#include "CherenkovG4HistoFillSet5.h"
#include "GaussRICH/RichG4HistoFillTimer.h"
#include "CkvG4EventHitCount.h"
#include "CherenkovG4HitRecon.h"
#include "GaussRICH/RichG4InputMon.h"
#include "CherenkovG4HistoHitTime.h"

/** @class CherenkovG4EventAction CherenkovG4EventAction.h src/RichActions/CherenkovG4EventAction.h
 *
 *
 *  @author Sajan Easo
 *  @date   2011-03-07
 */
class CherenkovG4EventAction : public GiGaEventActionBase
{
public:

 /** standard constructor
   *  @see GiGaEventActionBase
   *  @see GiGaBase
   *  @see AlgTool
   *  @param type type of the object (?)
   *  @param name name of the object
   *  @param parent  pointer to parent object
   */
  CherenkovG4EventAction
  ( const std::string& type   ,
    const std::string& name   ,
    const IInterface*  parent ) ;

  ///  destructor (virtual and protected)
  virtual ~CherenkovG4EventAction();

  /// initialize
  StatusCode initialize() override;

  /// finalize
  StatusCode finalize() override;

  void BeginOfEventAction ( const G4Event* ) override;
  void EndOfEventAction   ( const G4Event* ) override;
  int RichG4CollectionID(int CollNum )
   {return   m_RichG4CollectionID[CollNum]; }
  ///
  CkvG4HitCollName* RichHitCName() {return  m_RichHitCName; }
  int NumRichColl(){
    return  m_NumRichColl;
  }

  CkvG4HistoFillSet1* GetRichG4HistoFillSet1()
  {
    return m_RichG4HistoFillSet1;
  }
  CherenkovG4HistoFillSet2* GetRichG4HistoFillSet2()
  {
    return m_RichG4HistoFillSet2;
  }
  RichG4HistoFillSet3* GetRichG4HistoFillSet3()
  {
    return m_RichG4HistoFillSet3;
  }

  CherenkovG4HistoFillSet4* GetRichG4HistoFillSet4()
  {
    return m_RichG4HistoFillSet4;
  }
  CherenkovG4HistoFillSet5* GetCherenkovG4HistoFillSet5()
  {
    return m_CherenkovG4HistoFillSet5;
  }


  RichG4HistoFillTimer* GetRichG4HistoFillTimer()
  {
    return m_RichG4HistoFillTimer;
  }

  int RichEventActionVerboseLevel()
  {
    return  m_RichEventActionVerboseLevel;
  }

  CkvG4EventHitCount* RichG4EventHitCounter()
  {

    return m_RichG4EventHitCounter;
  }


  void   PrintRichG4HitCounters();


  bool RichEventActionHistoFillActivateSet1()
  {
    return  m_RichEventActionHistoFillActivateSet1;
  }
  bool RichEventActionHistoFillActivateSet2()
  {
    return  m_RichEventActionHistoFillActivateSet2;
  }
  bool RichEventActionHistoFillActivateSet3()
  {
    return  m_RichEventActionHistoFillActivateSet3;
  }
  bool RichEventActionHistoFillActivateTimer()
  {
    return  m_RichEventActionHistoFillActivateTimer;
  }
  CherenkovG4HitRecon* getRichG4HitRecon()
  {
    return m_RichG4HitRecon;
  }

  bool RichG4EventActivateCkvRecon()
  {
    return  m_RichG4EventActivateCkvRecon;
  }
  bool  RichG4HistoActivateQw() {
    return m_RichG4HistoActivateQw;
  }

  RichG4InputMon* getRichG4InputMon()
  {    return m_RichG4InputMon;}


  bool RichG4InputMonActivate()
  {
    return m_RichG4InputMonActivate;
  }
  bool RichG4HitReconUseStdRadHit() {
    return m_RichG4HitReconUseStdRadHit;}
  void setRichG4HitReconUseStdRadHit( bool afl ) {
    m_RichG4HitReconUseStdRadHit= afl;}

  bool IsRichG4FirstEvent() {return m_IsRichG4FirstEvent;}



private:

  CherenkovG4EventAction() ; ///< no default constructor
  CherenkovG4EventAction( const CherenkovG4EventAction& ) ; ///< no copy
  CherenkovG4EventAction& operator=( const CherenkovG4EventAction& ) ; ///< no =

  template <typename TOOL> inline
  void delPointer( TOOL *& tool ) {
    if ( 0 != tool ) { delete tool; tool = 0; }
  }

private:

  std::vector<int> m_RichG4CollectionID;
  int m_NumRichColl{0};
  int m_NumRichClassicColl{0};
  CkvG4HitCollName* m_RichHitCName{nullptr};
  CkvG4HistoFillSet1* m_RichG4HistoFillSet1{nullptr};
  CherenkovG4HistoFillSet2* m_RichG4HistoFillSet2{nullptr};
  RichG4HistoFillSet3* m_RichG4HistoFillSet3{nullptr};
  CherenkovG4HistoFillSet4* m_RichG4HistoFillSet4{nullptr};
  CherenkovG4HistoFillSet5* m_CherenkovG4HistoFillSet5{nullptr};
  RichG4HistoFillTimer* m_RichG4HistoFillTimer{nullptr};
  CkvG4EventHitCount* m_RichG4EventHitCounter{nullptr};
  CherenkovG4HitRecon* m_RichG4HitRecon{nullptr};
  RichG4InputMon* m_RichG4InputMon{nullptr};


  Gaudi::Property<int> m_RichEventActionVerboseLevel{this,"RichEventActionVerbose",0,"RichEventActionVerbose"};
  Gaudi::Property<bool> m_RichEventActionHistoFillActivateSet1{this,"RichEventActionHistoFillSet1",false,"Turn on RichEventActionHistoFillSet1"};
  Gaudi::Property<bool> m_RichEventActionHistoFillActivateSet2{this,"RichEventActionHistoFillSet2",false,"Turn on RichEventActionHistoFillSet2"};
  Gaudi::Property<bool> m_RichEventActionHistoFillActivateSet3{this,"RichEventActionHistoFillSet3",false,"Turn on RichEventActionHistoFillSet3"};
  Gaudi::Property<bool> m_RichEventActionHistoFillActivateSet4{this,"RichEventActionHistoFillSet4",false,"Turn on RichEventActionHistoFillSet4"};
  Gaudi::Property<bool> m_RichEventActionHistoFillActivateSet5{this,"RichEventActionHistoFillSet5",false,"Turn on RichEventActionHistoFillSet5"};
  Gaudi::Property<bool> m_RichEventActionHistoFillActivateTimer{this,"RichEventActionHistoFillTimer",false,"Turn on RichEventActionHistoFillTimer"};
  Gaudi::Property<bool> m_RichG4EventHitActivateCount{this,"RichG4EventActivateCounting",false,"Turn on RichG4EventActivateCounting"};
  Gaudi::Property<bool> m_RichG4EventActivateCkvRecon{this,"RichG4EventActivateCkvReconstruction",false,"Turn on RichG4EventActivateCkvReconstruction"};
  Gaudi::Property<bool> m_RichG4HistoActivateQw{this,"RichG4QuartzWindowCkvHistoActivate",false,"Turn on RichG4QuartzWindowCkvHistoActivate"};

  Gaudi::Property<bool> m_RichG4HitReconUseSatHit{this,"RichG4EventHitReconUseSaturatedHit",true,"Turn on RichG4EventHitReconUseSaturatedHit"};
  Gaudi::Property<bool> m_RichG4HitReconUseStdRadHit{this,"RichG4EventHitReconUseStdRadiatorHit",true,"Turn on RichG4EventHitReconUseStdRadiatorHit"};
  Gaudi::Property<bool> m_RichG4HitReconUseMidRadiator{this,"RichG4EventHitReconUseMidRadiator",false,"Turn on RichG4EventHitReconUseMidRadiator"};

  Gaudi::Property<bool> m_RichG4InputMonActivate{this,"RichG4InputMonitorActivate",false,"RichG4InputMonitorActivate"};

  bool m_IsRichG4FirstEvent{true};

  Gaudi::Property<bool> m_CkvG4HitReconUseOnlySignalHit{this,"RichG4HitReconUseSignalHit",false,"RichG4HitReconUseSignalHit"};
  Gaudi::Property<bool> m_CkvG4HitReconUseOnlyHighMom{this,"RichG4HitReconUseHighMomTk",false,"RichG4HitReconUseHighMomTk"};

  Gaudi::Property<bool> m_CkvHistoHitTimeActivate{this,"CkvHistoHitTimeActivate",false,"CkvHistoHitTimeActivate"};
  Gaudi::Property<std::string> m_CkvHistoHitTimeNtupleFileName{this,"CkvHistoHitTimeNtupleFileName","DummyNtupleHitTimeFileName","CkvHistoHitTimeNtupleFileName"};



};

#endif  // end of CherenkovG4EventAction_H
