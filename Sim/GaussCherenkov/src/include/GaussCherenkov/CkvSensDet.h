/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: CkvSensDet.h,v 1.6 2009-07-17 13:46:13 jonrob Exp $
#ifndef       CkvSensDet_H
#define       CkvSensDet_H 1

// Include files
// from GiGa
#include "GiGa/GiGaSensDetBase.h"

// local
#include "GaussCherenkov/CkvG4Hit.h"
#include "GaussRICH/RichG4HitCollName.h"
#include "CkvG4GeomProp.h"
#include <map>

// forward declarations
class G4HCofThisEvent;

/** @class CkvSensDet CkvSensDet.h src/SensDet/CkvSensDet.h
 *
 *
 *  @author Sajan Easo
 *  @date   2002-05-24, last modified 2007-01-11
 */

class CkvSensDet: virtual public GiGaSensDetBase
{

public:

  /** standard constructor
   *  @see GiGaSensDetBase
   *  @see GiGaBase
   *  @see AlgTool
   *  @param type type of the object (?)
   *  @param name name of the object
   *  @param parent  pointer to parent object
   */
  CkvSensDet
  ( const std::string& type   ,
    const std::string& name   ,
    const IInterface*  parent ) ;

  /// destructor (virtual and protected)
  ~CkvSensDet();
  /// initialize
  StatusCode initialize() override;

  /// finalize
  StatusCode finalize() override;

  /** process the hit
   *  @param step     pointer to current Geant4 step
   *  @param history  pointert to touchable history
   */
  void Initialize(G4HCofThisEvent* HCE) override;
  // void EndOfEvent(G4HCofThisEvent* HCE) override;
  bool ProcessHits( G4Step* step,    G4TouchableHistory* history ) override;
  void clear() override;
  void DrawAll() override;
  void PrintAll() override;

  void InitPmtHC();



  CkvG4GeomProp*  RichGeomProperty() {return  m_RichGeomProperty; }
  RichG4HitCollName* RichG4HCName() {return m_RichG4HCName; }
  G4int NumberofRichDet() {return  m_RichGeomProperty->NumberOfRichDet(); }

  G4int NumberOfDetSectionsInRich1Det() const
  {return m_RichGeomProperty->NumberOfDetSectionsInRich1() ; }

  G4int NumberOfDetSectionsInRich2Det() const
  {return m_RichGeomProperty->NumberOfDetSectionsInRich2(); }

  G4int NumberofPixelsInPMTofRich() const
  {return m_RichGeomProperty->NumberOfPixelsInPMT(); }

  G4double MaxZHitInRich1Detector() const
  {return m_RichGeomProperty->MaxZHitInRich1Det(); }

  const std::vector<G4double> & PixelBoundaryInX() const
  {return m_RichGeomProperty->PixelXBoundary(); }

  const std::vector<G4double> & PixelBoundaryInY() const
  {return m_RichGeomProperty->PixelYBoundary(); }

  G4double PixelBoundaryInXValue(const int aPXNumber ) const
  {return  m_RichGeomProperty->PixelXBoundaryValue(aPXNumber) ;}

  G4double PixelBoundaryInYValue(const int aPYNumber ) const
  {return  m_RichGeomProperty->PixelYBoundaryValue(aPYNumber) ;}

  G4int PixelXNum(const G4double localXCoord ) const
  {return m_RichGeomProperty->PixelXNumFromCoord(localXCoord) ;}

  G4int PixelYNum( const G4double localYCoord ) const
  {return m_RichGeomProperty->PixelYNumFromCoord(localYCoord) ;}

  G4int GrandPixelXNum(const G4double localXCoord ) const
  {return m_RichGeomProperty->GrandPixelXNumFromCoord(localXCoord) ;}

  G4int GrandPixelYNum ( const G4double localYCoord ) const
  {return m_RichGeomProperty->GrandPixelYNumFromCoord(localYCoord) ;}

  void ResetPmtMapInCurrentEvent();

private:
  ///

  CkvSensDet(); ///< no default constructor
  CkvSensDet( const CkvSensDet& ); ///< no copy constructor
  CkvSensDet& operator=( const CkvSensDet& ) ; ///< no =


  ///
private:
  ///
  CkvG4GeomProp* m_RichGeomProperty;
  std::vector<G4int> m_Rich1PhdSDID;
  std::vector<G4int> m_Rich2PhdSDID;

  RichG4HitCollName* m_RichG4HCName;
  G4int m_NumberOfHCInRICH;
  std::vector<CkvG4HitsCollection*>  m_RichHC;
  std::vector<G4int> m_PhdHCID;
  bool  m_SuperRichFlag;
  bool  m_OptHorizRichFlag;
  bool  m_Rich2UseGrandPmt;

  Gaudi::Property<bool> m_RichPmtAviodDuplicateHitsActivate{this,"RichPmtAviodDuplicateHitsActivate",
    false,"RichPmtAviodDuplicateHitsActivate"};
  Gaudi::Property<bool> m_RichPmtFlagDuplicateHitsActivate{this,"RichPmtFlagDuplicateHitsactivate",
    true,"RichPmtFlagDuplicateHitsactivate"};
  G4int m_TotNumPmtsInRich{5000};
  std::vector<bool> m_RichPmtAlreadyHit{std::vector<bool>(5000)}; // flag for pmt to have hits in current event
  std::multimap<G4int,G4int> m_RichPmtToPixelNumMap; // map between pmt and Pixelnum for each hit in current event
  //  bool m_CkvSensInitFlag;
  // bool m_CkvSensInitEventFlag;


};

#endif
