/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENERATORS_FIXEDLUMINOSITY_H
#define GENERATORS_FIXEDLUMINOSITY_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

#include "Generators/IPileUpTool.h"

class IGaussRedecayStr;  ///< GaussRedecay storage service

/** @class GaussRedecayFakePileUp GaussRedecayFakePileUp.h
 * "GaussRedecayFakePileUp.h"
 *
 *  Tool to get the number of pileup events to redecay.
 *  Basically just a getter to have Generation get the pileup from the
 *  service without code changes to Generation
 *
 *  @author Dominik Muller
 *  @date   2016-05-28
 */
class GaussRedecayFakePileUp : public GaudiTool, virtual public IPileUpTool {
  public:
  /// Standard constructor
  GaussRedecayFakePileUp(const std::string& type, const std::string& name,
                         const IInterface* parent);

  StatusCode initialize() override;

  StatusCode finalize() override;

  unsigned int numberOfPileUp() override;

  void printPileUpCounters() override {};

  private:
  std::string m_gaussRDSvcName;
  IGaussRedecayStr* m_gaussRDStrSvc = nullptr;
};
#endif  // GENERATORS_FIXEDLUMINOSITY_H
