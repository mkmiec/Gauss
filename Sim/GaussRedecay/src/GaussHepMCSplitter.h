/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GaussHepMCSplitter_H
#define GaussHepMCSplitter_H 1

// Include files
// from Gaudi
#include "Event/HepMCEvent.h"
#include "GaudiAlg/GaudiAlgorithm.h"

// forward declarations
class IGaussRedecayStr;  ///< GaussRedecay storage service
class IGaussRedecayCtr;  ///< GaussRedecay counter service
class IProductionTool;

namespace HepMC {
class GenParticle;
}

namespace LHCb {
class IParticlePropertySvc;
}

/** @class GaussHepMCSplitter GaussHepMCSplitter.h
 *
 *Based on GaussRedecaySorter
 Simplified to store all stable photons from the signal vertex in the service to abuse as many redecay functionality as possible
 *
 *  @author Dominik Muller
 *  @date   2018-4-18
 */
class GaussHepMCSplitter : public GaudiAlgorithm {
  public:
  GaussHepMCSplitter(const std::string& Name, ISvcLocator* SvcLoc);

  virtual StatusCode initialize() override;  ///< Algorithm initialization
  virtual StatusCode execute() override;     ///< Algorithm execution

  private:
  std::string m_gaussRDSvcName;
  IGaussRedecayStr* m_gaussRDStrSvc = nullptr;
  IGaussRedecayCtr* m_gaussRDCtrSvc = nullptr;
  LHCb::IParticlePropertySvc* m_ppSvc = nullptr;
  HepMC::GenParticle* m_theSignal = nullptr;
  int m_current_pileup = 0;

  bool m_store_fail = false;

  std::string m_generationLocation;
  std::string m_outputGenerationLocation;
  std::string m_outputCollisionLocation;
  // Variables for photon selection
  std::string m_mother;
  int m_motherID;
  bool m_from_signal;
  int m_maxSearchDepth;
  bool m_matchSearchDepth;

  std::string m_productionToolName;
  IProductionTool* m_productionTool;

  HepMC::GenParticle* find_signal(LHCb::HepMCEvents* evts);
  void store_particle(HepMC::GenParticle*);
  /*Decay everything that is heavier than the signal (Only in the signal event
   * for now.)*/
  void printChildren(HepMC::GenParticle*, int level = 0);

  void prepareInteraction( LHCb::HepMCEvents * theEvents ,
      LHCb::GenCollisions * theCollisions , HepMC::GenEvent * & theGenEvent ,  
      LHCb::GenCollision * & theGenCollision ) const;

};

#endif  // GaussHepMCSplitter_H
