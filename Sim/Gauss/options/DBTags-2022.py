###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# File with LATEST database tags for Sim10
# YEAR = 2022 for pp-collisions
from Configurables import LHCbApp
from Configurables import CondDB

# The upgrade branch in DDDB and SIMCOND need to be selected via CondDB()
CondDB().Upgrade     = True
# The latest tags are the following
LHCbApp().DDDBtag   = "dddb-20220323"
LHCbApp().CondDBtag = "sim-20220509-vc-md100"
