###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##
## Simulate a triangular gas profile in the SMOG2 cell
## 
from Configurables import Generation
from Configurables import TriangZSmearVertex

for algName in ["BeamGasGeneration", "GasBeamGeneration"]:
    generation=Generation(algName)
    generation.VertexSmearingTool = "TriangZSmearVertex"
    generation.addTool( TriangZSmearVertex )
    generation.TriangZSmearVertex.ZMin = -540.
    generation.TriangZSmearVertex.ZMax = -340.


