/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $

#ifndef G4XiccStarPlus_h
#define G4XiccStarPlus_h 1

#include "globals.hh"
#include "G4ios.hh"
#include "G4ParticleDefinition.hh"

// ######################################################################
// ###                         XiccStarPlus                        ###
// ######################################################################

class G4XiccStarPlus : public G4ParticleDefinition
{
 private:
  static G4XiccStarPlus * theInstance ;
  G4XiccStarPlus( ) { }
  ~G4XiccStarPlus( ) { }


 public:
  static G4XiccStarPlus * Definition() ;
  static G4XiccStarPlus * XiccStarPlusDefinition() ;
  static G4XiccStarPlus * XiccStarPlus() ;
};


#endif
