/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $

#ifndef G4XibcZero_h
#define G4XibcZero_h 1

#include "globals.hh"
#include "G4ios.hh"
#include "G4ParticleDefinition.hh"

// ######################################################################
// ###                         XibcZero                        ###
// ######################################################################

class G4XibcZero : public G4ParticleDefinition
{
 private:
  static G4XibcZero * theInstance ;
  G4XibcZero( ) { }
  ~G4XibcZero( ) { }


 public:
  static G4XibcZero * Definition() ;
  static G4XibcZero * XibcZeroDefinition() ;
  static G4XibcZero * XibcZero() ;
};


#endif
