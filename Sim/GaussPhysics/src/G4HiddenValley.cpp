/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "G4ParticleTable.hh"
#include "GaudiKernel/PhysicalConstants.h"
#include "GiGa/GiGaException.h"
#include "G4HiddenValley.h"

/* ###############################################################################
 * ### Hidden Valley particles for Dark Showers (HV processes in Pythia 8.230) ###
 * ### G4Higgses used as a template to write this file (author: Vanya Belyaev) ###
 * ### 2018-02-03 - written by Carlos Vazquez Sierra (carlos.vazquez@cern.ch)  ###
 * ### 2020-03-09 - revisited by CVS for inclusion on Gauss master             ###
 * ###############################################################################
 * id="4900111" name="pivDiag" spinType="1" chargeType="0" colType="0" m0="10.00000" 
 * id="4900113" name="rhovDiag" spinType="3" chargeType="0" colType="0" m0="10.00000" 
 * id="4900023" name="Zv" spinType="3" chargeType="0" colType="0" m0="1000.00000" mWidth="20.00000" mMin="100.00000" mMax="0.00000" 
 * id="4900101" name="qv" antiName="qvbar" spinType="1" chargeType="0" colType="0" m0="100.00000"
 * id="4900211" name="pivUp" antiName="pivDn" spinType="1" chargeType="0" colType="0" m0="10.00000"
 * id="4900213" name="rhovUp" antiName="rhovDn" spinType="3" chargeType="0" colType="0" m0="10.00000"
 * id="4900021" name="gv" spinType="3" chargeType="0" colType="0" m0="0.00000" 
 */

/// initialize the static instance
G4pivDiag*  G4pivDiag::s_instance = 0 ;
G4pivUp*    G4pivUp::s_instance = 0 ;
G4pivDn*    G4pivDn::s_instance = 0 ;

G4rhovDiag*  G4rhovDiag::s_instance = 0 ;
G4rhovUp*    G4rhovUp::s_instance = 0 ;
G4rhovDn*    G4rhovDn::s_instance = 0 ;

G4Zv* G4Zv::s_instance = 0 ;

G4gv* G4gv::s_instance = 0 ;

G4qv*    G4qv::s_instance = 0 ;
G4qvbar* G4qvbar::s_instance = 0 ;

// ================================================================================================
// id="4900111" name="pivDiag" spinType="1" chargeType="0" colType="0" m0="10.00000" 
// id="4900211" name="pivUp" antiName="pivDn" spinType="1" chargeType="0" colType="0" m0="10.00000"
// ================================================================================================
// Define the particles:
G4pivDiag::G4pivDiag  
( const double mass , 
  const double ctau ) 
  : G4ParticleDefinition 
( "pivDiag"                               , // the name 
  mass                                    , // the mass   
  Gaudi::Units::hbarc / ctau              , // the width  
  0                                       , // the charge 
  0                                       , // the spin 
  0                                       , // the parity
  0                                       , // the conjugation 
  0                                       , // the isospin 
  0                                       , // the z-projection of isospin
  0                                       , // the G-parity
  "hiddenvalley"                          , // p-type 
  0                                       , // lepton 
  0                                       , // baryon 
  4900111                                 , // PDG encoding 
  15 * Gaudi::Units::meter         < ctau , // stable 
  ctau / Gaudi::Units::c_light            , // lifetime 
  NULL                                    , // decay table 
  1.e-3 * Gaudi::Units::micrometer > ctau , // shortlived 
  "hiddenvalley"                          , // subtype    
  4900111                                   // antiparticle  
  )
{}

G4pivUp::G4pivUp  
( const double mass , 
  const double ctau ) 
  : G4ParticleDefinition 
( "pivUp"                                 , // the name 
  mass                                    , // the mass   
  Gaudi::Units::hbarc / ctau              , // the width  
  0                                       , // the charge 
  0                                       , // the spin 
  0                                       , // the parity
  0                                       , // the conjugation 
  0                                       , // the isospin 
  0                                       , // the z-projection of isospin
  0                                       , // the G-parity
  "hiddenvalley"                          , // p-type 
  0                                       , // lepton 
  0                                       , // baryon 
  4900211                                 , // PDG encoding 
  15 * Gaudi::Units::meter         < ctau , // stable 
  ctau / Gaudi::Units::c_light            , // lifetime 
  NULL                                    , // decay table 
  1.e-3 * Gaudi::Units::micrometer > ctau , // shortlived 
  "hiddenvalley"                          , // subtype    
  -4900211                                   // antiparticle  
  )
{}

G4pivDn::G4pivDn  
( const double mass , 
  const double ctau ) 
  : G4ParticleDefinition 
( "pivDn"                                 , // the name 
  mass                                    , // the mass   
  Gaudi::Units::hbarc / ctau              , // the width  
  0                                       , // the charge 
  0                                       , // the spin 
  0                                       , // the parity
  0                                       , // the conjugation 
  0                                       , // the isospin 
  0                                       , // the z-projection of isospin
  0                                       , // the G-parity
  "hiddenvalley"                          , // p-type 
  0                                       , // lepton 
  0                                       , // baryon 
  -4900211                                , // PDG encoding 
  15 * Gaudi::Units::meter         < ctau , // stable 
  ctau / Gaudi::Units::c_light            , // lifetime 
  NULL                                    , // decay table 
  1.e-3 * Gaudi::Units::micrometer > ctau , // shortlived 
  "hiddenvalley"                          , // subtype    
  4900211                                   // antiparticle  
  )
{}

// Create the particles:
G4pivDiag* G4pivDiag::Create 
( const double mass ,                                    // the mass 
  const double ctau )                                    // the lifetime 
{
  if ( 0 != s_instance ) { return s_instance ; }
  // check the presence of the particle in particle table:
  G4ParticleTable* table = G4ParticleTable::GetParticleTable();
  if ( 0 == table ) 
  { throw GiGaException ("G4pivDiag::Create: Invalid G4ParticleTable") ; }
  G4ParticleDefinition* check1 = table->FindParticle("pivDiag");
  if ( 0 != check1 ) 
  { throw GiGaException ("G4pivDiag::Create: pivDiag already exists!") ; }
  G4ParticleDefinition* check2 = table->FindParticle( 4900111 );
  if ( 0 != check2 ) 
  { throw GiGaException ("G4pivDiag::Create: PDGID(4900111) already exists!") ; }
  // create the particle: 
  s_instance = new G4pivDiag ( mass , ctau ) ;
  return s_instance ;
}

G4pivUp* G4pivUp::Create 
( const double mass ,                                    // the mass 
  const double ctau )                                    // the lifetime 
{
  if ( 0 != s_instance ) { return s_instance ; }
  // check the presence of the particle in particle table:
  G4ParticleTable* table = G4ParticleTable::GetParticleTable();
  if ( 0 == table ) 
  { throw GiGaException ("G4pivUp::Create: Invalid G4ParticleTable") ; }
  G4ParticleDefinition* check1 = table->FindParticle("pivUp");
  if ( 0 != check1 ) 
  { throw GiGaException ("G4pivUp::Create: pivUp already exists!") ; }
  G4ParticleDefinition* check2 = table->FindParticle( 4900211 );
  if ( 0 != check2 ) 
  { throw GiGaException ("G4pivUp::Create: PDGID(4900211) already exists!") ; }
  // create the particle: 
  s_instance = new G4pivUp ( mass , ctau ) ;
  return s_instance ;
}

G4pivDn* G4pivDn::Create 
( const double mass ,                                    // the mass 
  const double ctau )                                    // the lifetime 
{
  if ( 0 != s_instance ) { return s_instance ; }
  // check the presence of the particle in particle table:
  G4ParticleTable* table = G4ParticleTable::GetParticleTable();
  if ( 0 == table ) 
  { throw GiGaException ("G4pivDn::Create: Invalid G4ParticleTable") ; }
  G4ParticleDefinition* check1 = table->FindParticle("pivDn");
  if ( 0 != check1 ) 
  { throw GiGaException ("G4pivDn::Create: pivDn already exists!") ; }
  G4ParticleDefinition* check2 = table->FindParticle( -4900211 );
  if ( 0 != check2 ) 
  { throw GiGaException ("G4pivDn::Create: PDGID(-4900211) already exists!") ; }
  // create the particle: 
  s_instance = new G4pivDn ( mass , ctau ) ;
  return s_instance ;
}

// ==================================================================================================
// id="4900113" name="rhovDiag" spinType="3" chargeType="0" colType="0" m0="10.00000" 
// id="4900213" name="rhovUp" antiName="rhovDn" spinType="3" chargeType="0" colType="0" m0="10.00000"
// ==================================================================================================
// Define the particles:
G4rhovDiag::G4rhovDiag  
( const double mass , 
  const double ctau ) 
  : G4ParticleDefinition 
( "rhovDiag"                              , // the name 
  mass                                    , // the mass   
  Gaudi::Units::hbarc / ctau              , // the width  
  0                                       , // the charge 
  2                                       , // the spin 
  0                                       , // the parity
  0                                       , // the conjugation 
  0                                       , // the isospin 
  0                                       , // the z-projection of isospin
  0                                       , // the G-parity
  "hiddenvalley"                          , // p-type 
  0                                       , // lepton 
  0                                       , // baryon 
  4900113                                 , // PDG encoding 
  15 * Gaudi::Units::meter         < ctau , // stable 
  ctau / Gaudi::Units::c_light            , // lifetime 
  NULL                                    , // decay table 
  1.e-3 * Gaudi::Units::micrometer > ctau , // shortlived 
  "hiddenvalley"                          , // subtype    
  4900113                                   // antiparticle  
  )
{}

G4rhovUp::G4rhovUp  
( const double mass , 
  const double ctau ) 
  : G4ParticleDefinition 
( "rhovUp"                                 , // the name 
  mass                                    , // the mass   
  Gaudi::Units::hbarc / ctau              , // the width  
  0                                       , // the charge 
  2                                       , // the spin 
  0                                       , // the parity
  0                                       , // the conjugation 
  0                                       , // the isospin 
  0                                       , // the z-projection of isospin
  0                                       , // the G-parity
  "hiddenvalley"                          , // p-type 
  0                                       , // lepton 
  0                                       , // baryon 
  4900213                                 , // PDG encoding 
  15 * Gaudi::Units::meter         < ctau , // stable 
  ctau / Gaudi::Units::c_light            , // lifetime 
  NULL                                    , // decay table 
  1.e-3 * Gaudi::Units::micrometer > ctau , // shortlived 
  "hiddenvalley"                          , // subtype    
  -4900213                                  // antiparticle  
  )
{}

G4rhovDn::G4rhovDn  
( const double mass , 
  const double ctau ) 
  : G4ParticleDefinition 
( "rhovDn"                                , // the name 
  mass                                    , // the mass   
  Gaudi::Units::hbarc / ctau              , // the width  
  0                                       , // the charge 
  2                                       , // the spin 
  0                                       , // the parity
  0                                       , // the conjugation 
  0                                       , // the isospin 
  0                                       , // the z-projection of isospin
  0                                       , // the G-parity
  "hiddenvalley"                          , // p-type 
  0                                       , // lepton 
  0                                       , // baryon 
  -4900213                                , // PDG encoding 
  15 * Gaudi::Units::meter         < ctau , // stable 
  ctau / Gaudi::Units::c_light            , // lifetime 
  NULL                                    , // decay table 
  1.e-3 * Gaudi::Units::micrometer > ctau , // shortlived 
  "hiddenvalley"                          , // subtype    
  4900213                                   // antiparticle  
  )
{}

// Create the particles:
G4rhovDiag* G4rhovDiag::Create 
( const double mass ,                                    // the mass 
  const double ctau )                                    // the lifetime 
{
  if ( 0 != s_instance ) { return s_instance ; }
  // check the presence of the particle in particle table:
  G4ParticleTable* table = G4ParticleTable::GetParticleTable();
  if ( 0 == table ) 
  { throw GiGaException ("G4rhovDiag::Create: Invalid G4ParticleTable") ; }
  G4ParticleDefinition* check1 = table->FindParticle("rhovDiag");
  if ( 0 != check1 ) 
  { throw GiGaException ("G4rhovDiag::Create: rhovDiag already exists!") ; }
  G4ParticleDefinition* check2 = table->FindParticle( 4900113 );
  if ( 0 != check2 ) 
  { throw GiGaException ("G4rhovDiag::Create: PDGID(4900113) already exists!") ; }
  // create the particle: 
  s_instance = new G4rhovDiag ( mass , ctau ) ;
  return s_instance ;
}

G4rhovUp* G4rhovUp::Create 
( const double mass ,                                    // the mass 
  const double ctau )                                    // the lifetime 
{
  if ( 0 != s_instance ) { return s_instance ; }
  // check the presence of the particle in particle table:
  G4ParticleTable* table = G4ParticleTable::GetParticleTable();
  if ( 0 == table ) 
  { throw GiGaException ("G4rhovUp::Create: Invalid G4ParticleTable") ; }
  G4ParticleDefinition* check1 = table->FindParticle("rhovUp");
  if ( 0 != check1 ) 
  { throw GiGaException ("G4rhovUp::Create: rhovUp already exists!") ; }
  G4ParticleDefinition* check2 = table->FindParticle( 4900213 );
  if ( 0 != check2 ) 
  { throw GiGaException ("G4rhovUp::Create: PDGID(4900213) already exists!") ; }
  // create the particle: 
  s_instance = new G4rhovUp ( mass , ctau ) ;
  return s_instance ;
}

G4rhovDn* G4rhovDn::Create 
( const double mass ,                                    // the mass 
  const double ctau )                                    // the lifetime 
{
  if ( 0 != s_instance ) { return s_instance ; }
  // check the presence of the particle in particle table:
  G4ParticleTable* table = G4ParticleTable::GetParticleTable();
  if ( 0 == table ) 
  { throw GiGaException ("G4rhovDn::Create: Invalid G4ParticleTable") ; }
  G4ParticleDefinition* check1 = table->FindParticle("rhovDn");
  if ( 0 != check1 ) 
  { throw GiGaException ("G4rhovDn::Create: rhovDn already exists!") ; }
  G4ParticleDefinition* check2 = table->FindParticle( -4900213 );
  if ( 0 != check2 ) 
  { throw GiGaException ("G4rhovDn::Create: PDGID(-4900213) already exists!") ; }
  // create the particle: 
  s_instance = new G4rhovDn ( mass , ctau ) ;
  return s_instance ;
}

// ================================================================================================================================
// id="4900023" name="Zv" spinType="3" chargeType="0" colType="0" m0="1000.00000" mWidth="20.00000" mMin="100.00000" mMax="0.00000" 
// ================================================================================================================================
// Define the particle:
G4Zv::G4Zv   
( const double mass , 
  const double ctau ) 
  : G4ParticleDefinition 
( "Zv"                                    , // the name 
  mass                                    , // the mass   
  Gaudi::Units::hbarc / ctau              , // the width  
  0                                       , // the charge 
  2                                       , // the spin
  0                                       , // the parity
  0                                       , // the conjugation 
  0                                       , // the isospin 
  0                                       , // the z-projection of isospin
  0                                       , // the G-parity
  "hiddenvalley"                          , // p-type 
  0                                       , // lepton 
  0                                       , // baryon 
  4900023                                 , // PDG encoding 
  15 * Gaudi::Units::meter         < ctau , // stable 
  ctau / Gaudi::Units::c_light            , // lifetime 
  NULL                                    , // decay table 
  1.e-3 * Gaudi::Units::micrometer > ctau , // shortlived 
  "hiddenvalley"                          , // subtype    
  4900023                                   // antiparticle  
  )
{}

// Create the particle:
G4Zv* G4Zv::Create 
( const double mass ,                                    // the mass 
  const double ctau )                                    // the lifetime 
{
  if ( 0 != s_instance ) { return s_instance ; }
  // check the presence of the particle in particle table:
  G4ParticleTable* table = G4ParticleTable::GetParticleTable();
  if ( 0 == table ) 
  { throw GiGaException ("G4Zv::Create: Invalid G4ParticleTable") ; }
  G4ParticleDefinition* check1 = table->FindParticle("Zv");
  if ( 0 != check1 ) 
  { throw GiGaException ("G4Zv::Create: Zv already exists!") ; }
  G4ParticleDefinition* check2 = table->FindParticle( 4900023 );
  if ( 0 != check2 ) 
  { throw GiGaException ("G4Zv::Create: PDGID(4900023) already exists!") ; }
  // create the particle: 
  s_instance = new G4Zv ( mass , ctau ) ;
  return s_instance ;
}

// ==============================================================================================
// * id="4900021" name="gv" spinType="3" chargeType="0" colType="0" m0="0.00000" 
// ==============================================================================================
// Define the particle:
G4gv::G4gv   
( const double mass , 
  const double ctau ) 
  : G4ParticleDefinition 
( "gv"                                    , // the name 
  mass                                    , // the mass   
  Gaudi::Units::hbarc / ctau              , // the width  
  0                                       , // the charge 
  2                                       , // the spin
  0                                       , // the parity
  0                                       , // the conjugation 
  0                                       , // the isospin 
  0                                       , // the z-projection of isospin
  0                                       , // the G-parity
  "hiddenvalley"                          , // p-type 
  0                                       , // lepton 
  0                                       , // baryon 
  4900021                                 , // PDG encoding 
  15 * Gaudi::Units::meter         < ctau , // stable 
  ctau / Gaudi::Units::c_light            , // lifetime 
  NULL                                    , // decay table 
  1.e-3 * Gaudi::Units::micrometer > ctau , // shortlived 
  "hiddenvalley"                          , // subtype    
  4900021                                   // antiparticle  
  )
{}

// Create the particle:
G4gv* G4gv::Create 
( const double mass ,                                    // the mass 
  const double ctau )                                    // the lifetime 
{
  if ( 0 != s_instance ) { return s_instance ; }
  // check the presence of the particle in particle table:
  G4ParticleTable* table = G4ParticleTable::GetParticleTable();
  if ( 0 == table ) 
  { throw GiGaException ("G4gv::Create: Invalid G4ParticleTable") ; }
  G4ParticleDefinition* check1 = table->FindParticle("gv");
  if ( 0 != check1 ) 
  { throw GiGaException ("G4gv::Create: gv already exists!") ; }
  G4ParticleDefinition* check2 = table->FindParticle( 4900021 );
  if ( 0 != check2 ) 
  { throw GiGaException ("G4gv::Create: PDGID(4900021) already exists!") ; }
  // create the particle: 
  s_instance = new G4gv ( mass , ctau ) ;
  return s_instance ;
}

// ==============================================================================================
// id="4900101" name="qv" antiName="qvbar" spinType="1" chargeType="0" colType="0" m0="100.00000"
// ==============================================================================================
// Define the particles:
G4qv::G4qv   
( const double mass , 
  const double ctau ) 
  : G4ParticleDefinition 
( "qv"                                    , // the name 
  mass                                    , // the mass   
  Gaudi::Units::hbarc / ctau              , // the width  
  0                                       , // the charge 
  0                                       , // the spin
  0                                       , // the parity
  0                                       , // the conjugation 
  0                                       , // the isospin 
  0                                       , // the z-projection of isospin
  0                                       , // the G-parity
  "hiddenvalley"                          , // p-type 
  0                                       , // lepton (qv is a quark - setting it to zero just in case)
  0                                       , // baryon 
  4900101                                 , // PDG encoding 
  15 * Gaudi::Units::meter         < ctau , // stable 
  ctau / Gaudi::Units::c_light            , // lifetime 
  NULL                                    , // decay table 
  1.e-3 * Gaudi::Units::micrometer > ctau , // shortlived 
  "hiddenvalley"                          , // subtype    
  -4900101                                  // antiparticle (this one has antiparticle)
  )
{}

G4qvbar::G4qvbar  
( const double mass , 
  const double ctau ) 
  : G4ParticleDefinition 
( "qvbar"                                 , // the name 
  mass                                    , // the mass   
  Gaudi::Units::hbarc / ctau              , // the width  
  0                                       , // the charge 
  0                                       , // the spin
  0                                       , // the parity
  0                                       , // the conjugation 
  0                                       , // the isospin 
  0                                       , // the z-projection of isospin
  0                                       , // the G-parity
  "hiddenvalley"                          , // p-type 
  0                                       , // lepton (qv is a quark - setting it to zero just in case)
  0                                       , // baryon 
  -4900101                                 , // PDG encoding 
  15 * Gaudi::Units::meter         < ctau , // stable 
  ctau / Gaudi::Units::c_light            , // lifetime 
  NULL                                    , // decay table 
  1.e-3 * Gaudi::Units::micrometer > ctau , // shortlived 
  "hiddenvalley"                          , // subtype    
  4900101                                  // antiparticle (this one has antiparticle)
  )
{}

// Create the particles:
G4qv* G4qv::Create 
( const double mass ,                                    // the mass 
  const double ctau )                                    // the lifetime 
{
  if ( 0 != s_instance ) { return s_instance ; }
  // check the presence of the particle in particle table:
  G4ParticleTable* table = G4ParticleTable::GetParticleTable();
  if ( 0 == table ) 
  { throw GiGaException ("G4qv::Create: Invalid G4ParticleTable") ; }
  G4ParticleDefinition* check1 = table->FindParticle("qv");
  if ( 0 != check1 ) 
  { throw GiGaException ("G4qv::Create: qv already exists!") ; }
  G4ParticleDefinition* check2 = table->FindParticle( 4900101 );
  if ( 0 != check2 ) 
  { throw GiGaException ("G4qv::Create: PDGID(4900101) already exists!") ; }
  // create the particle: 
  s_instance = new G4qv ( mass , ctau ) ;
  return s_instance ;
}

G4qvbar* G4qvbar::Create 
( const double mass ,                                    // the mass 
  const double ctau )                                    // the lifetime 
{
  if ( 0 != s_instance ) { return s_instance ; }
  // check the presence of the particle in particle table:
  G4ParticleTable* table = G4ParticleTable::GetParticleTable();
  if ( 0 == table ) 
  { throw GiGaException ("G4qvbar::Create: Invalid G4ParticleTable") ; }
  G4ParticleDefinition* check1 = table->FindParticle("qvbar");
  if ( 0 != check1 ) 
  { throw GiGaException ("G4qvbar::Create: qvbar already exists!") ; }
  G4ParticleDefinition* check2 = table->FindParticle( -4900101 );
  if ( 0 != check2 ) 
  { throw GiGaException ("G4qvbar::Create: PDGID(-4900101) already exists!") ; }
  // create the particle: 
  s_instance = new G4qvbar ( mass , ctau ) ;
  return s_instance ;
}

// ============================================================================
// Get the definitions
// ============================================================================
G4pivDiag* G4pivDiag::Definition  () { return pivDiagDefinition() ; }
G4pivDiag* G4pivDiag::pivDiagDefinition () { return s_instance ; }
G4pivDiag* G4pivDiag::pivDiag            () { return pivDiagDefinition() ; }
G4pivUp*   G4pivUp::Definition  () { return pivUpDefinition() ; }
G4pivUp*   G4pivUp::pivUpDefinition () { return s_instance ; }
G4pivUp*   G4pivUp::pivUp            () { return pivUpDefinition() ; }
G4pivDn*   G4pivDn::Definition  () { return pivDnDefinition() ; }
G4pivDn*   G4pivDn::pivDnDefinition () { return s_instance ; }
G4pivDn*   G4pivDn::pivDn            () { return pivDnDefinition() ; }

G4rhovDiag* G4rhovDiag::Definition  () { return rhovDiagDefinition() ; }
G4rhovDiag* G4rhovDiag::rhovDiagDefinition () { return s_instance ; }
G4rhovDiag* G4rhovDiag::rhovDiag            () { return rhovDiagDefinition() ; }
G4rhovUp*   G4rhovUp::Definition  () { return rhovUpDefinition() ; }
G4rhovUp*   G4rhovUp::rhovUpDefinition () { return s_instance ; }
G4rhovUp*   G4rhovUp::rhovUp            () { return rhovUpDefinition() ; }
G4rhovDn*   G4rhovDn::Definition  () { return rhovDnDefinition() ; }
G4rhovDn*   G4rhovDn::rhovDnDefinition () { return s_instance ; }
G4rhovDn*   G4rhovDn::rhovDn            () { return rhovDnDefinition() ; }

G4Zv* G4Zv::Definition  () { return ZvDefinition() ; }
G4Zv* G4Zv::ZvDefinition () { return s_instance ; }
G4Zv* G4Zv::Zv            () { return ZvDefinition() ; }

G4gv* G4gv::Definition  () { return gvDefinition() ; }
G4gv* G4gv::gvDefinition () { return s_instance ; }
G4gv* G4gv::gv            () { return gvDefinition() ; }

G4qv* G4qv::Definition  () { return qvDefinition() ; }
G4qv* G4qv::qvDefinition () { return s_instance ; }
G4qv* G4qv::qv            () { return qvDefinition() ; }
G4qvbar* G4qvbar::Definition  () { return qvbarDefinition() ; }
G4qvbar* G4qvbar::qvbarDefinition () { return s_instance ; }
G4qvbar* G4qvbar::qvbar            () { return qvbarDefinition() ; }

// ============================================================================
// Virtual destructors
// ============================================================================
G4pivDiag::~G4pivDiag(){} 
G4pivUp::~G4pivUp(){} 
G4pivDn::~G4pivDn(){} 
G4rhovDiag::~G4rhovDiag(){} 
G4rhovUp::~G4rhovUp(){} 
G4rhovDn::~G4rhovDn(){} 
G4Zv::~G4Zv(){} 
G4gv::~G4gv(){} 
G4qv::~G4qv(){} 
G4qvbar::~G4qvbar(){} 

// ============================================================================
// The END 
// ============================================================================
