/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: G4AntiOmegabcZero.cpp,v 1.1 2018-10-02 20:10:51 Jingyi Xu Exp $

#include "G4AntiOmegabcZero.h"
#include "G4ParticleTable.hh"

// ######################################################################
// ###                      AntiOmegabcZero                        ###
// ######################################################################

G4AntiOmegabcZero * G4AntiOmegabcZero::theInstance = 0 ;

G4AntiOmegabcZero * G4AntiOmegabcZero::Definition()
{
  if (theInstance !=0) return theInstance;
  const G4String name = "anti-omega_bc0";
  // search in particle table
  G4ParticleTable* pTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* anInstance = pTable->FindParticle(name);
  if (anInstance ==0)
  {
  // create particle
  //
  //    Arguments for constructor are as follows
  //               name             mass          width         charge
  //             2*spin           parity  C-conjugation
  //          2*Isospin       2*Isospin3       G-parity
  //               type    lepton number  baryon number   PDG encoding
  //             stable         lifetime    decay table
  //             shortlived      subType    anti_encoding
    anInstance = 
      new G4ParticleDefinition( name , 7.19099*CLHEP::GeV , 5.1e-10*CLHEP::MeV ,  0 ,
                                3,              +1,             0,
                                0,               0,             0,
                                "baryon", 0,  -1, -5342,
                                false,     0.18e-3*CLHEP::ns,          NULL,
                                false, "omega_bc" );
  }
  theInstance = reinterpret_cast<G4AntiOmegabcZero*>(anInstance);
  return theInstance;
}

G4AntiOmegabcZero * G4AntiOmegabcZero::AntiOmegabcZeroDefinition() {
  return Definition( ) ;
}

G4AntiOmegabcZero * G4AntiOmegabcZero::AntiOmegabcZero() {
  return Definition( ) ;
}
