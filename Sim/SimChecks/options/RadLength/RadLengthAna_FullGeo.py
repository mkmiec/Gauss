###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#################################################################################
## Example to run the Radiation length scan of the detector                    ##
## In this .py a geometry containing scoring planes is loaded                  ##
## and the radiation lenght tool is activated.                                 ##
## In order for this to work you also need Gauss-Job.py and MaterialEvalGun.py ##
##                                                                             ##
##  @author : K.Zarebski, L. Pescatore, Benedetto G. Siddi                     ##
##  @date   : last modified on 2020-11-26                                      ##
#################################################################################

from Gauss.Configuration import importOptions, appendPostConfigAction
from Configurables import CondDB, LHCbApp, Gauss
import sys

rad_length_gauss = Gauss()

rad_length_gauss.Production = 'PGUN'
rad_length_gauss.DeltaRays = False

import logging
logging.basicConfig()

_logger = logging.getLogger("RADLENGTHANA")
_logger.setLevel("INFO")

def scoringGeoGauss():
    from Configurables import GaussGeo
    geo = GaussGeo()
    geo.GeoItemsNames += ["/dd/Structure/Scoring/Scoring_Plane_AfterTT"]
    geo.GeoItemsNames += ["/dd/Structure/Scoring/Scoring_Plane_AfterMagnet"]
    geo.GeoItemsNames += ["/dd/Structure/Scoring/Scoring_Plane_AfterOTPlane1"]
    geo.GeoItemsNames += ["/dd/Structure/Scoring/Scoring_Plane_AfterOTPlane2"]
    geo.GeoItemsNames += ["/dd/Structure/Scoring/Scoring_Plane_AfterOTPlane3"]
    geo.GeoItemsNames += ["/dd/Structure/Scoring/Scoring_Plane_AfterRich2"]
    geo.GeoItemsNames += ["/dd/Structure/Scoring/Scoring_Plane_AfterSpd"]
    geo.GeoItemsNames += ["/dd/Structure/Scoring/Scoring_Plane_AfterEcal"]
    geo.GeoItemsNames += ["/dd/Structure/Scoring/Scoring_Plane_AfterHcal"]
    geo.GeoItemsNames += ["/dd/Structure/Scoring/Scoring_Plane_EoD"]
                                       

def scoringGeoGiGa():
    from Configurables import GiGaInputStream
    geo = GiGaInputStream('Geo')
    geo.StreamItems += ["/dd/Structure/Scoring/Scoring_Plane_AfterTT"]
    geo.StreamItems += ["/dd/Structure/Scoring/Scoring_Plane_AfterMagnet"]
    geo.StreamItems += ["/dd/Structure/Scoring/Scoring_Plane_AfterOTPlane1"]
    geo.StreamItems += ["/dd/Structure/Scoring/Scoring_Plane_AfterOTPlane2"]
    geo.StreamItems += ["/dd/Structure/Scoring/Scoring_Plane_AfterOTPlane3"]
    geo.StreamItems += ["/dd/Structure/Scoring/Scoring_Plane_AfterRich2"]
    geo.StreamItems += ["/dd/Structure/Scoring/Scoring_Plane_AfterSpd"]
    geo.StreamItems += ["/dd/Structure/Scoring/Scoring_Plane_AfterEcal"]
    geo.StreamItems += ["/dd/Structure/Scoring/Scoring_Plane_AfterHcal"]
    geo.StreamItems += ["/dd/Structure/Scoring/Scoring_Plane_EoD"]
    
def choose_geo():
    if 'UseGaussGeo' in dir(rad_length_gauss):
        # As GaussGeo is not set to any value by default, check if set to 'False'
        # else take default 'member' state to be 'True'
        try:
           assert rad_length_gauss.UseGaussGeo == False
           _logger.info("Using 'GiGaGeo' for Geometry Input.")
           appendPostConfigAction(scoringGeoGiGa)
        except AssertionError:
           _logger.info("Using 'GaussGeo' for Geometry Input.")
           appendPostConfigAction(scoringGeoGauss) 
        except AttributeError:
           # Check if no value set and warn user which reader is being used
           _logger.warning("'UseGaussGeo' not set using 'GaussGeo' for Geometry Input.")
           appendPostConfigAction(scoringGeoGauss) 

    else:
        try:
           from Configurables import GaussGeo
           _logger.warning("'UseGaussGeo' option not found in current Gauss version, using Default reader 'GaussGeo' instead.")
           appendPostConfigAction(scoringGeoGauss)
        except ImportError:
           _logger.warning("'UseGaussGeo' option not found in current Gauss version, using Default reader 'GiGaGeo' instead.")
           appendPostConfigAction(scoringGeoGiGa)


appendPostConfigAction(choose_geo)

# --- activate RadLengthColl tool
def addMyTool():
    from Configurables import GiGa, GiGaStepActionSequence
    giga = GiGa()
    giga.StepSeq.Members.append("RadLengthColl")
appendPostConfigAction(addMyTool)


def trackNeutrinos():
    from Configurables import GiGa, GiGaRunActionSequence, TrCutsRunAction
    giga = GiGa()
    giga.addTool(GiGaRunActionSequence("RunSeq"), name="RunSeq")
    giga.RunSeq.addTool(TrCutsRunAction("TrCuts"), name="TrCuts")
    giga.RunSeq.TrCuts.DoNotTrackParticles = []

appendPostConfigAction(trackNeutrinos)
