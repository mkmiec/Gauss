/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// GiGa 
#include "GiGa/GiGaPhysConstructorBase.h"

// Geant4
#include "G4FastSimulationManagerProcess.hh"
#include "G4PhysicsConstructorFactory.hh"
#include "G4ProcessManager.hh"

/*
 *  Implementation file for the class GiGaFastPhysConstructor
 * 
 *  author: Matteo Rama - INFN Pisa
 *          Giacomo Vitali - SNS Pisa
 *          3 Mar 2020    
 */

// forward declarations 
template <class TYPE> class GiGaFactory;

class GiGaFastPhysConstructor : public GiGaPhysConstructorBase
{
  /// friend factory for instantiation 
  friend class GiGaFactory<GiGaFastPhysConstructor>;
  
 public:
   GiGaFastPhysConstructor( const std::string& type, const std::string& name, const IInterface* parent );

   void ConstructParticle() override;
   void ConstructProcess() override;

 private:
  GiGaFastPhysConstructor           ( const GiGaFastPhysConstructor& );
  GiGaFastPhysConstructor& operator=( const GiGaFastPhysConstructor& );
  
  
};

DECLARE_COMPONENT( GiGaFastPhysConstructor )

GiGaFastPhysConstructor::GiGaFastPhysConstructor 
( const std::string& type   ,
  const std::string& name   ,
  const IInterface*  parent )
: GiGaPhysConstructorBase( type , name , parent )
{}

void GiGaFastPhysConstructor::ConstructParticle() {}

void GiGaFastPhysConstructor::ConstructProcess() {
  
  std::cout << "Constructing the fast process" << std::endl;
  
  G4FastSimulationManagerProcess* fastSimProcess = new G4FastSimulationManagerProcess("G4FSMP");
  auto theParticleIterator = GetParticleIterator();
  theParticleIterator->reset();
  // Fast simulation manager process is available for all the particles
  while ((*theParticleIterator)()) {
    G4ParticleDefinition* particle = theParticleIterator->value();
    G4ProcessManager* process_manager = particle->GetProcessManager();
    process_manager->AddDiscreteProcess(fastSimProcess);
  }
}

