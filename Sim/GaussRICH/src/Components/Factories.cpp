/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files 



#include "GaussRICH/RichSensDet.h"
#include "GaussRICH/RichG4TrackActionPhotOpt.h"
#include "GaussRICH/RichG4TrackAction.h"
#include "GaussRICH/RichG4TrackActionAerogelPhoton.h"
#include "GaussRICH/RichG4RunAction.h"
#include "GaussRICH/RichG4EventAction.h"
#include "GaussRICH/Rich1G4TrackActionUpstrPhoton.h"
#include "GaussRICH/RichG4TrackActionRich2Photon.h"
#include "GaussRICH/RichG4TrackActionRich2DbgPhotonTrack.h"
#include "GaussRICH/GiGaPhysConstructorHpd.h"
#include "GaussRICH/GiGaPhysConstructorOp.h"
#include "GaussRICH/GetMCRichHitsAlg.h"
#include "GaussRICH/GetMCRichOpticalPhotonsAlg.h"
#include "GaussRICH/GetMCRichSegmentsAlg.h"
#include "GaussRICH/GetMCRichTracksAlg.h"
#include "GaussRICH/RichG4StepAnalysis3.h"
#include "GaussRICH/RichG4StepAnalysis4.h"
#include "GaussRICH/RichG4StepAnalysis5.h"
#include "GaussRICH/RichG4StepAnalysis6.h"
#include "GaussRICH/RichG4StepAnalysis15.h"



// Declaration of the Tool Factory

DECLARE_COMPONENT( RichSensDet )
DECLARE_COMPONENT( RichG4TrackActionPhotOpt )

DECLARE_COMPONENT( RichG4TrackAction )

DECLARE_COMPONENT( RichG4TrackActionAerogelPhoton )
DECLARE_COMPONENT( RichG4RunAction )
DECLARE_COMPONENT( RichG4EventAction )
DECLARE_COMPONENT( Rich1G4TrackActionUpstrPhoton )
DECLARE_COMPONENT( RichG4TrackActionRich2Photon )
DECLARE_COMPONENT( RichG4TrackActionRich2DbgPhotonTrack )
DECLARE_COMPONENT( GiGaPhysConstructorHpd )
DECLARE_COMPONENT( GiGaPhysConstructorOp )
DECLARE_COMPONENT( GetMCRichHitsAlg )
DECLARE_COMPONENT( GetMCRichOpticalPhotonsAlg )
DECLARE_COMPONENT( GetMCRichSegmentsAlg )
DECLARE_COMPONENT( GetMCRichTracksAlg )

DECLARE_COMPONENT( RichG4StepAnalysis3 )
DECLARE_COMPONENT( RichG4StepAnalysis4 )
DECLARE_COMPONENT( RichG4StepAnalysis5 )
DECLARE_COMPONENT( RichG4StepAnalysis6 )
DECLARE_COMPONENT( RichG4StepAnalysis15 )


