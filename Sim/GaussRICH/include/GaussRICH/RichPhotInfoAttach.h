/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: RichPhotInfoAttach.h,v 1.1 2008-01-21 16:59:01 seaso Exp $
#ifndef GAUSSRICH_RICHPHOTINFOATTACH_H 
#define GAUSSRICH_RICHPHOTINFOATTACH_H 1

// Include files

/** @class RichPhotInfoAttach RichPhotInfoAttach.h Misc/RichPhotInfoAttach.h
 *  
 *
 *  @author Sajan EASO
 *  @date   2007-10-29
 */
#include "RichInfo.h"
#include "G4Track.hh"

extern void setRichHpdQwPcReflInfo(const G4Track& aPhotonTk,G4int aFlag);
extern G4int getRichHpdQwPcReflInfo(const G4Track& aPhotonTk);
extern G4int getRichHpdQwPcReflInfoSave(const G4Track& aPhotonTk);


#endif // GAUSSRICH_RICHPHOTINFOATTACH_H
