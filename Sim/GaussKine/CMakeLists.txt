###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Sim/GaussKine
-------------
#]=======================================================================]

gaudi_add_module(GaussKine
    SOURCES
        src/GenerationToSimulation.cpp
        src/MaskParticles.cpp
        src/SimulationToMCTruth.cpp
        src/MakeG4UnknownIon.cpp
    LINK
        LHCb::GenEvent
        LHCb::MCEvent
        Gauss::GaussToolsLib
        Gauss::LoKiGenLib
)
