/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENERATIONTOSIMULATION_H
#define GENERATIONTOSIMULATION_H 1

// Gaudi.
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/Vector4DTypes.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/ToolHandle.h"

//CLHEP
#include "CLHEP/Units/PhysicalConstants.h"

// Event.
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"

#include "Event/HepMCEvent.h"
#include "Event/MCHeader.h"


// HepMC.
#include "HepMC/SimpleVector.h"
#include "GenEvent/HepMCUtils.h"

// LoKi.
# include "LoKi/IGenHybridFactory.h"


// Forward declarations.
class IGiGaSvc;
class G4PrimaryVertex ;
class G4PrimaryParticle ;
class IFlagSignalChain ;
class IMakeG4UnknownTool;
namespace HepMC {
  class GenEvent ;
  class GenParticle ;
  class GenVertex ;
}

/** @class GenerationToSimulation GenerationToSimulation.h
 *  Algorithm to transmit particles from the generation phase
 *  to the simulation phase.
 *
 *  @author Gloria CORTI, Patrick ROBBE
 *  @date   2008-09-24
 */
class GenerationToSimulation : public GaudiAlgorithm {
public:
  /// Standard constructor.
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override;    ///< Algorithm initialization.
  StatusCode execute   () override;    ///< Algorithm execution.

private:
  /** Accessor to GiGa service.
   *  @return pointer to GiGa Service.
   */
  IGiGaSvc* gigaSvc() const { return m_gigaSvc; }

  // The tool to be used to add unknown particles
  ToolHandle<IMakeG4UnknownTool> m_makeIonTool{this, "MakeG4UnknownIon", "MakeG4UnknownIon"};

  /// Determine the primary vertex of the interaction.
  Gaudi::LorentzVector primaryVertex(const HepMC::GenEvent* genEvent) const;

  /// Decide if a particle has to be kept or not.
  bool keep(const HepMC::GenParticle* particle) const;

  /// Convert a GenParticle either into a MCParticle or G4PrimaryParticle.
  void convert( HepMC::GenParticle*& particle,
                G4PrimaryVertex* pvertexg4,
                LHCb::MCVertex* originVertex, G4PrimaryParticle* motherg4,
                LHCb::MCParticle* mothermcp);

  /// Decide if the particle should be transfered to Geant4 or only MCParticle.
  unsigned char transferToGeant4( const HepMC::GenParticle* p) const;

  /// Create a G4PrimaryParticle from a HepMC GenParticle.
  G4PrimaryParticle* makeG4Particle(HepMC::GenParticle*& particle,
				    LHCb::MCParticle* mcp) const;

  /// Create an MCParticle from a HepMC GenParticle.
  LHCb::MCParticle* makeMCParticle(HepMC::GenParticle*& particle,
				   LHCb::MCVertex*& endVertex) const;

   /// Compute the lifetime of a particle.
  double lifetime(const HepMC::FourVector mom, const HepMC::GenVertex* P,
		  const HepMC::GenVertex* E) const;

  /// Check if a particle has oscillated.
  const HepMC::GenParticle* hasOscillated(const HepMC::GenParticle* P) const;

  /// Remove a primary particle from a primary vertex.
  void removeFromPrimaryVertex(G4PrimaryVertex*& pvertexg4, const
			       G4PrimaryParticle* particleToDelete) const;

  Gaudi::Property<std::string> m_gigaSvcName{this,"GiGaService","GiGa","Name of the GiGa service."};         ///< Name of the GiGa service.
  IGiGaSvc*   m_gigaSvc{nullptr};             ///< Pointer to the GiGa service.
  ///< Max value of distance in detector for Geant4.
  Gaudi::Property<double>      m_travelLimit{this,"TravelLimit",
    1e-10 * CLHEP::m,"Pass particles to Geant4 with travel length above this."};
  ///< Check if the particles are known to G4.
  Gaudi::Property<bool>        m_lookForUnknownParticles{this,"LookForUnknownParticles",
    false,"Check if the particles are known to G4."};
  ///< Do not transmit anything to Geant4 (useful for fast MC).
  Gaudi::Property<bool>        m_skipGeant4{this,"SkipGeant",
    false,"Skip passing everything to Geant4."};
  ///< Update G4 properties for masses.
  Gaudi::Property<bool>        m_updateG4ParticleProperties{this,"UpdateG4ParticleProperties",
    true,"Update the Geant4 particle properties."};
  
  Gaudi::Property<std::string> m_generationLocation{this,"HepMCEventLocation",
    LHCb::HepMCEventLocation::Default,"Location to read the HepMC event."}; ///< Location in TES of input HepMC events.
  Gaudi::Property<std::string> m_particlesLocation{this,"Particles",
    LHCb::MCParticleLocation::Default,"Location to place the MCParticles."};  ///< Location in TES of output MCParticles.
  Gaudi::Property<std::string> m_verticesLocation{this,"Vertices",
    LHCb::MCVertexLocation::Default,"Location to place the MCVertices."};   ///< Location in TES of output MCVertices.
  Gaudi::Property<std::string> m_mcHeader{this,"MCHeader",
    LHCb::MCHeaderLocation::Default,"Location to retrieve the MCHeader."};           ///< Location in TES of MCHeader.

  LHCb::MCParticles* m_particleContainer{nullptr}; ///< Container of MCParticles.
  LHCb::MCVertices*  m_vertexContainer{nullptr};   ///< Container of MCVertices.

  ///< Map to know if a particle was already converted to G4.
  std::map<int, std::pair<bool, G4PrimaryParticle*> > m_g4ParticleMap;
  ///< Map to know if a particle was already converted to MCParticle.
  std::map<int, bool> m_mcParticleMap;
  ///< Vector of particles to remove from G4PrimaryVertex.
  std::vector<G4PrimaryParticle*> m_particlesToDelete;

  Gaudi::Property<std::string>          m_keepCode{this,"KeepCode",
    "","The code to flag additional particles for storage."}; ///< Code to keep additional particles.
  LoKi::GenTypes::GCut m_keepCuts{LoKi::Constant<const HepMC::GenParticle*, 
           bool>(true)}; ///< Cuts to keep additional particles.

  /// Reference to tool to propagate fromSignal flag
  IFlagSignalChain* m_setSignalFlagTool{nullptr};

};
#endif // GENERATIONTOSIMULATION_H
