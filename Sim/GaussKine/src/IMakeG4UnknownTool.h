/***********************************************************************************\
* (c) Copyright 1998-2019 CERN for the benefit of the LHCb and ATLAS collaborations *
*                                                                                   *
* This software is distributed under the terms of the Apache version 2 licence,     *
* copied verbatim in the file "LICENSE".                                            *
*                                                                                   *
* In applying this licence, CERN does not waive the privileges and immunities       *
* granted to it by virtue of its status as an Intergovernmental Organization        *
* or submit itself to any jurisdiction.                                             *
\***********************************************************************************/
#ifndef GAUSSKINE_IMAKEG4UNKNOWNTOOL_H
#define GAUSSKINE_IMAKEG4UNKNOWNTOOL_H 1

// Include files
#include "GaudiKernel/IAlgTool.h"

/** @class IMakeG4UnknownTool IMakeG4UnknownTool.h
 *  Abstract interface for dynamic allocation of a new G4Ion
 *
 *  @author M. Veltri
 */
class IMakeG4UnknownTool : virtual public IAlgTool {

public:
  // InterfaceID
  DeclareInterfaceID( IMakeG4UnknownTool, 1, 0 );

  // To add a new ion to the G4 table 
  virtual void AddIonToG4Table(int) const = 0;

};

#endif // GAUSSKINE_IMAKEG4UNKNOWNTOOL_H
