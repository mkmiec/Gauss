/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <cstdlib>
#include <array>
#include <cmath>
#include <map>
#include <string>

#include "GaussFastCaloPointlib/FastsimPointlibModel.h"

// Gaudi
#include "GaudiKernel/IToolSvc.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/Point3DTypes.h"

// Geant4
#include "G4FieldTrackUpdator.hh"
#include "G4PathFinder.hh"
#include "G4PrimaryParticle.hh"
#include "G4SystemOfUnits.hh"
#include "G4UnitsTable.hh"
#include "G4Step.hh"
#include "G4FastStep.hh"
#include "G4FastTrack.hh"
#include "G4ParticleDefinition.hh"
#include "G4Gamma.hh"
#include "G4MuonMinus.hh"
#include "G4MuonPlus.hh"
#include "G4Electron.hh"
#include "G4Positron.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4HCofThisEvent.hh"
#include "G4Types.hh"
#include "G4ios.hh"
#include "Randomize.hh"
#include "G4TwoVector.hh"


// from GiGa 
#include "GiGa/IGiGaSvc.h"
#include "GiGa/GiGaHitsByName.h"

// ROOT
#include "TFile.h"

// local
#include "GaussCalo/CaloHit.h"
#include "GaussCalo/CaloSubHit.h"

// GaussTools
#include "GaussTools/GaussTrackInformation.h"


/*
 *  Implementation file for the class FastsimPointlibModel
 * 
 *  author: Matteo Rama - INFN Pisa
 *          Giacomo Vitali - SNS Pisa                                                                                                                              
 *          3 Mar 2020 
 */

FastsimPointlibModel::FastsimPointlibModel(const std::string& aModelName,
					   G4Region* aEnvelope, const std::string& library)
  : G4VFastSimulationModel(aModelName, aEnvelope),
    m_gigaSvc( nullptr ) {
  
  //Load the library
  TFile* infile = TFile::Open(library.c_str());
  if ( !infile ) {
    G4cout << "FastsimPointlibModel: Cannot open file with library: " << library << G4endl;
    std::exit( 1 );
  }

  std::vector< std::vector < std::vector<float> > >* myvec1a;
  std::vector< std::vector < std::vector<float> > >* myvec1b;
  std::vector< std::vector < std::vector<float> > >* myvec1c;
  std::vector< std::vector < std::vector<float> > >* myvec1d;
  std::vector< std::vector < std::vector<int> > >* myvec2;
  std::vector<float>* myvec3;
  std::vector < std::vector<float> >* myvec4;    
  infile->GetObject("x_lib",myvec1a);
  infile->GetObject("y_lib",myvec1b);
  infile->GetObject("e_lib",myvec1c);
  infile->GetObject("t_lib",myvec1d);
  infile->GetObject("calo_lib",myvec2);

  if ( myvec1a->size() != myvec1b->size() || myvec1a->size() != myvec1c->size() || myvec1a->size() != myvec1d->size() ||
       myvec1a->size() != myvec2->size() ) {
    G4cout << "FastsimPointlibModel: Issue with size of library components" << G4endl;
    std::exit( 1 );
  }

  // elements of these 5 vectors are always accessed together, so reshuffle
  // them to vector of struct
  m_library.resize( myvec1a->size() );
  for ( size_t i1 = 0; i1 < myvec1a->size(); ++i1 ) {
    m_library[i1].resize( (*myvec1a)[i1] .size() );
    for ( size_t i2 = 0; i2 < (*myvec1a)[i1].size(); ++i2 ) {
      m_library[i1][i2].reserve( (*myvec1a)[i1][i2].size() );
      for ( size_t i3 = 0; i3 < (*myvec1a)[i1][i2].size(); ++i3 ) {
        m_library[i1][i2].push_back( libraryPoint{ ( *myvec1a )[i1][i2][i3], ( *myvec1b )[i1][i2][i3],
                                                   ( *myvec1c )[i1][i2][i3], ( *myvec1d )[i1][i2][i3],
                                                   ( *myvec2 )[i1][i2][i3] } );
      }
    }
  }

  infile->GetObject("p_e_lib",myvec3);
  m_p_e_lib.reserve(myvec3->size());
  m_p_e_lib=*myvec3;
  infile->GetObject("p_theta_lib",myvec3);
  m_p_theta_lib.reserve(myvec3->size());
  m_p_theta_lib=*myvec3;
  infile->GetObject("p_phi_lib",myvec3);
  m_p_phi_lib.reserve(myvec3->size());
  m_p_phi_lib=*myvec3;
  infile->GetObject("xc_yc_vec",myvec3);
  m_xc_yc_vec.reserve(myvec3->size());
  m_xc_yc_vec=*myvec3;
  infile->GetObject("x_ave",myvec4);
  m_x_ave_vec.reserve(myvec4->size());
  m_x_ave_vec=*myvec4;
  infile->GetObject("y_ave",myvec4);
  m_y_ave_vec.reserve(myvec4->size());
  m_y_ave_vec=*myvec4;
  infile->GetObject("sx_ave",myvec4);
  m_sx_ave_vec.reserve(myvec4->size());
  m_sx_ave_vec=*myvec4;
  infile->GetObject("sy_ave",myvec4);
  m_sy_ave_vec.reserve(myvec4->size());
  m_sy_ave_vec=*myvec4;
  infile->GetObject("efrac_ave",myvec4);
  m_efrac_ave_vec.reserve(myvec4->size());
  m_efrac_ave_vec=*myvec4;

  // Metadata
  std::map<std::string, int> *intMetadata;
  std::map<std::string, double> *doubleMetadata;

  infile->GetObject("intMetadata", intMetadata);
  infile->GetObject("doubleMetadata", doubleMetadata);

  m_nbinsE = (*intMetadata)["energyBins"];
  m_nbinsTheta = (*intMetadata)["thetaBins"];
  m_nbinsPhi = (*intMetadata)["phiBins"];

  m_lib_E_min = (*doubleMetadata)["minE"];
  m_lib_E_max = (*doubleMetadata)["maxE"];

  m_theta_min = (*doubleMetadata)["minTheta"];
  m_theta_max = (*doubleMetadata)["maxTheta"];

  infile->Close();
  delete infile;

  G4cout << "FastsimPointlibModel: Number of bins in library for E, theta, phi: " << m_nbinsE << " " << m_nbinsTheta
         << " " << m_nbinsPhi << G4endl;
  if ( m_nbinsPhi != 1 ){
    G4cout << "FastsimPointlibModel: ERROR, current model assumes 1 bin in phi, but library has " << m_nbinsPhi
          << " bins" << G4endl;
    std::exit( 1 );
  }

  // Some library metadata
  m_theta_step    = ( m_theta_max - m_theta_min ) / m_nbinsTheta;
  m_cols_per_node = m_library[0].size();

  // Here we will set up range of where model should be applied, but user can
  // overwrite to smaller range
  m_p_min     = m_lib_E_min;
  m_p_max     = m_lib_E_max;

  TFile* fractionfile=TFile::Open("$PARAMFILESROOT/data/gausscalo.root","READ");
  m_spdtime_his[0]=*(TH1F*)fractionfile->Get("SPDTIME/h1");
  m_spdtime_his[1]=*(TH1F*)fractionfile->Get("SPDTIME/h2");
  m_spdtime_his[2]=*(TH1F*)fractionfile->Get("SPDTIME/h3");
  m_ecaltime_his[0]=*(TH1F*)fractionfile->Get("ECALTIME/h1");
  m_ecaltime_his[1]=*(TH1F*)fractionfile->Get("ECALTIME/h2");
  for ( auto& hist : m_spdtime_his ) { hist.SetDirectory( 0 ); }
  for ( auto& hist : m_ecaltime_his ) { hist.SetDirectory( 0 ); }
  fractionfile->Close();
  delete fractionfile;
} 

/*
FastsimPointlibModel::FastsimPointlibModel(const std::string& aModelName)
  : G4VFastSimulationModel(aModelName) {}
*/

void FastsimPointlibModel::initialize() {
}

G4bool FastsimPointlibModel::IsApplicable(const G4ParticleDefinition& aParticleType) {
  //
  // In this example the model is only called if the particle is a photon
  //
  if (aParticleType == *G4Gamma::GammaDefinition()) return true;
  else return false;
}

G4bool FastsimPointlibModel::ModelTrigger( const G4FastTrack& aFastTrack ) {
  //
  // In this example the model is only called if the particle has p_min<p<p_max and theta<theta_max
  //
  const G4Track* triggertrack = aFastTrack.GetPrimaryTrack();
  const G4ThreeVector  track_p      = triggertrack->GetMomentum();
  const double         trig_p       = track_p.mag();
  const double         trig_theta   = track_p.theta();
  if ( trig_theta >= m_theta_min && trig_theta <= m_theta_max && trig_p >= m_p_min && trig_p <= m_p_max ) return true;
  return false;
}

void FastsimPointlibModel::setPRange( const float min, const float max ){
  if ( min > m_p_min ) { m_p_min = min; }
  if ( max < m_p_max ) { m_p_max = max; }
}

void FastsimPointlibModel::setThetaRange( const float min, const float max ) {
  if ( min > m_theta_min ) { m_theta_min = min; }
  if ( max < m_theta_max ) { m_theta_max = max; }
}

void FastsimPointlibModel::setCaloDB( DeCalorimeter* Spd, DeCalorimeter* Prs, DeCalorimeter* Ecal,
                                      DeCalorimeter* Hcal ) {
  m_SpdDB = Spd;
  m_PrsDB = Prs;
  m_EcalDB = Ecal;
  m_HcalDB = Hcal;

  auto cell = m_SpdDB->cellIdByIndex( 1 );
  auto cellCenter = m_SpdDB->cellCenter(cell);
  m_caloZPositions[SPD] = cellCenter.z()-cellCenter.y()*m_sinTilt;

  cell = m_PrsDB->cellIdByIndex( 1 );
  cellCenter = m_PrsDB->cellCenter(cell);
  m_caloZPositions[PRS] = cellCenter.z()-cellCenter.y()*m_sinTilt;

  cell = m_EcalDB->cellIdByIndex( 1 );
  cellCenter = m_EcalDB->cellCenter(cell);
  m_caloZPositions[ECAL] = cellCenter.z();
  m_caloZPositions[ECAL] = cellCenter.z()-cellCenter.y()*m_sinTilt;

  cell = m_HcalDB->cellIdByIndex( 1 );
  cellCenter = m_HcalDB->cellCenter(cell);
  m_caloZPositions[HCAL] = cellCenter.z()-cellCenter.y()*m_sinTilt;
}

void FastsimPointlibModel::DoIt(const G4FastTrack& aFastTrack, G4FastStep& aFastStep) {
  //
  // Here the CaloSubHits are created from the library, and then stored
  //
  G4RunManager* fRM = G4RunManager::GetRunManager();
  const G4Event* currentEvent = fRM->GetCurrentEvent();
  G4HCofThisEvent* HCofEvent = currentEvent->GetHCofThisEvent();
  
  G4SDManager* fSDM = G4SDManager::GetSDMpointer();
  
  G4int SpdcollectionID = fSDM->GetCollectionID("SpdHits_Fast");
  CaloSubHitsCollection* SpdCollection = (CaloSubHitsCollection*)(HCofEvent->GetHC(SpdcollectionID));
  
  G4int PrscollectionID = fSDM->GetCollectionID("PrsHits_Fast");
  CaloSubHitsCollection* PrsCollection = (CaloSubHitsCollection*)(HCofEvent->GetHC(PrscollectionID));
  
  G4int EcalcollectionID = fSDM->GetCollectionID("EcalHits_Fast");
  CaloSubHitsCollection* EcalCollection = (CaloSubHitsCollection*)(HCofEvent->GetHC(EcalcollectionID));
  
  G4int HcalcollectionID = fSDM->GetCollectionID("HcalHits_Fast");
  CaloSubHitsCollection* HcalCollection = (CaloSubHitsCollection*)(HCofEvent->GetHC(HcalcollectionID));
  
  const G4Track* track = aFastTrack.GetPrimaryTrack();
  
  //Get the properties of the incident particle
  const G4ThreeVector track_p=track->GetMomentum();

  const G4ThreeVector track_pos=track->GetPosition();

  const double part_t = track->GetGlobalTime();
  //double part_tloc=track->GetLocalTime();//roughly 1.5 ns smaller than the global time

  int col_index = int( G4UniformRand() * m_cols_per_node ); // choose the collection randomly
  std::vector<libraryPoint> newCollection;
  create_hit_collection( col_index, track_p, track_pos, part_t, &newCollection );

  //
  // Create the CaloSubHits
  //
  m_hitmap.clear();

  // check the status of the track
  GaussTrackInformation* info = gaussTrackInformation ( track->GetUserInformation() );
  if( info==nullptr )
    { G4cout<<"FastsimPointlibModel: Invalid Track information"<<G4endl ; return ; }     // RETURN
  // ID of the track to be stored
  const int sTrackID = track->GetParentID();

  for ( auto& hitinfo : newCollection ) {
    
    LHCb::Detector::Calo::CellID cellID;
    CaloSubHit::Time slot;
    std::vector<double> timefractions;

    double nonuniformity_corr{1};
    const bool isgoodpoint = getInfo( cellID, slot, timefractions, nonuniformity_corr, hitinfo );
    if ( !isgoodpoint ) { // skip hit if not good (eg if it's not in acceptance)
      continue;
    }
    hitinfo.e *= nonuniformity_corr; // apply nonuniformity correction

    CaloSubHit*& sub = m_hitmap[cellID];//NB: CaloSubHit* sub = m_hitmap[cellID] ritorna sempre 0, CHIARIRE
    if (!sub){
      sub = new CaloSubHit( cellID, sTrackID );
      m_hitmap.insert( SubHitMap::value_type( cellID, sub ) ); // insert the new CaloSubHit into the hitmap
    }
    for ( std::size_t j = 0; j < timefractions.size(); j++ ) {
      float frac = timefractions[j];
      if ( frac > 1e-5 ) { sub->add( slot, hitinfo.e * frac ).ignore(); }
      slot++;
    }
  }//end loop over subhits

  // Store the generated hits into the corresponding hit collection
  for ( auto it = m_hitmap.begin(); it != m_hitmap.end(); ++it ) {
    CaloSubHit* sub=it->second;
    const int calo=it->first.calo();
    if ( calo == ECAL ) {
      EcalCollection->insert( sub );
    } else if ( calo == PRS ) {
      PrsCollection->insert( sub );
    } else if ( calo == SPD ) {
      SpdCollection->insert( sub );
    } else if ( calo == HCAL ) {
      HcalCollection->insert( sub );
    }
  }

  // Kill the G4track once the hits have been generated
  aFastStep.ClearDebugFlag();
  aFastStep.KillPrimaryTrack();
  
}//end DoIt

void FastsimPointlibModel::compute_E_theta_phi(const G4ThreeVector& momentum, double* E, double* theta, double* phi) const {
  //NB: after this transfromation px,py,pz are the coordinates in the reference frame of the calorimeter, ie the y-z plane is parallel to the Spd front face,
  const double ppz = momentum.getZ();
  const double ppy = momentum.getY();
  const double pz  = ppz * m_cosTilt - ppy * m_sinTilt;
  const double py  = ppz * m_sinTilt + ppy * m_cosTilt;
  *E         = std::sqrt( momentum.getX() * momentum.getX() + py * py + pz * pz );
  *theta     = std::acos( pz / ( *E ) );

  *phi = std::atan2( py, momentum.getX() );

  return;

}//end compute_E_theta_phi

void FastsimPointlibModel::create_hit_collection( const int col_index, const G4ThreeVector& momentum,
                                                  const G4ThreeVector& position, const double part_t,
                                                  std::vector<libraryPoint>* newCollection ) const {

  double part_E, part_theta, part_phi;
  compute_E_theta_phi( momentum, &part_E, &part_theta, &part_phi );

  //
  // Compute the E,theta library nodes corresponding to the (E,theta) of the particle entering the calorimeter
  //
  size_t bin1_theta,bin2_theta;
  size_t bin1_E,bin2_E;
  get_bin_nodes( part_E, part_theta, &bin1_E, &bin2_E, &bin1_theta, &bin2_theta );

  //
  // Compute the parameters dx,dy,alphax,alphay which are used to modify the point hits when (part_E,part_theta) does not coincide with a node of the library
  //
  std::array<double, 4> dx_arr, dy_arr, alphax_arr, alphay_arr, scaleE_arr;
  const size_t index = bin1_E + m_nbinsE * bin1_theta;    // il nodo cui appartiene part_E,part_theta
  const size_t index_E = bin2_E + m_nbinsE * bin1_theta;  // il nodo per ottenere la 'df/dE' (f=<x>,<y>,<sx>,<sy>,<efrac>)
  const size_t index_th = bin1_E + m_nbinsE * bin2_theta; // il nodo per ottenere la 'df/dtheta'
  for ( size_t calo = 0; calo < 4; calo++ ) {
    dx_arr[calo]     = get_interpol_par( part_E, part_theta, calo, index, index_E, index_th, m_x_ave_vec, true );
    dy_arr[calo]     = get_interpol_par( part_E, part_theta, calo, index, index_E, index_th, m_y_ave_vec, true );
    alphax_arr[calo] = get_interpol_par( part_E, part_theta, calo, index, index_E, index_th, m_sx_ave_vec, false );
    alphay_arr[calo] = get_interpol_par( part_E, part_theta, calo, index, index_E, index_th, m_sy_ave_vec, false );
    scaleE_arr[calo] = get_interpol_par( part_E, part_theta, calo, index, index_E, index_th, m_efrac_ave_vec, false );
  }

  //
  // Compute the means mux,muy, for each calo, of the point hit collection selected for the current particle
  // NB: these means do differ from those saved in m_x_ave_vec[index][calo] and m_y_ave_vec[index][calo]. The latter are means averaged over all the point hit collections with given (index,calo), while mux,muy are the means of the single collection selected for the current particle. 
  //
  std::array<double, 4> mux_arr{ 0, 0, 0, 0 }, muy_arr{ 0, 0, 0, 0 }, etot_arr{ 0, 0, 0, 0 };
  for (const auto& ii : m_library[index][col_index]){
    mux_arr[ii.calo] += ii.e * ii.x;
    muy_arr[ii.calo] += ii.e * ii.y;
    etot_arr[ii.calo] += ii.e;
  }
  for ( int i = 0; i < 4; i++ ) {
    if ( etot_arr[i] ) {
      mux_arr[i] /= etot_arr[i];
      muy_arr[i] /= etot_arr[i];
    }
  }

  //
  // Theory: x_i -> R(A*x_i+beta-x0)+x1 = RA*x_i + R(beta-x0)+x1
  // where R, A are matrices, x_i, beta, x0, x1 are 2d vectors
  // x0: entry point to build the library
  // x1: entry point of current particle
  //
  // 1) x->A*x-A*mu+mu [expansion, A diag matrix]
  // 2) x->A*x-A*mu+mu+dmu [dmu = correction of means, eg between two nodes]
  // 3) x->R(A*x-A*mu+mu+dmu-x0)+x0 [rotation around x0] 
  // 4) x->R(A*x-A*mu+mu+dmu-x0)+x0 + (x1-x0) [translation to entry point of current particle]
  // 5) 4) is RA*x + R(mu-A*mu+dmu-x0)+x1
  //

  const G4TwoVector x0_vec( m_xc_yc_vec[0], m_xc_yc_vec[1] );
  const G4TwoVector x1_vec( position.x(), position.y() );
  const double ang = part_phi - m_p_phi_lib[0];
  std::array<G4TwoVector, 4> beta_vec_arr;
  // Compute R(beta-x0)+x1, which is independent on the points x_i
  for ( int i = 0; i < 4; i++ ) {
    if ( etot_arr[i] ) {
      beta_vec_arr[i].set( dx_arr[i] - mux_arr[i] * ( alphax_arr[i] - 1 ),
                           dy_arr[i] - muy_arr[i] * ( alphay_arr[i] - 1 ) );
      beta_vec_arr[i] -= x0_vec;
      beta_vec_arr[i].rotate( ang );
      beta_vec_arr[i] += x1_vec;
    }
  }
  // Compute RA*x_i + R(beta-x0)+x1 and fill the new point library (A=diag matrix)
  newCollection->reserve( m_library[index][col_index].size() );
  for ( const auto& ii : m_library[index][col_index] ) {
    G4TwoVector x_vec( alphax_arr[ii.calo] * ii.x, alphay_arr[ii.calo] * ii.y );
    x_vec.rotate( ang );
    x_vec += beta_vec_arr[ii.calo]; // NB: In previous lines of code beta_vec_arr[calo] gets initialised if there is at
                                    // least one hit with 'calo', hence it's OK
    newCollection->push_back( libraryPoint{
        x_vec.x(), x_vec.y(), ii.e * part_E / m_p_e_lib[index] * scaleE_arr[ii.calo], ii.t + part_t, ii.calo } );
  }

  return;

} // end create_hit_collection

void FastsimPointlibModel::get_bin_nodes( const double part_E, const double part_theta, size_t* bin1_E, size_t* bin2_E,
                                          size_t* bin1_theta, size_t* bin2_theta ) const {
  if ( part_theta < 0 ) {
    G4cout << "FastsimPointlibModel: get_bin_nodes: Error: part_theta = " << part_theta << " return;" << G4endl;
    return;
  }
  const double x_theta     = part_theta / m_theta_step;
  *bin1_theta = floor(x_theta+0.5);
  *bin2_theta = *bin1_theta < x_theta ? *bin1_theta+1 : *bin1_theta-1;
  if ( *bin1_theta >= ( m_nbinsTheta - 1 ) ) {
    *bin1_theta = m_nbinsTheta - 1;
    *bin2_theta = m_nbinsTheta - 2;
  }

  if (part_E<0){ G4cout<<"FastsimPointlibModel: create_hit_collection: Error: part_E = "<<part_E<<" return;"<<G4endl; return; }
  const double x_E = log( part_E / m_lib_E_min ) / log( m_lib_E_max / m_lib_E_min ) * ( m_nbinsE - 1 );
  *bin1_E = floor( x_E + 0.5 );
  *bin2_E = *bin1_E < x_E ? *bin1_E + 1 : *bin1_E - 1;
  if ( *bin1_E >= ( m_nbinsE - 1 ) ) {
    *bin1_E = m_nbinsE - 1;
    *bin1_E = m_nbinsE - 2; 
  }
  if ( x_E < 0 ) {
    *bin1_E = 0;
    *bin2_E = 1; // bin1_E + 1
  }

  return;
} // end get_bin_nodes

double FastsimPointlibModel::get_interpol_par( const double part_E, const double part_theta, const size_t calo,
                                               const size_t index, const size_t index_E, const size_t index_th,
                                               const std::vector<std::vector<float>>& interpolationArray,
                                               const bool                             shift ) const {

  double df_dE = ( ( interpolationArray[index] )[calo] - ( interpolationArray[index_E] )[calo] ) /
                 ( m_p_e_lib[index] - m_p_e_lib[index_E] );
  double df_dth = ( ( interpolationArray[index] )[calo] - ( interpolationArray[index_th] )[calo] ) /
                  ( m_p_theta_lib[index] - m_p_theta_lib[index_th] );
  double val0 = ( interpolationArray[index] )[calo];

  double dE  = df_dE * ( part_E - m_p_e_lib[index] );
  double dth = df_dth * ( part_theta - m_p_theta_lib[index] );
  double val = val0 + dE + dth;

  if ( shift ) { return val - val0; }
  return ( val0 ? val / val0 : 1 );

} // end get_interpol_par

/*
 * Function which collects relevant information to create hit.
 * It effectively emulates what is done in CaloSensDet class in functions
 * fillHitInfo() and timing().
 */
bool FastsimPointlibModel::getInfo( LHCb::Detector::Calo::CellID& cellID, CaloSubHit::Time& slot, std::vector<double>& fractions,
                                    double& nonuniform_corr, const libraryPoint& point ) const {
  if (point.x==0 && point.y==0 && point.t==0 && point.calo==SPD) return false;

  // Given the depth of SPD/PRS we need to take into account tilt
  const double zpos = m_caloZPositions[point.calo] + point.y * m_sinTilt / m_cosTilt;
  CellParam* par{nullptr};
  const Gaudi::XYZPoint pp{ point.x, point.y, zpos };
  if ( point.calo == ECAL ) {
    par = const_cast<CellParam*>( m_EcalDB->Cell_( pp ) );
  } else if ( point.calo == PRS ) {
    par = const_cast<CellParam*>( m_PrsDB->Cell_( pp ) );
  } else if ( point.calo == SPD ) {
    par = const_cast<CellParam*>( m_SpdDB->Cell_( pp ) );
  } else if ( point.calo == HCAL ) {
    par = const_cast<CellParam*>( m_HcalDB->Cell_( pp ) );
  }
  if ( !par ) {
    return false;
  }

  cellID = par->cellID();
  if ( cellID.row()==0 && cellID.col()==0 ) {
    return false;
  }

  // Compute the localNonUniformity correction
  nonuniform_corr = 1;
  if ( point.calo == ECAL ) nonuniform_corr = localNonUniformity( cellID, point ); // NB: it only applies to Ecal

  // Compute the time slot
  double cellTime{0};
  if ( point.calo == ECAL ) {
    cellTime = m_EcalDB->cellTime( cellID );
  } else if ( point.calo == PRS ) {
    cellTime = m_PrsDB->cellTime( cellID );
  } else if ( point.calo == SPD ) {
    cellTime = m_SpdDB->cellTime( cellID );
  } else if ( point.calo == HCAL ) {
    cellTime = m_HcalDB->cellTime( cellID );
  }
  const double t0     = cellTime - m_dT0; //_dT0=0.5 ns
  const double deltaT = point.t - t0;     // Same as deltaT defined in EHCalSensDet::timing(...)
  slot          = (CaloSubHit::Time)floor( deltaT / m_slotWidth );
  const double dt     = deltaT - slot * m_slotWidth;

  // Compute the energy fractions vs time slot
  // Ecal or Hcal
  if ( point.calo == ECAL || point.calo == HCAL ) {
    for ( int i = 0; i < 2; i++ ) {
      const int bin = m_ecaltime_his[i].GetXaxis()->FindBin( dt ); // NB: Ecal and Hcal histos are identical, hence it's
                                                                   // fine to use Ecal also for Hcal
      const double frac = m_ecaltime_his[i].GetBinContent( bin );
      fractions.push_back( frac );
    }
  }
  // Spd or Prs
  else if ( point.calo == PRS || point.calo == SPD ) {
    const unsigned int area = cellID.area();
    double t = - m_slotWidth + m_sDelays[area] - dt;
    for ( int i = 0; i < m_numBXs; i++, t += m_slotWidth ) {
      if ( m_spdprs_lowerEdge < t && t < m_spdprs_upperEdge ) {
        const int    bin  = m_spdtime_his[area].GetXaxis()->FindBin( t );
        const double frac = m_spdtime_his[area].GetBinContent( bin );
        fractions.push_back( frac );
      } else
        fractions.push_back( 0 );
    }
    slot -= 1;
  }

  return true;
}

/*
 * ECAL non-uniformity correction based on
 * EcalSensDet::localNonUniformity()
 */
double FastsimPointlibModel::localNonUniformity( const LHCb::Detector::Calo::CellID& cellID, const libraryPoint& point ) const {

  if ( point.calo != ECAL ) return 1; // NB: nonuniformity correction only applies to Ecal

  const double x0 = m_EcalDB->cellX( cellID );
  const double y0 = m_EcalDB->cellY( cellID );
  
  const double cellSize = m_EcalDB->cellSize( cellID );
  double A_local{m_a_local_inner_ecal};
  double A_global{m_a_global_inner_ecal};
  double d{ 10.1 * CLHEP::mm };

  if ( cellID.area() == 1 ) { // middle
    A_local  = m_a_local_middle_ecal;
    A_global = m_a_global_middle_ecal;
  } else if ( cellID.area() == 0 ) { // outer
    A_local  = m_a_local_outer_ecal;
    A_global = m_a_global_outer_ecal;
    d        = 15.25 * CLHEP::mm;
  }

  double rX = point.x - x0;
  double rY = point.y - y0;

  //local non-uniformity correction
  const double loc_corr =
      A_local / 2. * ( 1 - cos( 2. * CLHEP::pi * ( rX ) / d ) ) * ( 1 - cos( 2. * CLHEP::pi * ( rY ) / d ) );

  //global non-uniformity correction
  double hCell    = cellSize / 2.;
  const double glob_corr = A_global * ( hCell - rX ) * ( rX + hCell ) / ( hCell * hCell ) * ( hCell - rY ) *
                           ( rY + hCell ) / ( hCell * hCell );

  //Light Reflexion on the edges
  rX    = rX / m_a_reflection_width;
  rY    = rY / m_a_reflection_width;
  hCell = hCell / m_a_reflection_width;

  const double refl_corr = m_a_reflection_height * ( exp( -fabs( rX + hCell ) ) + exp( -fabs( rX - hCell ) ) +
                                                     exp( -fabs( rY + hCell ) ) + exp( -fabs( rY - hCell ) ) );

  return 1 + loc_corr + glob_corr + refl_corr;
}
