/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SIMG4FAST_FASTSIMPOINTLIBMODEL_H
#define SIMG4FAST_FASTSIMPOINTLIBMODEL_H 1

#include <fstream>
#include <cstdlib>
#include <memory>

// forward declarations
class IGiGaSvc ;

// Geant
#include "G4VFastSimulationModel.hh"

// Gaudi
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/IHistogramSvc.h"
#include "GaudiKernel/NTuple.h"

// GaussCalo
#include "CaloDet/DeCalorimeter.h"
#include "GaussCalo/CaloSubHit.h"

// ROOT
#include "TH1F.h"

// Boost
#include <boost/math/constants/constants.hpp>

class TTree;

/*
 * Class implementing a dummy fast simulation model for the calorimeter
 *
 *
 *  author: Matteo Rama - INFN Pisa
 *          Giacomo Vitali - SNS Pisa
 *          3 Mar 2020
 */

class FastsimPointlibModel : public G4VFastSimulationModel {
 public:
   explicit FastsimPointlibModel( const std::string& aModelName, G4Region* aEnvelope, const std::string& library );
   /** Constructor.
    *  @param aModelName Name of the fast simulation model.
    */
//   FastsimPointlibModel( const std::string& aModelName );

   // Method called before the run start.
   virtual void initialize();

   /** Check if this model should be applied to this particle type.
    *  @param aParticle Particle definition (type).
    */
   // virtual G4bool IsApplicable(const G4ParticleDefinition& aParticle) final;
   G4bool IsApplicable( const G4ParticleDefinition& aParticle ) final override;
   /** Check if the model should be applied taking into account the kinematics of a track.
    *  @param aFastTrack Track.
    */
   // virtual G4bool ModelTrigger(const G4FastTrack& aFastTrack) final;
   G4bool ModelTrigger( const G4FastTrack& aFastTrack ) final override;
   /** Implement the engine which creates the fast-simulated CaloSubHits
    */
   // virtual void DoIt(const G4FastTrack& aFastTrack, G4FastStep& aFastStep) final;
   void DoIt( const G4FastTrack& aFastTrack, G4FastStep& aFastStep ) final override;

   void setPRange( const float min, const float max );
   void setThetaRange( const float min, const float max );

   void setCaloDB( DeCalorimeter* Spd, DeCalorimeter* Prs, DeCalorimeter* Ecal, DeCalorimeter* Hcal);

 private:
   
   std::vector<float>                           m_p_e_lib;
   std::vector<float>                           m_p_theta_lib;
   std::vector<float>                           m_p_phi_lib;
   std::vector<float>                           m_xc_yc_vec;
   std::vector<std::vector<float>>              m_x_ave_vec;
   std::vector<std::vector<float>>              m_y_ave_vec;
   std::vector<std::vector<float>>              m_sx_ave_vec;
   std::vector<std::vector<float>>              m_sy_ave_vec;
   std::vector<std::vector<float>>              m_efrac_ave_vec;

   const double m_sinTilt{std::sin( 0.206 / 180 * CLHEP::pi )};
   const double m_cosTilt{std::cos( 0.206 / 180 * CLHEP::pi )};
   float m_lib_E_min{0};
   float m_lib_E_max{0};
   float m_theta_step{0};
   float m_p_min{0};
   float m_p_max{0};
   float m_theta_min{0};
   float m_theta_max{0};
   size_t m_cols_per_node{ 0 };
   size_t m_nbinsE{0};
   size_t m_nbinsTheta{0};
   size_t m_nbinsPhi{0};

   struct libraryPoint {
     double x;
     double y;
     double e;
     double t;
     int calo;
   };
 
   std::vector<std::vector<std::vector<libraryPoint>>> m_library;

   DeCalorimeter* m_EcalDB{nullptr};
   DeCalorimeter* m_PrsDB{nullptr};
   DeCalorimeter* m_SpdDB{nullptr};
   DeCalorimeter* m_HcalDB{nullptr};

   void compute_E_theta_phi( const G4ThreeVector& momentum, double* E, double* theta, double* phi ) const;

   void create_hit_collection( const int col_index, const G4ThreeVector& momentum, const G4ThreeVector& position,
                               const double part_t, std::vector<libraryPoint>* newCollection ) const;

   void get_bin_nodes( const double part_E, const double part_theta, size_t* bin1_E, size_t* bin2_E, size_t* bin1_theta,
                       size_t* bin2_theta ) const;

   double get_interpol_par( const double part_E, const double part_theta, const size_t calo, const size_t index,
                               const size_t index_E, const size_t index_th,
                               const std::vector<std::vector<float>>& interpolationArray,
                               const bool                             shift = true ) const;

   bool getInfo( LHCb::Detector::Calo::CellID& cellID, CaloSubHit::Time& slot, std::vector<double>& fractions,
                 double& nonuniform_corr, const libraryPoint& point ) const;

   double localNonUniformity( const LHCb::Detector::Calo::CellID& cellID, const libraryPoint& point ) const;

   enum CaloTypes { SPD = 0, PRS = 1, ECAL = 2, HCAL = 3 };

   const double m_dT0{0.5};
   const double m_slotWidth{25};
   const std::vector<double> m_sDelays{18.5,17.5,16.5};
   const double m_spdprs_lowerEdge{-24.5};
   const double m_spdprs_upperEdge{125.5};
   const int m_numBXs{6};
 
   // These are constants needed for non-uniformity correction. All are
   // taken from EcalSensDet 
   const double m_a_local_inner_ecal{0};
   const double m_a_local_middle_ecal{0};
   const double m_a_local_outer_ecal{0};
   const double m_a_global_inner_ecal{0.0004};
   const double m_a_global_middle_ecal{0.002};
   const double m_a_global_outer_ecal{0.03};
   const double m_a_reflection_height{0.09};
   const double m_a_reflection_width{ 6 * CLHEP::mm }; // mm

   // Z positions of calorimeter subsystems. These are sensible defaults,
   // which should get updated when we have all to access DetDesc
   std::array<double, 4> m_caloZPositions{12363.7, 12416.3, 12650.5, 13690};
   
   std::array<TH1F,3> m_spdtime_his;
   std::array<TH1F,2> m_ecaltime_his;

   /// Pointer to the GiGa service
   IGiGaSvc* m_gigaSvc;

   typedef GaudiUtils::HashMap<LHCb::Detector::Calo::CellID, CaloSubHit*> SubHitMap;
   SubHitMap                                                  m_hitmap;
  
};


#endif /* SIMG4FAST_FASTSIMPOINTLIBMODEL_H */
