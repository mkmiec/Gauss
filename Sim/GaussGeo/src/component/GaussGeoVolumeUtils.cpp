/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// GEANT4
#include "G4LogicalVolume.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4Region.hh"
#include "G4RegionStore.hh"

// Local
#include "GaussGeoAssembly.h"
#include "GaussGeoAssemblyStore.h"
#include "GaussGeoVolumeUtils.h"

G4LogicalVolume* GaussGeoVolumeUtils::findLVolume(const std::string& name) {
  G4LogicalVolumeStore* store = G4LogicalVolumeStore::GetInstance();
  if (store == nullptr) {
    return nullptr;
  }

  for (G4LogicalVolumeStore::iterator vol_it = store->begin();
       store->end() != vol_it; ++vol_it)
  {
    if (name == (*vol_it)->GetName()) {
      return *vol_it;
    }
  }

  return (G4LogicalVolume*) 0;
}

G4VPhysicalVolume* GaussGeoVolumeUtils::findPVolume(const std::string& name) {
  G4PhysicalVolumeStore* store = G4PhysicalVolumeStore::GetInstance();
  if (store == nullptr) {
    return nullptr;
  }

  for (G4PhysicalVolumeStore::iterator vol_it = store->begin();
       store->end() != vol_it; ++vol_it)
  {
    if (name == (*vol_it)->GetName()) {
      return *vol_it;
    }
  }

  return (G4VPhysicalVolume*) 0;
}

GaussGeoAssembly* GaussGeoVolumeUtils::findLAssembly(const std::string& name) {
  GaussGeoAssemblyStore* store = GaussGeoAssemblyStore::store();
  if (store != nullptr) {
    return store->assembly(name);
  }

  return (GaussGeoAssembly*) 0;
}

G4Region* GaussGeoVolumeUtils::findRegion(const std::string& name) {
  G4RegionStore* store = G4RegionStore::GetInstance();
  if (store == nullptr) {
    return nullptr;
  }

  return store->GetRegion(name);
}
