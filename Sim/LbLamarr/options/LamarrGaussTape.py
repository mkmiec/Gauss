###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
  Define the TES locations to be stored in the output DST (.gen)
"""

from Gaudi.Configuration import *

def writeHits():    
    OutputStream("GaussTape").ItemList.append("/Event/MC/Vertices#1")
    OutputStream("GaussTape").ItemList.append("/Event/MC/Particles#1")
#    OutputStream("GaussTape").ItemList.append("/Event/MCFast/MCParticles#1")
#    OutputStream("GaussTape").ItemList.append("/Event/MCFast/MCVertices#1")
    OutputStream("GaussTape").OptItemList.append("/Event/Rec/ProtoP/Charged#1")
    OutputStream("GaussTape").OptItemList.append("/Event/Rec/ProtoP/Neutrals#1")
    OutputStream("GaussTape").OptItemList.append("/Event/Rec/Track/Best#1")
    OutputStream("GaussTape").OptItemList.append("/Event/Rec/Rich/PIDs#1")
    OutputStream("GaussTape").OptItemList.append("/Event/Rec/Muon/MuonPID#1")
    OutputStream("GaussTape").OptItemList.append("/Event/Rec/Calo/Photons#1")
    OutputStream("GaussTape").OptItemList.append("/Event/Rec/Calo/EcalClusters#1")
    OutputStream("GaussTape").OptItemList.append("/Event/Relations/Rec/ProtoP/Charged#1")
#   OutputStream("GaussTape").ItemList.append("/Event/Relations/Rec/ProtoP/Neutrals#1")
    OutputStream("GaussTape").ItemList.append("/Event/MC/DigiHeader#1")
    OutputStream("GaussTape").ItemList.append("/Event/DAQ/RawEvent#1")
    OutputStream("GaussTape").ItemList.append("/Event/DAQ/ODIN#1")
    OutputStream("GaussTape").OptItemList.append("/Event/Rec/Vertex/Primary#1")
    OutputStream("GaussTape").OptItemList.append("/Event/Rec/Summary#1")
    print(OutputStream("GaussTape"))


appendPostConfigAction(writeHits)


