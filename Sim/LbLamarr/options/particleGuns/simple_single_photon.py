###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#setup environment
from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import MeV
from PhysSelPython.Wrappers import Selection, SelectionSequence
from Configurables import DaVinci, DataOnDemandSvc,DecayTreeTuple,FilterDesktop, CombineParticles,PhotonMaker, PhotonMakerAlg#, Selection, SelectionSequence

from CommonParticles.Utils import *
#get photons protoparticles, make them into particles.
#PhotonMaker takes directly from LHCb::ProtoParticleLocation::Neutrals

algorithm =  PhotonMakerAlg ( 'StdLooseAllPhotons'         ,
                                DecayDescriptor = 'Gamma' )

# configure desktop&particle maker: 
algorithm.addTool ( PhotonMaker , name = 'PhotonMaker' )
photon = algorithm.PhotonMaker
photon.ConvertedPhotons   = True
photon.UnconvertedPhotons = True
#photon.ConfidenceLevelBase   = []
#photon.ConfidenceLevelSwitch = []
photon.PtCut              = 0 * MeV 

algorithm.InputPrimaryVertices = 'None'
## configure Data-On-Demand service 
locations = updateDoD ( algorithm )

## finally: define the symbol 
myStdLooseAllPhotons = Selection("myStdLooseAllPhotons",Algorithm = algorithm ,
                               InputDataSetter=None)

myStdLooseAllPhotonsSeq = SelectionSequence("myStdLooseAllPhotonsSeq",TopSelection = myStdLooseAllPhotons)
# #pi0filter = FilterDesktop("'myStdLooseAllPhotons"

# mypi0s = CombineParticles('mypi0s',
#                           DecayDescriptors = ['pi0 -> gamma gamma'],
#                           CombinationCut = '(AALL)',
#                           MotherCut = '(ALL)'
#                           )
# mypi0Sel = Selection("mypi0sel",
#                      Algorithm = mypi0s,
#                      RequiredSelections = [myStdLooseAllPhotons])
# mypi0SelSeq = SelectionSequence("mypi0SelSeq",TopSelection = mypi0Sel)


tuple = DecayTreeTuple("gammaTree")
tuple.Inputs = [myStdLooseAllPhotonSequence.outputLocation()]
tuple.decay = '[^gamma]'
tuple.ToolList+= ["TupleToolKinematic",
                  "TupleToolGeometry",
                  "TupleToolEventInfo",
                  "TupleToolTrackInfo",
                  ]
DaVinci().PrintFreq = 5000



# tuple = DecayTreeTuple("pi0tree")
# tuple.Inputs = [mypi0SelSeq.outputLocation()]
# tuple.Decay = '[pi0 -> ^gamma ^gamma]CC'
# tuple.ToolList+= ["TupleToolKinematic",
#                   "TupleToolGeometry",
#                   "TupleToolEventInfo",
#                   "TupleToolTrackInfo",
#                   ]
# from Configurables import DaVinci
# DaVinci().PrintFreq = 5000
DaVinci().TupleFile = "gamma_DV_Tuples_from_DelphesAlgs.root"

DaVinci().UserAlgorithms=[algorithm,myStdLooseAllPhotonsSeq]
DaVinci().appendToMainSequence([myStdLooseAllPhotonsSeq.sequence(),mypi0SelSeq.sequence(),tuple])
DaVinci().InputType = 'DST'
DaVinci().Lumi = False
DaVinci().Simulation = True
DaVinci().DataType = "2012"
#DaVinci().CondDBtag = 
#DaVinci().DDDBtag = 
from GaudiConf import IOHelper
IOHelper().inputFiles(['Gauss-10000ev-20180619.gen'],clear=True)
