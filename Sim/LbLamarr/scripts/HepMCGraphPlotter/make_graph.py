###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from copy import deepcopy
from tqdm import tqdm

import numpy as np
import pandas as pd 

from particle import Particle as skhep
from particle import ParticleNotFound, InvalidParticle

import graphviz as gv


## Node class 
class Particle:
    def __init__ (self, index, particle, daughters, parents):
        self.index = index
        self.particle = particle
        self.daughters = daughters
        self.parents = parents
        #self.graph = graph
        self.weight = None
    
    def __str__ (self):
        return f"{self.particle.pdgid}"
    
    @property
    def is_signal(self):
        return self.particle.status == 889

    @property 
    def has_b(self):
      try:
        include_b = ('b' in skhep.from_pdgid(self.particle.pdgid).quarks.lower())     
        is_not_q = len(skhep.from_pdgid(self.particle.pdgid).quarks.lower()) > 1
        return include_b and is_not_q 
      except:
        return False



class Graph:
    def __init__ (self, roots=None):
        self.roots = roots if roots is not None else []
        self._dict = dict()
        
    def __getitem__ (self, key):
        return self._dict[key] 
    
    def __setitem__ (self, key, val):
        self._dict[key] = val
        
    def keys(self):
        return self._dict.keys()

    def values(self):
        return self._dict.values()
    
    def items(self):
        return self._dict.items()
    
    def __len__ (self):
        return len(self._dict)
    
      
    def forward_traversal (self, idx, level=0):
        yield level, self[idx]
        for d in self[idx].daughters:
            try:
              yield from self.forward_traversal (d, level+1)
            except RecursionError:
              continue 
    
    def backward_traversal (self, idx, level=0):
        yield level, self[idx]
        for d in self[idx].parents:
          try:
              yield from self.backward_traversal (d, level+1)
          except RecursionError:
            continue 
            
    def in_ancestors (self, idx, function):
        for iPart, part in self.backward_traversal(idx):
            if function(part):
                return True
        return False

    @staticmethod
    def make (df, weigh=True, desc=None):
      roots = list(df.query('originvertex == 0').index)
      graph = Graph(roots)

      for iPart, particle in tqdm(df.iterrows(), total=len(df), desc=desc):
        graph[iPart] = Particle (iPart,
            particle,
            daughters = list(df[(df.originvertex == particle.endvertex) & (particle.endvertex != 0)].index),
            parents = list(df[(df.endvertex == particle.originvertex) & (particle.originvertex != 0)].index)
            )

      if weigh:
        graph._compute_weights()

      return graph


    def _compute_weights(self):
      "Compute weights of the graph based on the distance from the signal"
      ## Weight initialization 
      for part in self.values():
        part.weight = 0 if (part.is_signal or part.has_b) else len(self) 

      while True:
        modified = False
        for iPart, part in self.items():
            relatives = part.daughters + part.parents
            if len(relatives) > 0:
                if not all([p in self.keys() for p in relatives]): continue
                new_weight = min([self[p].weight for p in relatives]) + 1
                new_weight = min(new_weight, part.weight)
                if new_weight != part.weight:
                    modified = True
                    part.weight = new_weight
        
        if not modified: 
            break

    
    def selected_nodes(self, max_distance=1, signal_descendants=True, b_descendants=True):
      uniques = set()
      
      for idx, part in self.items():
        if part.weight <= max_distance:
          uniques.add (idx)

      if signal_descendants:
        for idx, part in self.items():
          if self.in_ancestors(idx, lambda x: x.is_signal):
            uniques.add (idx)

      if b_descendants:
        for idx, part in self.items():
          if self.in_ancestors(idx, lambda x: x.has_b):
            uniques.add (idx)

      return list(uniques)


    def get_dot(self, matched=None, max_distance=1):
      dot = gv.Digraph('pippo', comment='test', format='svg')
      
      indices = self.selected_nodes(max_distance=max_distance)
      ov = ['MC' if v==0 else v for v in [self[idx].particle.originvertex for idx in indices]]
      ev = ['LHCb' if v==0 else v for v in [self[idx].particle.endvertex for idx in indices]]


      for vtx in set(ov+ev):
        if isinstance(vtx, str):
          dot.node(str(vtx), str(vtx), style='filled', color='cyan', shape='rect', width="4")
        else:
          dot.node(str(vtx), "", shape='circle', width="0.15", color='black', style='filled')

      for idx in indices:
        part = self[idx].particle
        isMatched = matched is not None and part.barcode in matched
        if self.in_ancestors(idx, lambda x: x.is_signal):
            color = 'green' if isMatched else 'red' 
        elif isMatched:
            color = 'blue'
        else:
            color = 'gray'
        
        try:
            label = skhep.from_pdgid(part.pdgid).evtgen_name
        except (ParticleNotFound, InvalidParticle):
            label = str(part.pdgid)
            
        #label += f" [{part.barcode}]"
        
        dot.edge(
            'MC' if part.originvertex==0 else str(part.originvertex), 
            'LHCb' if part.endvertex==0 else str(part.endvertex), 
            arrowsize="0.4",
            penwidth="2" if isMatched else "1",
            label=label, color=color, texmode='math')

      return dot

      


if __name__ == '__main__':
  from argparse import ArgumentParser
  import sqlite3 as sql

  parser = ArgumentParser()
  parser.add_argument("inputfile", type=str, default='output.db', 
    help="Path of the SQLite file to process"
    )
  parser.add_argument("-d", "--depth", type=int, default=1, 
    help="Depth of the HepMC tree to draw. Tree size increases exponentially"
    )
  args = parser.parse_args()
 

  conn = sql.connect(args.inputfile)
  conn.executescript( """
  CREATE TEMP VIEW IF NOT EXISTS mcParts AS SELECT * FROM particles WHERE category=='MC';
  CREATE TEMP VIEW IF NOT EXISTS hepmcParts AS SELECT * FROM particles WHERE category=='HepMC';
  """)

  particles = pd.read_sql_query( "SELECT * FROM particles", conn)
  vertices = pd.read_sql_query("SELECT * FROM vertices", conn)

  matched = pd.read_sql_query( """
  SELECT mc.barcode AS mcid, hmc.barcode AS hmcid, hmc.event, hmc.interaction, (hmc.originvertex & 0xFF) AS ov, (hmc.endvertex & 0xFF) AS ev FROM mcParts mc 
      JOIN hepmcParts hmc
      WHERE 
          mc.event == hmc.event 
          -- AND hmc.interaction == 1 
          AND (mc.barcode & 0xFFFF) == (hmc.barcode & 0xFFFF)
          AND (mc.barcode >> 16) == (hmc.interaction)
          AND mc.pdgid == hmc.pdgid
  """, conn)

  figs = []
  for event in particles.event.unique():
    for interaction in particles[particles.event==event].interaction.unique():
      figs.append(f"{event}.{interaction}")

  with open('index.htm', 'w') as fout:
    print (f"""
      <HTML>
        <HEAD><TITLE>Index</TITLE></HEAD>
        <BODY>
          {"".join([f"<IMG src='processed-graph-event-{f}.svg'>" for f in figs])}
        </BODY>
      </HEAD>
    """, file=fout)



  for event in particles.event.unique():
    for interaction in particles[particles.event==event].interaction.unique():
      df = particles.query(f'event == {event} and interaction == {interaction} and category == "HepMC"')
      if len(df):
        dot = Graph.make(df, desc=f'Fig. {event}.{interaction}').get_dot(
            matched=matched.query(f'event == {event}').hmcid.values,
            max_distance=args.depth,
            )
        dot.render(filename=f'processed-graph-event-{event}.{interaction}')


