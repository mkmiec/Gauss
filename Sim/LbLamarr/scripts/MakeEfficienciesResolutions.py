#!/opt/local/bin/python
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import sys, os

if (len(sys.argv)<2):
    print("Specify a file please")
    sys.exit()

import ROOT as R
import pandas
import numpy as np
import root_pandas as rp

printFreq = 1000
tr_recoble = rp.read_root(sys.argv[1],"TrackEffRes/TrackTree_mc",where="reconstructible && recobleCat==2")
tr_recoed  = rp.read_root(sys.argv[1],"TrackEffRes/TrackTree_recoed", where="type==3")

statesInTuple = [
    "EndVelo",
    "BegRich1",
    "EndRich1",
    "EndT",
    "BegRich2"
]

states = []

if(len(sys.argv)>2):
    if(sys.argv[2] == "all"):
        states = statesInTuple
    else:
        for i in range(2,len(sys.argv)):
            states.append(sys.argv[i])

RecobleHistos = []
RecoedHistos  = []

stateDict = {
    "ClosestToBeam" : "0",
    "BegRich1" :   "990",
    "EndRich1" :  "2165",
    "EndT"     :  "9410",
    "BegRich2" :  "9450",
    "EndRich2" : "11900",
    "BegT"     :  "7500",
    "MidT"     :  "8520",
    "MidTT"    :  "2485",
    "EndTT"    :  "2700",
    "EndVelo"  :   "770"
    }



#nbinsX = 100
#nbinsY = 100
#nbinsZ = 500
#nbinsX = 50
#nbinsY = 50
#nbinsZ = 100
nbinsX = 10
nbinsY = 10
nbinsZ = 20

xmin   = -1.0
xmax   = 1.0
ymin   = -0.3
ymax   = 0.3
zmin   = 0.0
zmax   = 0.001

#nbins = np.array([nbinsX, nbinsY, nbinsZ, 500, 500, 500], dtype=np.int32)
nbins = np.array([50, 50, 250, 100, 100, 500], dtype=np.int32)
xminv = np.array([xmin, ymin, 0, -0.02, -0.02, -2.5e3]) 
xmaxv = np.array([xmax, ymax, 100e3, 0.02, 0.02, 2.5e3])

RecobleEffHistos = {}
RecoedEffHistos = {}
ResHistos = {}

tf = R.TFile("ResSparse_newEdition.root","RECREATE")

for state in states:
    RecobleEffHistos[state] = R.TH3D("hReconstructible_{0}".format(state),"hReconstructible_{0}".format(state),nbinsX,xmin,xmax,nbinsY,ymin,ymax,nbinsZ,zmin,zmax)
    RecoedEffHistos[state]  = R.TH3D("hReconstructed_{0}".format(state),"hReconstructed_{0}".format(state),nbinsX,xmin,xmax,nbinsY,ymin,ymax,nbinsZ,zmin,zmax)
    ResHistos[state] = R.THnSparseD("sparseRes_{0}".format(state),"sparseRes_{0}".format(state),6,nbins,xminv,xmaxv)
    print("Filling {0} state Reconstructible histo".format(state))
    for i in range(len(tr_recoble)):
        if(i%printFreq == 0):
            print("event: ",i)
        RecobleEffHistos[state].Fill(tr_recoble["tx_{0}".format(state)][i],
                                     tr_recoble["ty_{0}".format(state)][i],
                                     1./tr_recoble["p_{0}".format(state)][i]
        )

        
    print("Filling {0} state Reconstructed histo".format(state))
    for i in range(len(tr_recoed)):
        if(i%printFreq == 0):
            print("event: ",i)
        RecoedEffHistos[state].Fill(tr_recoed["tx_{0}".format(state)][i],
                                    tr_recoed["ty_{0}".format(state)][i],
                                    1./tr_recoed["p_{0}".format(state)][i]
        )
        
        arrayFill = np.array([tr_recoed["tx_{0}".format(state)][i],
                              tr_recoed["ty_{0}".format(state)][i],
                              tr_recoed["p_{0}".format(state)][i],
                              tr_recoed["tx_{0}".format(state)][i] - tr_recoed["true_tx_{0}".format(state)][i],
                              tr_recoed["ty_{0}".format(state)][i] - tr_recoed["true_ty_{0}".format(state)][i],
                              tr_recoed["p_{0}".format(state)][i]  - tr_recoed["true_p_{0}".format(state)][i]
        ])
        ResHistos[state].Fill(arrayFill)
    RecobleEffHistos[state].Write()
    RecoedEffHistos[state].Write()
    ResHistos[state].Write()
tf.Close()




