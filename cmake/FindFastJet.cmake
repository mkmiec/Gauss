###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# - Locate FastJet package
# Defines:
#
#  FASTJET_FOUND
#  FASTJET_INCLUDE_DIR
#  FASTJET_INCLUDE_DIRS
#  FASTJET_LIBRARIES
#  FASTJET_LIBRARY_DIRS
#
#  Targets:
#    FastJet::FastJet


find_library(FASTJET_LIBRARY NAMES fastjet
             HINTS $ENV{FASTJET_ROOT_DIR}/lib ${FASTJET_ROOT_DIR}/lib)

find_path(FASTJET_INCLUDE_DIR fastjet/ClusterSequence.hh
  HINTS $ENV{FASTJET_ROOT_DIR}/include ${FASTJET_ROOT_DIR}/include)

mark_as_advanced(FASTJET_INCLUDE_DIR FASTJET_LIBRARY)
		
# handle the QUIETLY and REQUIRED arguments and set FASTJET_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(FastJet DEFAULT_MSG FASTJET_INCLUDE_DIR FASTJET_LIBRARY)

set(FASTJET_LIBRARIES ${FASTJET_LIBRARY})
get_filename_component(FASTJET_LIBRARY_DIRS ${FASTJET_LIBRARY} PATH)

set(FASTJET_INCLUDE_DIRS ${FASTJET_INCLUDE_DIR})

mark_as_advanced(FASTJET_FOUND)

if(FASTJET_FOUND AND NOT TARGET FastJet::FastJet)
  add_library(FastJet::FastJet UNKNOWN IMPORTED)
  set_target_properties(FastJet::FastJet PROPERTIES IMPORTED_LOCATION ${FASTJET_LIBRARIES})
  target_include_directories(FastJet::FastJet SYSTEM INTERFACE ${FASTJET_INCLUDE_DIR})
endif()
