###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# - Try to find Madgraph
# Defines:
#
#  MADGRAPH_FOUND
#  MADGRAPH_EXE
#  MADGRAPH_PDF
#  MADGRAPH_VERSION

find_path(MADGRAPH_BIN mg5_aMC PATH_SUFFIXES bin
  HINTS $ENV{MADGRAPH_ROOT_DIR} ${MADGRAPH_ROOT_DIR})

find_file(MADGRAPH_EXE mg5_aMC
  HINTS ${MADGRAPH_BIN})

find_file(MADGRAPH_PDF pdf_lhapdf62.cc
  HINTS ${MADGRAPH_BIN}/../Template/NLO/Source/PDF)

mark_as_advanced(MADGRAPH_EXE MADGRAPH_PDF)

# Find the Madgraph version.
if(MADGRAPH_BIN AND NOT MADGRAPH_VERSION)
  string(REGEX REPLACE ".*/madgraph5amc/(.*)/bin" "\\1" MADGRAPH_VERSION
    "${MADGRAPH_BIN}")
  set(MADGRAPH_VERSION ${MADGRAPH_VERSION} CACHE
    STRING "Detected version of Madgraph.")
  mark_as_advanced(MADGRAPH_VERSION)
endif()

# Handle the QUIETLY and REQUIRED arguments and set MADGRAPH_FOUND to
# TRUE if all listed variables are TRUE.
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Madgraph DEFAULT_MSG
  MADGRAPH_EXE MADGRAPH_PDF MADGRAPH_VERSION)

mark_as_advanced(MADGRAPH_FOUND)
