###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# - Try to find CRMC
# Defines:
#
#  CRMC_FOUND
#  CRMC_HOME (not cached)
#  CRMC_INCLUDE_DIR
#  CRMC_INCLUDE_DIRS (not cached)
#  CRMC_LIBRARY
#  CRMC_LIBRARIES (not cached)
#  CRMC_LIBRARY_DIRS (not cached)
#
#  Targets:
#    CRMC::CrmcBasic
#    CRMC::HepEvtDummy


find_path(CRMC_INCLUDE_DIR CRMCconfig.h
          HINTS $ENV{CRMC_ROOT_DIR}/src
          PATH_SUFFIXES src)

if(CRMC_INCLUDE_DIR)
  get_filename_component(CRMC_HOME "${CRMC_INCLUDE_DIR}" DIRECTORY)
  set(CRMC_INCLUDE_DIRS "${CRMC_INCLUDE_DIR}")
  set(CRMC_LIBRARY_DIRS "${CRMC_HOME}/lib")
  set(CRMC_LIBRARIES "${CRMC_LIBRARY_DIRS}/libCrmcBasic.so" "${CRMC_LIBRARY_DIRS}/libHepEvtDummy.so")
  
endif()

mark_as_advanced(CRMC_INCLUDE_DIR)

# handle the QUIETLY and REQUIRED arguments and set CRMC_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(CRMC DEFAULT_MSG CRMC_INCLUDE_DIR)

mark_as_advanced(CRMC_FOUND)

if(NOT CRMC_FIND_COMPONENTS)
  set(CRMC_FIND_COMPONENTS CrmcBasic HepEvtDummy)
endif()

if(CRMC_FOUND)
  foreach(component ${CRMC_FIND_COMPONENTS})
    if(NOT TARGET CRMC::${component})
      add_library(CRMC::${component} UNKNOWN IMPORTED)
      set_target_properties(CRMC::${component} PROPERTIES IMPORTED_LOCATION ${CRMC_LIBRARY_DIRS}/lib${component}.so)
      target_include_directories(CRMC::${component} SYSTEM INTERFACE ${CRMC_INCLUDE_DIRS})
    endif()
  endforeach()
endif()
