2020-10-09 Gauss v54r4p1
===

This version uses LHCb v51rX, Gaudi v33r0, Geant4 v106r2p1 and LCG_96b with ROOT 6.18.04.

The following generators are used from LCG_96b:
- pythia8   244.lhcb4,  lhapdf 6.2.3,     photos++  3.56.lhcb1, tauola++  1.1.6b.lhcb
- pythia6   427.2.lhcb
- herwig++  2.7.1,      thepeg 2.1.5
- crmc      1.6.0.lhcb,      hijing 1.383bs.2, starlight r300
- rivet     2.7.2b,     yoda   1.7.7   

while EvtGen, `R02-00-00` tag, is built internally in Gauss from its HepForge repository

and the followings are built privately:
- EvtGenExtras, AmpGen, Mint
- BcVegPy,  GenXicc 
- SuperChic2, LPair
- MIB


This version is released on `master` branch.
Built relative to Gauss v54r4p1, with the following changes:


### Fixes ~"bug fix" ~workaround

- ~Generators | Configure the Primary Vertex for ParticleGun in the upgrade as in 2018, !660 (@gcorti)


### Documentation ~Documentation


