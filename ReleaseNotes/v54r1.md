2020-03-23 Gauss v54r1
===

This version uses LHCb v51r0, Gaudi v33r0, Geant4 v106r1 and LCG_96b with ROOT 6.18.04.

The following generators are used from LCG_96b:
- pythia8   240.lhcb4,  lhapdf 6.2.3,     photos++  3.56.lhcb1, tauola++  1.1.6b.lhcb
- pythia6   427.2.lhcb
- herwig++  2.7.1,      thepeg 2.1.5
- crmc      1.5.6,      hijing 1.383bs.2, starlight r300
- rivet     2.7.2b,     yoda   1.7.7   

while the followings are built privately:
- EvtGen with EvtGenExtras, AmpGen, Mint
- BcVegPy,  GenXicc 
- SuperChic2, LPair
- MIB

This version is released on `master` branch.
Built relative to Gauss v54r0, with the following changes:


### Fixes ~"bug fix" ~workaround

- ~Generators ~IFT | Versions of generators with increased HEPEVT size to fix crash with EPOS, !566 (@gcorti) [LHCBGAUSS-1944]


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Configuration ~IFT | Update all epos qmtest to remove Gauss-DEV, !574 (@gcorti)
- ~Simulation | Updated SplitSim test, !572 (@dmuller)
- ~Simulation | Update reference of QMTests, !579 (@gcorti)

