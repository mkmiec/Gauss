2020-09-15 Gauss v54r4
===

This version uses LHCb v51r0, Gaudi v33r0, Geant4 v106r2p1 and LCG_96b with ROOT 6.18.04.

The following generators are used from LCG_96b:
- pythia8   **244.lhcb4**,  lhapdf 6.2.3,     photos++  3.56.lhcb1, tauola++  1.1.6b.lhcb
- pythia6   427.2.lhcb
- herwig++  2.7.1,      thepeg 2.1.5
- crmc      **1.6.0.lhcb**,      hijing 1.383bs.2, starlight r300
- rivet     2.7.2b,     yoda   1.7.7   

while the followings are built privately:
- EvtGen with EvtGenExtras, AmpGen, Mint
- BcVegPy,  GenXicc 
- SuperChic2, LPair
- MIB


This version is released on `master` branch.   
Built relative to Gauss v54r3, with the following changes:


### Fixes ~"bug fix" ~workaround

- ~"Detector simulation" ~Geant4 | Remove workaround preventing passing high energy heavy ions to Geant, !642 (@kreps)
- ~Generators ~Decays ~IFT | Change version of crmc to use special build, !639 (@gcorti)
- ~Generators ~IFT | Try version of Pythia8 that may fix LHCBGAUSS-2089, !641 (@gcorti) [LHCBGAUSS-2089]


### Enhancements ~enhancement

- ~Generators ~IFT | Patch to allow axions to be simulated with StarLight  to master, !647 (@gcorti)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Generators ~Decays | Modified Gauss to use the new Gen/LbEvtGen package for EvtGen (LHCBGAUSS-1492)., !425 (@jback) [LHCBGAUSS-1492]
- Update reference tests for release v54r4, !652 (@gcorti)
- Change GaussSys test to use Pythia8 instead of 6, !638 (@gcorti)
- Centralize preprocessing and fix test validators, !628 (@rmatev) [gaudi/Gaudi#108]


### Documentation ~Documentation

- Update README.md and CONTRIBUTING.md, !624 (@gcorti)
- Documentation and dependencies for v54r4 release, !654 (@gcorti)


