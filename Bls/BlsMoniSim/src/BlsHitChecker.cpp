/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: BlsHitChecker.cpp,v 1.1.1.2 2010-03-10 17:38:47 vtalanov Exp $
// Include files 

// from Gaudi
#include "GaudiAlg/Tuples.h"
#include "Event/MCParticle.h"
#include "Event/MCHeader.h"

// from STL
#include <map>
#include <string>

// local
#include "BlsHitChecker.h"

//-----------------------------------------------------------------------------
// Implementation file for class : BlsHitChecker
//
// 2010-02-06 : Vadim Talanov
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( BlsHitChecker )

//=============================================================================
// Initialization
//=============================================================================

StatusCode BlsHitChecker::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  if ( msgLevel(MSG::DEBUG) )
    debug() << "BlsHitChecker::initialize()" << endmsg;
  return StatusCode::SUCCESS;
} // end of: BlsHitChecker::initialize() {

//=============================================================================
// Main execution
//=============================================================================

StatusCode BlsHitChecker::execute() {
  if ( msgLevel(MSG::DEBUG) )
    debug() << "BlsHitChecker::execute()" << endmsg;
  if ( ! exist < LHCb::MCHits > ( m_blsHitsLocation.value() ) ) {
    // Something happened - m_blsHitsLocation points to nowhere?...
    error() << "There are no MCHits at "
            << m_blsHitsLocation.value()
            << " in TES!" << endmsg;
    // This is critical - tell the world about this error
    return StatusCode::FAILURE;
  } // end of: if ( ! exist < LHCb::MCHits > ( m_blsHitsLocation.value() ) )
  // Get MC hits in the scintillator volume of BLSs
  m_blsMCHits = get < LHCb::MCHits > ( m_blsHitsLocation.value() );
  // Just return if the number of hits in all BLSs is zero
  if ( 0 == m_blsMCHits->size() )
    return StatusCode::SUCCESS;
  // Get event header for event number (also used much later)
  const LHCb::MCHeader* myMCHeader =
    get < LHCb::MCHeader > ( LHCb::MCHeaderLocation::Default );
  if ( msgLevel(MSG::DEBUG) )
    debug() << "m_blsHitsLocation.value(): "
            << m_blsMCHits->size()
            << " hit(s) found at event no. "
            << myMCHeader->evtNumber() << endmsg;
  // Define a multimap for Particle/Hit pairs
  t_blsMCParticle2MCHitMultimap myMultimap;
  // Loop over all hits got from m_blsHitsLocation.value()
  for ( LHCb::MCHits::iterator It = m_blsMCHits->begin();
        It != m_blsMCHits->end();
        It++ ) {
    // Get MC Particle that created this hit
    const LHCb::MCParticle* myMCParticle = (*It)->mcParticle();
    // If DEBUG is switched on
    if ( msgLevel(MSG::DEBUG) ) {
      // Print some data for hit and particle
      LHCb::ParticleID myParticleId = myMCParticle->particleID();
      Gaudi::LorentzVector myParticleMomentum = myMCParticle->momentum();
      const LHCb::MCVertex* myMCVertex = myMCParticle->originVertex();
      debug () << "Particle ID and momentum: "
               << myParticleId.pid() << " "
               << myParticleMomentum.e()/Gaudi::Units::GeV << " "
               << myMCParticle << " "
               << (*It) << " "
               << endmsg;
      debug () << "Entry point: "
               << (*It)->entry().x()/Gaudi::Units::mm << " "
               << (*It)->entry().y()/Gaudi::Units::mm << " "
               << (*It)->entry().z()/Gaudi::Units::mm
               << endmsg;
      debug () << "Exit point: "
               << (*It)->exit().x()/Gaudi::Units::mm << " "
               << (*It)->exit().y()/Gaudi::Units::mm << " "
               << (*It)->exit().z()/Gaudi::Units::mm
               << endmsg;
      debug () << "Vertex position: "
               << myMCVertex->position().x()/Gaudi::Units::mm << " " 
               << myMCVertex->position().y()/Gaudi::Units::mm << " " 
               << myMCVertex->position().z()/Gaudi::Units::mm << " " 
               << myMCVertex->type()
               << endmsg;
    } // end of: if ( msgLevel(MSG::DEBUG) )
    // Positive X goes to positive BLS and negative X to negative BLS
    if ( ( (*It)->entry().x() > 0 && m_blsCOn.value() ) ||
         ( (*It)->entry().x() < 0 && m_blsAOn.value() ) ) {
      // Insert a pair of Particle-Hit into the map
      myMultimap.insert
        ( t_blsMCParticle2MCHitMultimap::value_type ( myMCParticle, *It ) );
    } // end of: if ( ( (*It)->entry().x() > 0 && m_blsCOn.value() ) ||
  } // end of: for ( LHCb::MCHits::iterator It = m_blsMCHits->begin();
  // Just return if no hits were found in a particular BLS
  if ( myMultimap.empty() ) {
    if ( msgLevel(MSG::DEBUG) )
      debug() << "No hits in this BLS found at event no. "
              << myMCHeader->evtNumber()
              << endmsg;
    return StatusCode::SUCCESS;
  } // end of: if ( myMultimap.empty() ) {
  // Calculate hits/tracks and energy deposited in event
  unsigned short myEventNumHits = 0;
  unsigned short myEventNumTracks = 0;
  double myEventEnDep = 0.0;
  // String for histogram titles
  std::string myTitle;
  // Scan Particle<->Hit map to get quantities per single track
  for ( t_blsMCParticle2MCHitMultimap::iterator anIt = myMultimap.begin();
        anIt != myMultimap.end();
        ) {
    // Count new track
    myEventNumTracks++;
    // Calculate energy deposited by a single track and this track length
    double myTrackEnDep = 0.0;
    double myTrackLength = 0.0;
    // For each unique key (== MCParticle) in a map collect hits it has created
    const LHCb::MCParticle* myKey = (*anIt).first;
    for ( t_blsMCParticle2MCHitMultimap::iterator oneMoreIt =
            myMultimap.lower_bound ( myKey );
          oneMoreIt != myMultimap.upper_bound ( myKey );
          oneMoreIt++ ) {
      // Count new hit
      myEventNumHits++;
      const LHCb::MCParticle* myMCParticle = (*oneMoreIt).first;
      LHCb::MCHit* myMCHit = (*oneMoreIt).second;
      // If DEBUG is switched on
      if ( msgLevel(MSG::DEBUG) ) {
        debug() << "Particle2Hit map: "
                << myMCParticle << " "
                << myMCHit << " "
                << myMCHit->energy()/Gaudi::Units::MeV << " "
                << myMCHit->time()/Gaudi::Units::ns
                << endmsg;
      } // end of: if ( msgLevel(MSG::DEBUG) )
      // Add properties for current hit (== step) to track
      myTrackEnDep += myMCHit->energy();
      myTrackLength += myMCHit->pathLength();
      // Make histograms for a per hit quantities
      myTitle = m_blsHTitlePrefix.value()
        + "Hit entry point in scintillator [mm], XY plane";
      plot2D( myMCHit->entry().x()/Gaudi::Units::mm,
              myMCHit->entry().y()/Gaudi::Units::mm,
              "HitEntryXY",
              myTitle,
              m_blsHEntryXMin.value()/Gaudi::Units::mm,
              m_blsHEntryXMax.value()/Gaudi::Units::mm,
              m_blsHEntryYMin.value()/Gaudi::Units::mm,
              m_blsHEntryYMax.value()/Gaudi::Units::mm,
              m_blsHEntryXNbins.value(),
              m_blsHEntryYNbins.value() );
      myTitle = m_blsHTitlePrefix.value()
        + "Hit entry point in scintillator [mm], ZX plane";
      plot2D( myMCHit->entry().z()/Gaudi::Units::mm,
              myMCHit->entry().x()/Gaudi::Units::mm,
              "HitEntryZX",
              myTitle,
              m_blsHEntryZMin.value()/Gaudi::Units::mm,
              m_blsHEntryZMax.value()/Gaudi::Units::mm,
              m_blsHEntryXMin.value()/Gaudi::Units::mm,
              m_blsHEntryXMax.value()/Gaudi::Units::mm,
              m_blsHEntryZNbins.value(),
              m_blsHEntryXNbins.value() );
      myTitle = m_blsHTitlePrefix.value()
        + "Hit entry point in scintillator [mm], ZY plane";
      plot2D( myMCHit->entry().z()/Gaudi::Units::mm,
              myMCHit->entry().y()/Gaudi::Units::mm,
              "HitEntryZY",
              myTitle,
              m_blsHEntryZMin.value()/Gaudi::Units::mm,
              m_blsHEntryZMax.value()/Gaudi::Units::mm,
              m_blsHEntryYMin.value()/Gaudi::Units::mm,
              m_blsHEntryYMax.value()/Gaudi::Units::mm,
              m_blsHEntryZNbins.value(),
              m_blsHEntryYNbins.value() );
      myTitle = m_blsHTitlePrefix.value()
        + "Hit energy deposited in scintillator [MeV]";
      plot( ( myMCHit->time() + m_blsHEntryTimeOffset.value() )/Gaudi::Units::ns,
            "HitEnDepTime",
            myTitle,
            m_blsHEntryTimeMin.value(),
            m_blsHEntryTimeMax.value(),
            m_blsHEntryTimeNbins.value(),
            myMCHit->energy()/Gaudi::Units::MeV);
      // This can as well be: anIt = upper_bound(myKey)
      anIt++;
    } // end of: for ( t_blsMCParticle2MCHitMultimap::iterator oneMoreIt
    // Add energy deposited by a single track to total energy deposited in event
    myEventEnDep += myTrackEnDep;
    // Make histograms for a per track quantities
    myTitle = m_blsHTitlePrefix.value()
      + "Energy deposited per track [MeV]";
    plot( myTrackEnDep/Gaudi::Units::MeV,
          "TrackEnDep",
          myTitle,
          m_blsHTrackEnDepMin.value(),
          m_blsHTrackEnDepMax.value(),
          m_blsHTrackEnDepNbins.value());
    myTitle = m_blsHTitlePrefix.value()
      + "Track length [cm]";
    plot( myTrackLength/Gaudi::Units::cm,
          "TrackLength",
          myTitle,
          m_blsHTrackLengthMin.value(),
          m_blsHTrackLengthMax.value(),
          m_blsHTrackLengthNbins.value());
    myTitle = m_blsHTitlePrefix.value()
      + "Energy deposited per track unit length [MeV/cm]";
    plot( (myTrackEnDep/Gaudi::Units::MeV)/(myTrackLength/Gaudi::Units::cm),
          "TrackEnDepPerUnitLength",
          myTitle,
          m_blsHTrackEnDepMin.value(),
          m_blsHTrackEnDepMax.value(),
          m_blsHTrackEnDepNbins.value());
  } // end of: for ( t_blsMCParticle2MCHitMultimap::iterator anIt
  // If DEBUG is switched on
  if ( msgLevel(MSG::DEBUG) ) {
    debug() << "Event no. "
            << myMCHeader->evtNumber()
            << ": Hits/Tracks = "
            << myEventNumHits
            << "/"
            << myEventNumTracks
            << ", EnDep = "
            << myEventEnDep/Gaudi::Units::MeV
            << endmsg;
  } // end of: if ( msgLevel(MSG::DEBUG) )
  // Make histograms for a per event quantities
  myTitle = m_blsHTitlePrefix.value()
    + "Number of hits per event ";
  plot( myEventNumHits,
        "EventNumHits",
        myTitle,
        m_blsHEventNumTracksMin.value(),
        m_blsHEventNumTracksMax.value(),
        m_blsHEventNumTracksNbins.value() );
  myTitle = m_blsHTitlePrefix.value()
      + "Number of tracks per event ";
  plot( myEventNumTracks,
        "EventNumTracks",
        myTitle,
        m_blsHEventNumTracksMin.value(),
        m_blsHEventNumTracksMax.value(),
        m_blsHEventNumTracksNbins.value() );
  myTitle = m_blsHTitlePrefix.value()
    + "Energy deposited per event [MeV]";
  plot( myEventEnDep/Gaudi::Units::MeV,
        "EventEnDep",
        myTitle,
        m_blsHTrackEnDepMin.value(),
        m_blsHTrackEnDepMax.value(),
        m_blsHTrackEnDepNbins.value());
  // Count number of hits/tracks per run
  m_blsHits += myEventNumHits;
  m_blsTracks += myEventNumTracks;
  // Plot global per run quantities
  myTitle = m_blsHTitlePrefix.value()
      + "Number of hits in event";
  plot( myMCHeader->evtNumber(),
        "RunNumHits",
        myTitle,
        m_blsHEventNumMin.value(),
        m_blsHEventNumMax.value(),
        m_blsHEventNumNbins.value(),
        myEventNumHits);
  myTitle = m_blsHTitlePrefix.value()
      + "Number of tracks in event";
  plot( myMCHeader->evtNumber(),
        "RunNumTracks",
        myTitle,
        m_blsHEventNumMin.value(),
        m_blsHEventNumMax.value(),
        m_blsHEventNumNbins.value(),
        myEventNumTracks);
  myTitle = m_blsHTitlePrefix.value()
    + "Energy deposited in event [MeV]";
  plot( myMCHeader->evtNumber(),
        "RunEnDep",
        myTitle,
        m_blsHEventNumMin.value(),
        m_blsHEventNumMax.value(),
        m_blsHEventNumNbins.value(),
        myEventEnDep/Gaudi::Units::MeV);
  // Final global return
  return StatusCode::SUCCESS;
} // end of: BlsHitChecker::execute()

//=============================================================================
//  Finalize
//=============================================================================

StatusCode BlsHitChecker::finalize() {
  if ( msgLevel(MSG::INFO) )
    // Print numbers for Hits and Tracks to the common output
    info() << "Hits/Tracks: "
           << m_blsHits
           << "/"
           << m_blsTracks << endmsg; 
  return GaudiAlgorithm::finalize();  // must be called after all other actions
} // end of: BlsHitChecker::finalize()

//=============================================================================
